// Product array puzzle

import java.util.*;

class Array0 {

	static int[] productArray(int arr[]) {
		int prod[] = new int[arr.length];
		prod[0] = arr[0];
		for(int i = 1 ; i<prod.length ; i++) {
			prod[i] = prod[i-1]*arr[i];
		}

		int prodOfArray = prod[prod.length-1];
		for(int i = 0 ; i<prod.length ; i++) 
			prod[i] = prodOfArray/arr[i];
		
		return prod;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int prod[] = productArray(arr);

		System.out.print("Prod Array : ");
		for(int i = 0 ; i<prod.length ; i++) {
			System.out.print(prod[i] + " ");
		}
		System.out.println();
	}
}
