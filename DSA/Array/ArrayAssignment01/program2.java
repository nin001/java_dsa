// Linear Search - multiple occurances

import java.util.*;

class Array0 {

	static int multipleOccurances(int arr[] , int b) {
		int count = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == b)
				count++;
		}
		return count;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter n : ");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("enter b : ");
		int b = sc.nextInt();
		System.out.println(multipleOccurances(arr,b));
	}
}
