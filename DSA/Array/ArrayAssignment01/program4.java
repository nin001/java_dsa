// Time to equality
// in one second we can increase the value by 1 find the total number of seconds to 
// make each element same

import java.util.*;

class Array0 {

	static int timeToEquality(int arr[]) {
		int max = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(max<arr[i]) {
				max = arr[i];
			}
		}

		int time = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(max == arr[i]) {
				continue;
			}else {
				time += (max-arr[i]);
			}
		}
		
		return time;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println(timeToEquality(arr));
	}
}
