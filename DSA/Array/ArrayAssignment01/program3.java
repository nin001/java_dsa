// Range Query Sum problem

import java.util.*;

class Array0 {

	static void rangeQuery(int arr[] , int q[][]) {
		int preSum[] = new int[arr.length];
		preSum[0] = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			preSum[i] = preSum[i-1]+arr[i];
		}

		for(int i = 0 ; i<q.length ; i++) {
			if(q[i][0]>=0 && q[i][0]<arr.length && q[i][0]<q[i][1] && q[i][1]<arr.length) {
				if(q[i][0] != 0) {
					System.out.println(preSum[q[i][1]] - preSum[q[i][0]-1]);
				}else {
					System.out.println(preSum[q[i][1]]);
				}
			}
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("enter size :");
		int n = sc.nextInt();
		
		int arr[] = new int[n];
		System.out.println("enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter queries :");
		int q = sc.nextInt();

		int queries[][] = new int[q][2];
		for(int i = 0 ; i<q ; i++) {
			System.out.println("Enter range query :");
			queries[i][0] = sc.nextInt();
			queries[i][1] = sc.nextInt();
		}

		rangeQuery(arr,queries);
	
	}
}

