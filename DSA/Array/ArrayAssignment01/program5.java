// Inplace prefix sum

import java.util.*;

class Array0 {

	static void inplacePrefixSum(int arr[]) {
		for(int i = 1 ; i<arr.length ; i++) {
			arr[i] = arr[i-1]+arr[i];
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		inplacePrefixSum(arr);

		System.out.println("array :");
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
