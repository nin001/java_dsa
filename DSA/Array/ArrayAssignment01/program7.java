// Leaders in array

import java.util.*;

class Array0 {
	static void leaders(int arr[], int size) {
        	int rightMax = arr[size-1];
 
        	System.out.print(rightMax + " ");

        	for (int i = size-2; i >= 0; i--) {
            		if (rightMax < arr[i]) {
            			rightMax = arr[i];
            			System.out.print(rightMax + " ");
        		}
        	}

		System.out.println();
    	}

	public static void main(String args[]) {
               	Scanner sc = new Scanner(System.in);
      		System.out.println("Enter size :");
               	int n = sc.nextInt();
       	        int arr[] = new int[n];
                System.out.println("Enter elements :");
               	for(int i = 0 ; i<n ; i++) {
                        arr[i] = sc.nextInt();
                }

		leaders(arr,n);
	}
}
