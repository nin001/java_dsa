// Max and min of array
// return sum of max and min of array

import java.io.*;

class Array0 {

	static int minMaxSum(int arr[] , int n) {
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for(int i = 0 ; i<n ; i++) {
			if(min>arr[i]) 
				min = arr[i];
			if(max<arr[i])
				max = arr[i];
		}
		return min+max;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter n :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println(minMaxSum(arr,n));
	}
}
