// Prefix Sum in Query Range problem
// inplace sum

import java.util.*;

class PrefixSum {


	static int[] prefixSum(int arr[] , int n , int q) {

		for(int i = 1 ; i<n ; i++) {
			arr[i] = arr[i-1]+arr[i];
		}

		Scanner sc = new Scanner(System.in);
		for(int i = 0 ; i<q ; i++) {
			int s = sc.nextInt();
			int e = sc.nextInt();
			if(s<n && s>=0 && e>=s && e<n) {
				if(s==0) {
					System.out.println(arr[e]);
				}else {
					System.out.println(arr[e]-arr[s-1]);
				}
			}else {
				System.out.println("Invalid range");
			}
		}
		return arr;
	}

	public static void main(String args[]) {
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int n = arr.length;

		System.out.println("Enter q :");
		int q = new Scanner(System.in).nextInt();

		prefixSum(arr,n,q);

		System.out.println("Array :");
		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
