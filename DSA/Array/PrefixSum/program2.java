// Prefix Sum in Query Range problem

import java.util.*;

class PrefixSum {


	static void prefixSum(int arr[] , int n , int q) {

		int preSum[] = new int[n];
		preSum[0] = arr[0];
		for(int i = 1 ; i<n ; i++) {
			preSum[i] = preSum[i-1]+arr[i];
		}

		Scanner sc = new Scanner(System.in);
		for(int i = 0 ; i<q ; i++) {
			int s = sc.nextInt();
			int e = sc.nextInt();
			if(s<n && s>=0 && e>=s && e<n) {
				if(s==0) {
					System.out.println(preSum[e]);
				}else {
					System.out.println(preSum[e]-preSum[s-1]);
				}
			}else {
				System.out.println("Invalid range");
			}
		}
	}

	public static void main(String args[]) {
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int n = arr.length;

		System.out.println("Enter q :");
		int q = new Scanner(System.in).nextInt();

		prefixSum(arr,n,q);
	}
}
