//RangeQuery problem

import java.util.*;

class PrefixSum {
	public static void main(String args[]) {
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int n = arr.length;

		int q = 3;

		Scanner sc = new Scanner(System.in);
		for(int i = 0 ; i<q ; i++) {		//O(q)
			int s = sc.nextInt();
			int e = sc.nextInt();
			if(s<n && s>=0 && e>=s && e<n) {	//O(n)
				int sum = 0;
				for(int j = s ; j<=e ; j++) {
					sum += arr[j];
				}
				System.out.println(sum);
			}else {
				System.out.println("Invalid range");
			}
		}

		// Time complexity of this approch is O(q*n) which will give TLE 
		// for higher value of input
		// PrefixSum approch is used in this type of problems , where array of 
		// cummilative sum is calculated and query is resulted in O(1) Time
	}
}	
