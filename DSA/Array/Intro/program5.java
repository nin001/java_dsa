// Second largest element

import java.util.*;

class ArrayDemo {
	public static void main(String args[]) {
       	         Scanner sc = new Scanner(System.in);
	
        	 System.out.println("Enter size :");
                 int n = sc.nextInt();

	         int arr[] = new int[n];
        	 System.out.println("Enter elements :");
 	         for(int i = 0 ; i<n ; i++) {
        	         arr[i] = sc.nextInt();
       	 	 }

		 int max = Integer.MIN_VALUE;
		 for(int i = 0 ; i<n ; i++) {
			 if(max<arr[i])
				 max = arr[i];
		 }

		 int sec = Integer.MIN_VALUE;
		 for(int i = 0 ; i<n ; i++) {
			 if(sec<arr[i] && arr[i]<max)
				 sec = arr[i];
		 }

		 System.out.println("Second largest :"+sec);
	 }
}
