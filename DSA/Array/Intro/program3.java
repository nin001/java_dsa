// Given array print the count of pairs whose sum is k

import java.util.*;

class ArrayDemo {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter k : ");
		int k = sc.nextInt();

		int count = 0;
		for(int i = 0 ; i<n ; i++) {
			for(int j = i+1 ; j<n ; j++) {
				if(arr[i]+arr[j] == k) {
					count++;
				}
			}
		}

		System.out.println(count);
	}
}
