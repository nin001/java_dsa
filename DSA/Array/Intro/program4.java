// Reverse array

import java.util.*;

class ArrayDemo {

	static void reverseArray(int arr[] , int n) {
		int start = 0;
		int end = n-1;
		while(start<end) {
			int temp = arr[start];
			arr[start] = arr[end];
			arr[end] = temp;

			start++;
			end--;
		}
	}

        public static void main(String args[]) {
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter size :");
                int n = sc.nextInt();

                int arr[] = new int[n];
                System.out.println("Enter elements :");
                for(int i = 0 ; i<n ; i++) {
                        arr[i] = sc.nextInt();
                }

		reverseArray(arr,n);

		System.out.println("Reversed Array :");
		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

	}
}
