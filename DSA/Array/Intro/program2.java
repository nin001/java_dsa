// Count elements that have atleast one element greater than itself

import java.util.*;

class ArrayDemo {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int count = 0;
		int max = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(max<arr[i]) 
				max = arr[i];
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(max == arr[i]) 
				count++;
		}

		System.out.println("count : " + (n-count));
	}
}
