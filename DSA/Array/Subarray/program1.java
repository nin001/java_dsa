// Subarray is the part of the array that follows the sequence of the array
// eg - [1 2 3 4 5]
// subarray - [3,4,5]
// but the sequence of occurance of elements cant be changed

class Subarray {

	//brute force approch
	static int shortestSubarrayMinMax(int arr[]) {
	
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(min>arr[i])
				min = arr[i];
			if(max<arr[i])
				max = arr[i];
		}

		int len = Integer.MAX_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			int tempLen = 0;
			if(arr[i] == max) {
				for(int j = i+1 ; j<arr.length ; j++) {
					if(arr[j]==min) {
						tempLen = j-i+1;
						break;
					}
				}
			}else if(arr[i] == min) {
				for(int j = i+1 ; j<arr.length ; j++) {
					if(arr[j] == max) {
						tempLen = j-i+1;
						break;
					}
				}
			}
			if(len>tempLen) {
				len = tempLen;
			}
		}

		return len;
	}

	public static void main(String args[]) {

		int arr[] = new int[]{6,2,4,1,5,3,6,1,2,3};

		int size = shortestSubarrayMinMax(arr);

		System.out.println(size);
	}
}
