// right max

class RightMax {
	
	static int[] rightMax(int arr[]) {
		int rmax[] = new int[arr.length];
		rmax[arr.length-1] = arr[arr.length-1];
		for(int i = arr.length-2 ; i>=0 ; i--) {
			if(rmax[i+1]>arr[i])
				rmax[i] = rmax[i+1];
			else
				rmax[i] = arr[i];
		}

		return rmax;
	}

	static void inplaceRightMax(int arr[]) {
		for(int i = arr.length-2 ; i>=0 ; i--) {
			if(arr[i+1]>arr[i]) {
				arr[i] = arr[i+1];
			}
		}
	}
	public static void main(String args[]) {

		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int rmax[] = rightMax(arr);

		for(int i = 0 ; i<rmax.length ; i++) {
			System.out.print(rmax[i] + " ");
		}
		System.out.println();

		inplaceRightMax(arr);
		
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i]+ " ");
		}
		System.out.println();
	}
}
