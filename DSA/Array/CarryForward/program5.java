// count number of pairs of "ag" sequence in the character array

// Optimized
class AGSequence {

	static int countPair(char arr[]) {
		int countA = 0;
		int pairCount = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == 'a') 
				countA++;
			else if(arr[i] == 'g')
				pairCount += countA;
		}
		return pairCount;

		//TC = O(n)
		//SC = O(1)
	}
	public static void main(String args[]) {

		char arr[] = new char[]{'a','b','e','g','a','g'};

		int count = countPair(arr);

		System.out.println(count);
	}
}
