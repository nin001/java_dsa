// Equilibrium array

// optimized

class Equilibrium {

	static int equilibrium(int arr[]) {
		
		int preSum[] = new int[arr.length];
		preSum[0] = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			preSum[i] = preSum[i-1]+arr[i];
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(i == 0) {
				if((preSum[arr.length-1]-preSum[i]) == 0)
					return i;
			}else if(i == arr.length-1) {
				if(preSum[i-1] == 0) 
					return i;
			}else {
				if(preSum[i-1] == (preSum[arr.length-1]-preSum[i]))
					return i;
			}
		}
		return -1;
	}

	public static void main(String args[]) {

		int arr[] = new int[]{-7,1,5,2,-4,3,0};

		int eqIndex = equilibrium(arr);

		System.out.println(eqIndex);
	
	}
}
