// Equilibrium array

class Equilibrium {

	static int equilibrium(int arr[]) {
		
		for(int i  = 0 ; i<arr.length ; i++) {
			int lsum = 0;
			int rsum = 0;
			for(int j = 0 ; j<i ; j++) {
				lsum += arr[j];
			}

			for(int j = i+1 ; j<arr.length ; j++) {
				rsum += arr[j];
			}

			if(lsum == rsum)
				return i;
		}
		return -1;
	}

	public static void main(String args[]) {

		int arr[] = new int[]{-7,1,5,2,-4,3,0};

		int eqIndex = equilibrium(arr);

		System.out.println(eqIndex);
	
	}
}
