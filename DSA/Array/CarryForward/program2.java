// Inplace leftmax

class LeftMax {

	static void leftMax(int arr[]) {
		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[i]<arr[i-1])
				arr[i] = arr[i-1];
		}
	}
	public static void main(String args[]) {
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		leftMax(arr);

		System.out.print("array :");
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
