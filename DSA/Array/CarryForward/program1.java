// Carry forward approch is a approch , which helps to carry forward
// previously calculated results inorder to prevent repeated calculation
// which helps in improving time complexity
//
// LeftMax
// Build a array of size n (given array size) which contains data for i 
// where i is the max of left from 0 to i

class LeftMax {

	static int[] leftMax(int arr[]) {
		int lmax[] = new int[arr.length];
		lmax[0] = arr[0];

		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[i]<lmax[i-1]) {
				lmax[i] = lmax[i-1];
			}else {
				lmax[i] = arr[i];
			}
		}

		return lmax;
	}

	public static void main(String args[]) {
		int arr[] = new int[]{-3,6,2,4,5,2,8,-9,3,1};

		int leftMax[] = leftMax(arr);

		for(int i = 0 ; i<leftMax.length ; i++) {
			System.out.print(leftMax[i] + " ");
		}
		System.out.println();
	}
}
