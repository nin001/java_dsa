//Reverse Integer Leetcode 7

import java.util.*;

class LeetCode0 {

	static int reverseFun(int n) {

		int rev = 0;
		while(n!=0) {
			if(rev<=-231 && rev>231)
				return 0;
			rev = rev*10 + n%10;
			n /= 10;
		}
		return rev;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Num :");
		int n = sc.nextInt();

		System.out.println(reverseFun(n));
	}
}
