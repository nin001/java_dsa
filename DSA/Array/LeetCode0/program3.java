// insert into sorted array (leetcode 	35)

import java.util.*;

class LeetCode0 {
	
	static int insert(int arr[] , int n , int ele) {
		int start = 0;
		int end = n-1;
		int mid = (start+end)/2;

		while(start<=end) {
			if(arr[mid] == ele) {
				return mid;
			}else if(arr[mid]<ele) {
				start = mid+1;
			}else {
				end = mid-1;
			}
			mid = (start+end)/2;
		}
		return end+1;
		
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter element to insert :");
		int ele = sc.nextInt();

		System.out.println("index : "+insert(arr,size,ele));
	
	}
}
