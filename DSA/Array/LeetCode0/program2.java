// two sum Leetcode problem 1

import java.util.*;

class LeetCode0 {

	static int[] twoSum(int arr[] , int n , int t) {
		int ret[] = new int[2];
		ret[0] = -1;
		ret[1] = -1;

		for(int i = 0 ; i<n ; i++) {
			for(int j = i+1 ; j<n ; j++) {
				if(arr[i]+arr[j] == t) {
					ret[0] = i;
					ret[1] = j;
					return ret;
				}
			}
		}
		return ret;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) 
			arr[i] = sc.nextInt();

		System.out.println("Enter target :");
		int target = sc.nextInt();

		int ret[] = twoSum(arr,n,target);

		System.out.println(ret[0] + " " + ret[1]);
	}
}
