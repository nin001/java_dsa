// Given 2 sorted arrays . modify both arrays such that forms a merged sorted array

class Sorting {


	void sort(int arr1[] , int n , int arr2[] , int m) {

		int i = 0;
		
		while(i<n) {
			int j = 0;
			int temp1 = arr1[i];
			int temp2 = arr2[j];
			while(j<n && arr2[j]<temp1) {
				arr2[j] = arr2[j+1];
				j++;
			}
			
			arr1[i] = temp2;
			arr2[j-1] = temp1;
			i++;
		}

	}

	void printArr(int arr1[]) {
		for(int i = 0 ; i<arr1.length ; i++) {
			System.out.print(arr1[i] + " ");
		}

		System.out.println();
	}

	public static void main(String args[]) {

		Sorting obj = new Sorting();

		int arr1[] = new int[]{1,3,5,7};
		int arr2[] = new int[]{0,2,6,8,9};

		obj.sort(arr1,arr1.length,arr2,arr2.length);

		obj.printArr(arr1);
		obj.printArr(arr2);
	}
}
