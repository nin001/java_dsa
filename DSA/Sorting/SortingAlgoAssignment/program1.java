// Given list of arr and n . Sort using Bubble sort

class BubbleSort {

	void bubbleSort(int arr[] , int n) {

		for(int i = 0 ; i<n ; i++) {
			boolean flag = false;
			for(int j = 0 ; j<n-i-1 ; j++) {
				if(arr[j]>arr[j+1]) {
					flag = true;
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}

			if(flag == false)
				return;

		}
	}

	public static void main(String args[]) {

		BubbleSort obj = new BubbleSort();

		int arr[] = new int[]{7,4,10,25,3,2,1};

		obj.bubbleSort(arr,arr.length);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
