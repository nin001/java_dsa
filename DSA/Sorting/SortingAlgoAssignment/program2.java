// Array containing 0s 1s and 2s . Sort the array

class Sorting {

	void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	void sort(int arr[] , int n) {
		
		int i = 0;
		int j = 0;
		int k = arr.length-1;

		while(j<=k) {
			if(arr[j] == 0) {
				swap(arr,i,j);
				i++;
				j++;
			}else if(arr[j] == 1) {
				j++;
			}else {
				swap(arr,j,k);
			
				k--;
			}
		}
	}

	public static void main(String args[]) {

		Sorting obj = new Sorting();

		int arr[] = new int[]{2,0,1,0,1,2};

		obj.sort(arr,arr.length);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
