// Sort the Array using Merge sort algorithm

import java.util.Scanner;

class MergeSort {

	void mergeSort(int arr[] , int start , int end) {
		if(start<end) {
			int mid = (start+end)/2; 

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);

			merge(arr,start,mid,end);
		}
	}

	void merge(int arr[] , int start , int mid , int end) {

		int s1 = mid-start+1;
		int s2 = end-mid;

		int arr1[] = new int[s1];
		int arr2[] = new int[s2];

		for(int i = 0 ; i<arr1.length ; i++) {
			arr1[i] = arr[i+start];
		}

		for(int i = 0 ; i<arr2.length ; i++) {
			arr2[i] = arr[i+mid+1];
		}

		int i = 0;
		int j = 0;
		int k = start;

		while(i<arr1.length && j<arr2.length) {
			if(arr1[i]<arr2[j]) {
				arr[k] = arr1[i];
				i++;
			}else {
				arr[k] = arr2[j];
				j++;
			}
			k++;

		}

		while(i<arr1.length) {
			arr[k++] = arr1[i++];
		}

		while(j<arr2.length) {
			arr[k++] = arr2[j++];
		}
	}
}

class Client {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		MergeSort sort = new MergeSort();

		sort.mergeSort(arr,0,arr.length-1);

		System.out.print("SortedArray : ");
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();

	}
}
