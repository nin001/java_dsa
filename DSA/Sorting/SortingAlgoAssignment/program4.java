// Check if given array is in non decreasing format

class Sorting {

	boolean checkSorted(int arr[]) {
	
		for(int i = 0 ; i<arr.length-1 ; i++) {
			if(arr[i]>arr[i+1])
				return false;
		}

		return true;

		
	}
	public static void main(String args[]) {

		Sorting obj = new Sorting();

		int arr[] = new int[]{1,2,3,4,5};

		if(obj.checkSorted(arr)) {
			System.out.println("Sorted");
		}else {
			System.out.println("Not sorted");
		}
	}
}
