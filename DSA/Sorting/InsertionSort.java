// Insertion sort

import java.util.*;

class InsertionSort {

	void insertionSort(int arr[]) {

		for(int i = 1 ; i<arr.length ; i++) {
			int j = i-1;
			int temp = arr[i];
			while(j>=0 && arr[j]>temp) {
				arr[j+1] = arr[j];
				j--;
			}

			arr[j+1] = temp;
		}
	}
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = sc.nextInt();
		}

		InsertionSort obj = new InsertionSort();

		obj.insertionSort(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
