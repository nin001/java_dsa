// MergeSort

import java.util.*;

class MergeSort {

	void merge(int arr[] , int start , int mid , int end) {
		int n1 = mid-start+1;
		int n2 = end-mid;

		int arr1[] = new int[n1];
		int arr2[] = new int[n2];

		for(int i = 0 ; i<n1 ; i++) {
			arr1[i] = arr[i+start];
		}

		for(int i = 0 ; i<n2 ; i++) {
			arr2[i] = arr[i+mid+1];
		}

		int i = 0;
		int j = 0;
		int k = start;
		while(i<n1 && j<n2) {
			if(arr1[i]<arr2[j]) {
				arr[k] = arr1[i];
				i++;
			}else {
				arr[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i<n1) {
			arr[k++] = arr1[i++];
		}

		while(j<n2) {
			arr[k++] = arr2[j++];
		}

	}

	void mergeSort(int arr[] , int start , int end) {
		if(start<end) {
			int mid = start+(end-start)/2;

			mergeSort(arr,start,mid);
			mergeSort(arr,mid+1,end);

			merge(arr,start,mid,end);
		}
	}

	public static void main(String args[]) {
		
		MergeSort obj = new MergeSort();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements : ");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int start = 0;
		int end = n-1;

		obj.mergeSort(arr,start,end);

		System.out.println("Sorted Array :");
		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
