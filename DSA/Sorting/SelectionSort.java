//Selection sort

import java.io.*;

class SelectionSort {

	void selectionSort(int arr[]) {

		for(int i = 0 ; i<arr.length-1 ; i++) {
			int minIdx = i;
			for(int j = i+1 ; j<arr.length ; j++) {
				if(arr[minIdx]>arr[j])
					minIdx = j;
			}

			int temp = arr[minIdx];
			arr[minIdx] = arr[i];
			arr[i] = temp;
		}
	}

	public static void main(String args[]) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<size ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		SelectionSort obj = new SelectionSort();

		obj.selectionSort(arr);

		for(int i = 0 ; i<size ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
