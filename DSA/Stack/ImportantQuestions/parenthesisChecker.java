// Valid Parenthesis Checker

import java.util.*;

class ValidParenthesis {

	boolean validate(String exp) {

		Stack<Character> s = new Stack<Character>();
		
		for(int i = 0 ; i<exp.length() ; i++) {
			char par = exp.charAt(i);
			if(par == '{' || par == '(' || par == '[') {
				s.push(par);
			}else {
				if(s.empty()) {
					return false;
				}
				if(s.peek() == '{' && par == '}') {
					s.pop();
				}else if(s.peek() == '[' && par == ']') {
					s.pop();
				}else if(s.peek() == '(' && par == ')') {
					s.pop();
				}else {
					return false;
				}
			}
		}

		if(s.empty())
			return true;
		return false;
	}
}

class Client {

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter expression : ");
		String exp = sc.next();

		ValidParenthesis check = new ValidParenthesis();

		if(check.validate(exp)) {
			System.out.println("Balenced");
		}else {
			System.out.println("Not balanced");
		}

	}
}
