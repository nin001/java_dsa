// Two stacks in one array

import java.util.*;

class TwoStack {

	int stackArr[];
	int size;
	int top1;
	int top2;
	int flag = 0;
	TwoStack(int size) {
		this.size = size;
		stackArr = new int[size];
		top1 = -1;
		top2 = size;
	}

	void push1(int data) {
		if(top2-top1>1) {
			stackArr[++top1] = data;
		}else {
			System.out.println("Stack Overflow");
		}
	}

	void push2(int data) {
		if(top2-top1>1) {
			stackArr[--top2] = data;
		}else {
			System.out.println("Stack Overflow");
		}
	}

	int pop1() {
		if(top1 == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			int val = stackArr[top1];
			top1--;
			return val;
		}
	}

	int pop2() {
		if(top2 == size) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			int val = stackArr[top2];
			top2++;
			return val;
		}
	}

}

class Client {

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size of Array :");
		int size = sc.nextInt();

		TwoStack s = new TwoStack(size);

		int ch;

		do {
			System.out.println("1.Push1");
			System.out.println("2.Push2");
			System.out.println("3.Pop1");
			System.out.println("4.Pop2");

			System.out.println("Enter choice : ");
			int choice = sc.nextInt();

			switch(choice) {
				case 1: {
						System.out.println("Enter data :");
						int data = sc.nextInt();
						s.push1(data);
					}break;
				case 2: {
						System.out.println("Enter data :");
						int data = sc.nextInt();
						s.push2(data);
					}break;
				case 3: {
						int pop = s.pop1();
						if(s.flag != 0) {
							System.out.println(pop + " Popped");
						}else {
							System.out.println("Stack1 Empty");
						}
					}break;
				case 4: {
						int pop = s.pop2();
						if(s.flag != 0) {
							System.out.println(pop + " Popped");
						}else {
							System.out.println("Stack2 Empty");
						}
					}break;
			}
			System.out.println("Do You Want To Continue (1/0) : ");
			ch = sc.nextInt();
		}while(ch == 1);
				
	}
}
