// Stack implementation in Collection

import java.util.*;

class Client {
	public static void main(String args[]) {

		Stack<Integer> stack = new Stack<Integer>();

		stack.push(10);
		stack.push(20);
		stack.push(30);

		System.out.println(stack);//[10,20,30]

		System.out.println(stack.peek());//30
		System.out.println(stack.empty());//true
		System.out.println(stack.pop());//30
		System.out.println(stack.peek());//20
		System.out.println(stack);//[10,20]

		stack.pop();
		stack.pop();
		System.out.println(stack.empty());//true
	}
}

