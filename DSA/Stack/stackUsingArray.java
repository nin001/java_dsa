// Stack implementation using array

import java.util.*;

class Stack {
	int size;
	int stack[];
	int top = -1;
	int flag = 0;

	Stack(int size) {
		this.size = size;
		this.stack = new int[size];
	}

	//Push
	void push(int data) {
		if(top == (size-1)) {
			System.out.println("Stack Overflow");
		}else {
			stack[++top] = data;
		}
	}

	//Pop
	int pop() {
		if(top == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			return stack[top--];
		}
	}

	//Peek
	int peek() {
		if(top == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			return stack[top];
		}
	}

	//isEmpty
	
	boolean isEmpty() {
		if(top == -1) {
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		String print = "[ ";
		for(int i = 0 ; i<=top ; i++) {
			print = print + stack[i] + " ";
		}
		print = print+"]";

		return print;
	}
}

class Client {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size of stack :");
		int size = sc.nextInt();

		Stack s = new Stack(size);

		int ch;
		do {
			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Peek");
			System.out.println("4.isEmpty");

			System.out.print("Enter Choice : ");
			int choice = sc.nextInt();

			switch(choice) {

				case 1:
					{
						System.out.println("Enter Data : ");
						int data = sc.nextInt();
						s.push(data);
					}break;
				case 2:
					{
						int ret = s.pop();
						if(s.flag == 1) {
							System.out.println(ret + "Popped");
						}else {
							System.out.println("Stack Underflow");
						}
					}break;
				case 3:
					{
						int ret = s.peek();
						if(s.flag == 1) {
							System.out.println(ret+"At Top");
						}else {
							System.out.println("Stack empty");
						}
					}break;
				case 4:
					System.out.println(s.isEmpty());
					break;	
			}

			System.out.println("Do you want to continue (1/0) :");
			ch = sc.nextInt();

		}while(ch == 1);
	}
}
