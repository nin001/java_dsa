// Inplace Iterative

class Node {
	int data;
	Node next = null;

	Node(int data) {
		this.data = data;
	}
}

class LinkedList {
	Node head = null;

	void createLL() {
		Node node1 = new Node(10);
		head = node1;

		Node node2 = new Node(20);
		node1.next = node2;

		Node node3 = new Node(30);
		node2.next = node3;

		Node node4 = new Node(40);
		node3.next = node4;

	}

	void printLL() {
		Node temp = head;
		while(temp.next != null) {
			System.out.print(temp.data + "->");
			temp = temp.next;
		}

		System.out.println(temp.data);
	}

	void inplaceReverseIterative() {
		if(head == null) {
			System.out.println("LinkedList is empty");
			return;
		}

		Node prev = null;
		Node temp = head;
		while(head.next != null) {

			head = head.next;
			temp.next = prev;
			prev = temp;
			temp = head;
		}

		temp.next = prev;
	}

}

class Client {
	public static void main(String args[]) {
		LinkedList ll = new LinkedList();

		ll.createLL();
		ll.printLL();
		ll.inplaceReverseIterative();
		ll.printLL();
	}
}


