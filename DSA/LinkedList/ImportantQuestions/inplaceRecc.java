// inplace reverse Reccursive approch

class Node {
	int data;
	Node next = null;
	Node(int data) {
		this.data = data;
	}
}

class LinkedList {
	Node head = null;

	void createLL() {
		Node n1 = new Node(10);
		Node n2 = new Node(20);
		Node n3 = new Node(30);
		Node n4 = new Node(40);

		head = n1;
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
	}

	void printLL() {
		Node p = head;
		while(p.next != null) {
			System.out.print(p.data + "->");
			p = p.next;
		}

		System.out.println(p.data);
	}
	
	void inplaceRecc(Node head , Node prev , Node forward) {
	
		if(forward == null) {
			head.next = prev;
			this.head = head;
			return;
		}

		head.next = prev;
		prev = head;
		head = forward;
		forward = head.next;

		inplaceRecc(head,prev,forward);
	}

	// Sirs approch
	void inplaceReccSir(Node prev , Node curr) {
		if(curr == null) {
			head = prev;
			return;
		}else {
			Node forward = curr.next;
			curr.next = prev;
			prev = curr;
			curr = forward;
		}
		inplaceReccSir(prev,curr);
	}
}

class Client {
	public static void main(String args[]) {
		LinkedList ll = new LinkedList();

		ll.createLL();
		ll.printLL();

		ll.inplaceRecc(ll.head,null,ll.head.next);

		ll.printLL();

		ll.inplaceReccSir(null,ll.head);
		ll.printLL();
	}
}
