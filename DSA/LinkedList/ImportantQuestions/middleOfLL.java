// Middle of linkedlist
// Fastptr and Slowptr 

class Node {
	int data;
	Node next = null;

	Node(int data) {
		this.data = data;
	}
}

class LinkedList {
	 Node head = null;

        void createLL() {
                Node node1 = new Node(10);
                head = node1;

                Node node2 = new Node(20);
                node1.next = node2;

                Node node3 = new Node(30);
                node2.next = node3;

                Node node4 = new Node(40);
                node3.next = node4;

        }

        void printLL() {
                Node temp = head;
                while(temp.next != null) {
                        System.out.print(temp.data + "->");
                        temp = temp.next;
                }

                System.out.println(temp.data);
        }

	// count Approch
	int countNode() {
		return 4;
	}

	Node middleOfLLCount() {
		Node temp = head;
		int count = 0;
		while(count<countNode()/2) {
			count++;
			temp = temp.next;
		}
		return temp;
	}

	// 2ptr approch
	Node middleOfLL() {
		if(head == null || head.next == null) {
			return head;
		}

		Node fastPtr = head.next;
		Node slowPtr = head;
		
		while(fastPtr != null) {
			slowPtr = slowPtr.next;
			fastPtr = fastPtr.next;
			if(fastPtr != null) {
				fastPtr = fastPtr.next;
			}
	
		}

		return slowPtr;
	}
}

class Client {
	public static void main(String args[]) {

		LinkedList ll = new LinkedList();

		ll.createLL();
		ll.printLL();

		Node middle = ll.middleOfLL();
		if(middle != null) {
			System.out.println("Middle Node : "+middle.data);
		}else {
			System.out.println("Empty");
		}

		System.out.println(ll.middleOfLLCount().data);
	}
}
