// Singly linked list

import java.util.Scanner;

class Node {
	int data;
	Node next = null;

	Node(int data) {
		this.data = data;
	}
}

class LinkedList {
	Node head = null;

	void addFirst(int data) {
		Node newNode = new Node(data);
		if(head == null) {
			head = newNode;
		}else {
			newNode.next = head;
			head = newNode;
		}
	}

	void addLast(int data) {
		Node newNode = new Node(data);
		if(head == null) {
			head = newNode;
		}else {
			Node temp = head;
			while(temp.next != null) {
				temp = temp.next;
			}
			temp.next = newNode;
		}
	}

	int countNode() {
		if(head == null) {
			return 0;
		}else {
			int count = 0;
			Node temp = head;
			while(temp != null) {
				temp = temp.next;
				count++;
			}
			return count;
		}
	}

	void addAtPosition(int pos , int data) {
		if(pos < 0 || pos > countNode()) {
			System.out.println("Invalid Position");
		}else {
		
			if(pos == 0) {
				addFirst(data);
			}else if(pos == countNode()) {
				addLast(data);
			}else {
				Node newNode = new Node(data);
				Node temp = head;
				while((pos-1)!=0) {
					temp = temp.next;
					pos--;
				}

				newNode.next = temp.next;
				temp.next = newNode;
			}
		}
	}

	void deleteFirst() {
		if(head == null) {
			System.out.println("Linked List is Empty\nInvalid Operation");
		}else {
			head = head.next;
		}
	}

	void deleteLast() {
		if(head == null) {
			System.out.println("Linked List is Empty\nInvalid Operation");
		}else {
			if(head.next == null) {
				head = null;
			}else {
				Node temp = head;
				while(temp.next.next != null) {
				       temp = temp.next;
				}
				temp.next = null;
			}
		}
	}

	void deleteAtPosition(int pos) {
		if(pos<0 || pos>(countNode()-1)) {
			System.out.println("Invalid position cannot delete");
		}else {
			if(pos == 0) {
				deleteFirst();
			}else if(pos == countNode()-1) {
				deleteLast();
			}else {
				Node temp = head;
				while((pos-1)!=0) {
					temp = temp.next;
					pos--;
				}
				temp.next = temp.next.next;
			}
		}
	}

	void printLL() {
		if(head == null) {
			System.out.println("LinkedList Empty");
		}else {
			Node temp = head;
			while(temp.next != null) {
				System.out.print(temp.data + "->");
				temp = temp.next;
			}
			System.out.println(temp.data);
		}
	}

}


class Client {
	public static void main(String args[]) {

		LinkedList ll = new LinkedList();
		Scanner sc = new Scanner(System.in);

		int ch;
		do {
			System.out.println("1.AddFirst");
			System.out.println("2.AddLast");
			System.out.println("3.AddAtPosition");
			System.out.println("4.DeleteFirst");
			System.out.println("5.DeleteLast");
			System.out.println("6.DeleteAtPosition");
			System.out.println("7.printLL");
			System.out.println("8.countNode");

			System.out.print("Enter Your choice : ");
			int choice = sc.nextInt();

			switch(choice) {
				case 1:
					{
						System.out.println("Enter data : ");
						int data = sc.nextInt();
						ll.addFirst(data);
					}break;
				case 2:
					{
						System.out.println("Enter data : ");
						int data = sc.nextInt();
						ll.addLast(data);
					}break;
				case 3:
					{
						System.out.println("Enter data :");
						int data = sc.nextInt();
						System.out.println("Enter position : ");
						int pos = sc.nextInt();
						ll.addAtPosition(pos,data);
					}break;
				case 4:
					ll.deleteFirst();
					break;
				case 5:
					ll.deleteLast();
					break;
				case 6:
					{
						System.out.println("Enter position :");
						int pos = sc.nextInt();
						ll.deleteAtPosition(pos);
					}break;
				case 7:
					ll.printLL();
					break;
				case 8:
					System.out.println("Node Count : "+ll.countNode());
					break;
			}

			System.out.println("Do you want to continue(0/1) ");
			ch = sc.nextInt();
		}while(ch == 1);
			
	}
}
