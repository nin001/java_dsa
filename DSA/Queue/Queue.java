// Implementation of Queue using array

import java.util.Scanner;

class Queue {

	int size;
	int queue[];
	int front = -1;
	int rear = -1;
	int flag = 0;

	Queue(int size) {
		this.size = size;
		queue = new int[size];
	}

	void enqueue(int data) {
		if(rear == size-1) {
			System.out.println("Queue Full");
		}else {
			if(front == -1)
				front++;
			rear++;

			queue[rear] = data;
		}
	}

	int dequeue() {
		if(rear == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			int ret = queue[front];

			if(front == rear) {
				front = -1;
				rear = -1;
			}else {
				front++;
			}
			return ret;
		}
	}

	int front() {
		if(front == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			return queue[front];
		}
	}

	int rear() {
		if(rear == -1) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			return queue[rear];
		}
	}

	void printQueue() {
		if(front == -1) {
			System.out.println("Queue is empty");
		}else {
			System.out.print("[ ");
			for(int i = front ; i<=rear ; i++) {
				System.out.print(queue[i] + " ");
			}
			System.out.println("]");
		}
	}

}

class Client {
	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Queue Size : ");
		int size = sc.nextInt();

		Queue q = new Queue(size);

		int ch;
		do {
			System.out.println("1.enqueue");
			System.out.println("2.dequeue");
			System.out.println("3.front");
			System.out.println("4.rear");
			System.out.println("5.printQ");

			System.out.println("Enter choice :");

			int choice = sc.nextInt();

			switch(choice) {

				case 1:{
					       System.out.println("Enter Data : ");
					       int data = sc.nextInt();
					       q.enqueue(data);
					}break;
				case 2:{
					       
					       int data = q.dequeue();
					       if(q.flag == 0) {
						       System.out.println("Queue Empty");
					       }else {
						       System.out.println(data + " dequeued");
					       }
					}break;
				case 3:{
					       int data = q.front();
					       if(q.flag == 0) {
						       System.out.println("Queue empty");
					       }else {
						       System.out.println(data +" at front");
					       }
					}break;
				case 4:{
					       
					       int data = q.rear();
					       if(q.flag == 0) {
						       System.out.println("Queue empty");
					       }else {
						       System.out.println(data + " at rear");
					       }
					}break;
				case 5:
				       q.printQueue();
					       	 
			}

			System.out.println("Continue(1/0) : ");
			ch = sc.nextInt();
		}while(ch==1);
	}
}
