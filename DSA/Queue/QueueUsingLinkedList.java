// implementation of queue using LinkedList

import java.util.Scanner;

class Node {
	int data;
	Node next;

	Node(int data) {
		this.data = data;
		this.next = null;
	}
}

class QueueUsingLinkedList {

	Node front = null;
	Node rear = null;
	int flag = 0;

	void enqueue(int data) {
		Node newNode = new Node(data);

		if(front == null) {
			front = newNode;
			rear = newNode;
		}else {
			rear.next = newNode;
			rear = newNode;
		}
	}

	int dequeue() {
		if(rear == null) {
			flag = 0;
			return -1;
		}else {
			flag = 1;
			int val = front.data;
			if(front == rear) {
				front = null;
				rear = null;
			}else {
				front = front.next;
			}
			return val;
		}
	}

	int front() {
		if(front == null) {
			flag = 0;
			return -1;
		}
		flag = 1;
		return front.data;
	}

	int rear() {
		if(rear == null) {
			flag = 0;
			return -1;
		}
		flag = 1;
		return rear.data;
	}

	void printQueue() {
		if(front == null) {
			System.out.println("Queue empty");
		}else {
			System.out.print("[ ");
			Node temp = front;
			while(temp != null) {
				System.out.print(temp.data + " ");
				temp = temp.next;
			}
			System.out.println("]");
		}
	}
}

class Client {
	/*public static void main(String args[]) {

		//BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
	}*/
	 public static void main(String args[]) {

                Scanner sc = new Scanner(System.in);


                QueueUsingLinkedList q = new QueueUsingLinkedList();

                int ch;
                do {
                        System.out.println("1.enqueue");
                        System.out.println("2.dequeue");
                        System.out.println("3.front");
                        System.out.println("4.rear");
                        System.out.println("5.printQ");

                        System.out.println("Enter choice :");

                        int choice = sc.nextInt();

                        switch(choice) {

                                case 1:{
                                               System.out.println("Enter Data : ");
                                               int data = sc.nextInt();
                                               q.enqueue(data);
                                        }break;
                                case 2:{

                                               int data = q.dequeue();
                                               if(q.flag == 0) {
                                                       System.out.println("Queue Empty");
                                               }else {
                                                       System.out.println(data + " dequeued");
                                               }
                                        }break;
                                case 3:{
                                               int data = q.front();
                                               if(q.flag == 0) {
                                                       System.out.println("Queue empty");
                                               }else {
                                                       System.out.println(data +" at front");
                                               }
                                        }break;
                                case 4:{

                                               int data = q.rear();
                                               if(q.flag == 0) {
                                                       System.out.println("Queue empty");
                                               }else {
                                                       System.out.println(data + " at rear");
                                               }
                                        }break;
                                case 5:
                                       q.printQueue();

                        }

                        System.out.println("Continue(1/0) : ");
                        ch = sc.nextInt();
                }while(ch==1);
        }
}
