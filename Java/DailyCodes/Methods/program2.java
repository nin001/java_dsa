// functions for add , sub, mult , div
import java.util.*;

class MethodDemo{

	static void add(int a , int b){
		System.out.println("Addition : "+(a+b));
	}
	static void sub(int a , int b){
		System.out.println("Subtraction : "+(a-b));
	}
	static void  mult(int a , int b){
		System.out.println("Multiplication : "+(a*b));
	}
	static void div(int a , int b){
		System.out.println("Division : "+(a/b));
	}
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter int values : ");
		
		int a = input.nextInt();
		int b = input.nextInt();

		System.out.println("Values of operations");
		add(a,b);
		sub(a,b);
		mult(a,b);
		div(a,b);
	}
}
