//Return values of function (using in SOP)

class MethodDemo{
	int fun1(int x){
		return x*2;
	}

	void fun2(int x){
		int ans = x*2;
	}

	public static void main(String args[]){
		System.out.println("Start of program");
		MethodDemo obj = new MethodDemo();

		System.out.println(obj.fun1(10));
//		System.out.println(obj.fun2(10));
	}
}
/*
 program9.java:17: error: 'void' type not allowed here
		System.out.println(obj.fun2(10));
		                           ^
1 error
*/
