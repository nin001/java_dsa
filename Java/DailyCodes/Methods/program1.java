// Methods are the functions that are written in the class context
// There are two types of methods in java
// 1. Static method - Static method are declared using static keyword and are 
// accessible in main(Static context
// 2. Non-Static method - Methods that are not declared as static This methods
// cannot be accessed in static context directly

class MethodDemo{
	static void fun(){
		System.out.println("In fun");
	}

	void gun(){
		System.out.println("In gun");
	}
	public static void main(String args[]){
		System.out.println("In main");
		fun();
		//gun();
	
		MethodDemo obj = new MethodDemo();
	     	obj.gun();	
	}
}
/*
 program1.java:19: error: non-static method gun() cannot be referenced from a static context
		gun();
*/
