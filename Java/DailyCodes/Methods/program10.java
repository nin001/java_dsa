// Arguments differ in calling function

class MethodDemo{
	void fun(int x){
		System.out.println("value of x : "+x);
	}
	public static void main(String args[]){
		System.out.println("Start of prog");
		MethodDemo obj = new MethodDemo();
		
		obj.fun();
		obj.fun(10,20);
	}
}
