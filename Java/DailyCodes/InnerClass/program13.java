// constuctor inside inner class

class Outer {
	class Inner {
		int var1 , var2;
		Inner(int var1 , int var2) {
			this.var1 = var1;
			this.var2 = var2;

			System.out.println("Inner constructor");
			System.out.println(this);
			System.out.println(Outer.this);
		}
	}
}

class Client {
	public static void main(String arg[]) {
		Outer.Inner obj = new Outer().new Inner(100,400);
	}
}
