//Inner class
//
//Anonymous inner class
//
//Class with no name

class Demo {

}

class Client {
	public static void main(String args[]) {
		Demo obj = new Demo(){
			void m1() {
				System.out.println("in m1 - annonymous");
			}
		};

		obj.m1();
	}
}
