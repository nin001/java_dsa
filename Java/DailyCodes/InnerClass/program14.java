// Inner class which extends outer class

class Outer {
	Outer() {
		System.out.println("Outer constructor");
	}
	class Inner extends Outer {
		Inner() {
			System.out.println("Inner constuctor");
		}
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();

		Outer.Inner obj1 = obj.new Inner();

		Outer.Inner obj2 = new Outer().new Inner();
	}
}
