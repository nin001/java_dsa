// ways to access methods that are not overriding method
// of parent class of anonymous class

class Demo {
	void gun() {
		System.out.println("in gun-demo");	
	}

}

class Client {
	public static void main(String args[]) {
	  	Demo obj = new Demo() {
			void gun() {
				fun();
			}
			void fun() {
				System.out.println("in fun");
			}
		};

		obj.gun();		//1. accessing using overriding method
//		obj.fun();	cannot find symbol
  
		// returning function
		Demo obj2 = new Demo() {
			void gun() {
				System.out.println("in gun-ano");
			}
			Demo fun() {
				System.out.println("in fun");
				return new Demo();
			}
		}.fun();
	
		obj2.gun();	// calls the gun() methods that is present class Demo
	}
}
