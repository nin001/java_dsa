// Inner class supports static variables but normal static or non static methods
// doesnt support static variables

class Demo {
	int x = 10;
	static int y = 20;

	void fun() {
		int a = 0;
		static int b = 1;			//error
	}

	public static void main(String args[]) {
		int p = 0;
		static int q = 1;			// error
	}
}

/*
 * Even if we use final before the static variable it doesnt work in case of methods
 * and it gives error of 'Illegal start of expression'
 */
