// Inner class
//
// Static nested/static inner class 
//
// Class present inside the outer classes scope but as it is static
// can be accessed using classes name , so while creating object no
// need of creating outers object but just giving reference of the outer

class Outer {
	void m1() {
		System.out.println("in m1");
	}

	static class Inner {
		void m1() {
			System.out.println("in m1 - inner");
		}
	}

}

class Client {
	public static void main(String args[]) {
		Outer.Inner obj = new Outer.Inner();

		obj.m1();

	}
}
