// static inner class

class Outer {
	static class Inner {
		void m1() {
			System.out.println("inner m1");
		}
	}

	void m1() {
		System.out.println("Outer m1");
	}
}

class Client {
	public static void main(String args[]) {
		Outer.Inner obj = new Outer.Inner();
		obj.m1();
	}
}
