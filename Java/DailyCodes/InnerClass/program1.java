// Normal Inner class

class Outer {
	class Inner {
		void methodInner() {
			System.out.println("Inner method");
		}
	}

	void methodOuter() {
		System.out.println("Outer method");
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();

		obj.methodOuter();

		//obj.methodInner();	cannot find symbol

		Outer.Inner obj1 = new Outer().new Inner();		
		/*To create object of inner class , as it is dependent on the outer class
		 * During object creation we need to give the reference of outer class 
		 * and to create object first Outer's object is needed and with the help of
		 * outers object inners object is created*/

		Outer.Inner obj2 = obj.new Inner();

		obj1.methodInner();
		obj2.methodInner();
	}
}
