// Inner class 
//
// class code
// class Outer {
// 	class Inner {
// 		
// 	}
//}
//
// The name of inner class after compilation is Outer$Inner
// And compiler adds constructor of Inner class as Outer$Inner()
// First parameter of the constructor is this but , Inner class also
// has the second parameter as the reference of the outer class , on which
// inner classes object is created

class Outer {
	class Inner {
		void fun2() {
			System.out.println("Inner fun");
		}
	}
	void fun1() {
		System.out.println("Outer fun");
	}
}

class Client {
	public static void main(String args[]) {
		Outer.Inner obj = new Outer().new Inner();

		//Internal call of above line 
		// Outer$Inner(obj,new Outer());
		// 	address of obj and address of Outer obj
		obj.fun2();
	}
}	
