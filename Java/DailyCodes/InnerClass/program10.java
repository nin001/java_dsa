// this$0
//
// this$0 is the second parameter of the default constructor given by the java compiler
// this$0 is declared be compiler that is -> final <outer class> this$<level>
//
// this$0 is used for identifing on which outer classes the inner class's object is created
//
// this$0 is instance variable which gets initialized inside the inner class construtor
//
// this$0 can be accessed inside the class using outer.this

class Outer {
	class Inner {
		void fun() {
			System.out.println(Outer.this);		// this is same to this$0 (in bytecode this$0 getfield is invoked

			System.out.println(this);
		}
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();

		System.out.println("Main Object : "+obj);

		Outer.Inner obj1 =obj.new Inner();

		obj1.fun();
	}
}
/*
 * Output
Main Object : Outer@251a69d7
Outer@251a69d7
Outer$Inner@6b95977
*/
