// Inner class
//
// Method local inner class
//
// Inner class which is present inside the scope of a method 
// present in the outer class

class Outer {
	void m1() {
		System.out.println("in m1");

		class Inner {
			void m1() {
				System.out.println("in m1-inner");
			}
		}

		Inner obj = new Inner();

		obj.m1();
	}

	public static void main(String args[]) {
		Outer obj = new Outer();

		obj.m1();
	}
}

