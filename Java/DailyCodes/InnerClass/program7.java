// Accessing outer variables inside the Inner class

class Outer {
	int x = 10;
	static int y = 20;

	class Inner {
		void fun() {
			System.out.println("In inner fun");
			System.out.println(x);
			System.out.println(y);
//			System.out.println(this$0);
		}
	}
}

class Client {
	public static void main(String args[]) {
		Outer.Inner obj = new Outer().new Inner();

		obj.fun();
	}
}

