// static in inner class

class Outer {
	class Inner {
		final static int x = 10;									
		void fun() {
			System.out.println("Inner fun");
		}
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();
		System.out.println(Outer.Inner.x);
		System.out.println(obj.new Inner().x);
	}
}

/*
 * Reason that static in inner class is not supported because
 * Inner class is dependent on the outer class and as inner class is 
 * non-static /instance , it cannot be accessed without creating object (by law)
 * and to access static variables that can be accessed using class name , which
 * is not possible due to necessity of object creation of inner class*/
