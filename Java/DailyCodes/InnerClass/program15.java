// anonymous inner class
//
// anonymous inner class is used only when the class we want to use is for only
// one time
//
// we create anoymous class as
// Demo obj = new Demo {
// 	anonymous inner class
// };
//
// Here new Demo is replaced with the anonymous classes name 
//
// Anonymous class always extends the class on which reference , the anonymous class
// is written , in above case anonymous classes parent will be Demo class
//
// so above statemnt will be object creation using parent child relation
// so methods using the object obj can call only those methods that are present in
// parent class
//
//should be used when there is need of class which will not be in use furthur and 
//has to override methods on which anonymous class is written
//
class Demo {
	int x = 10;

	void marry() {
		System.out.println("disha patani");
	}
}

class Client {
	public static void main(String args[]) {
		Demo obj = new Demo() {
			void marry() {
				System.out.println("AM <3");
			}
		};

		obj.marry();	//object is of demo but here , anonymous class named Client$1.class method  marry is 
				//invoked
	}
}
