// inner class that implementing an interface

interface Hello {
	void greet();
	void greetSomeone(String name);
}

class Client {
	public static void main(String args[]) {
		Hello english = new Hello() {
			
			public void greet() {
				System.out.println("Hello everyone");
			}

			public void greetSomeone(String name) {
				System.out.println("Hello "+name);
			}
		};

		english.greet();
		english.greetSomeone("Niraj");

		Hello marathi = new Hello() {
			
			public void greet() {
				System.out.println("Namaskar sarwanna");
			}
			public void greetSomeone(String name) {
				System.out.println("Namaskar "+name);
			}
		};

		marathi.greet();
		marathi.greetSomeone("Niraj");
	}
}
