// Returning object outside the method local inner class

interface Demo {
	void m1();
}

class Outer {
	Demo m1() {
		System.out.println("outer m1");
		class Inner implements Demo {
			public void m1() {
				System.out.println("inner m1");
			}
		}
		
		return new Inner();
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();
		Demo obj1 = obj.m1();

		obj1.m1();		// Calling inner m1 method outside the method containing inner class
	}
}
