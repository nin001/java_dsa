// Method local inner class
// 
// method local inner class is used if we want to write a class
// which is dependent on a functionality , that class will come 
// in picture only if we invoke the particular method

class Outer {
	void m1() {
		System.out.println("In m1-outer");

		class Inner {
			void m1() {
				System.out.println("In m1-Inner");
			}
		}

		Inner obj = new Inner();
		obj.m1();		// we can only create object of method local inner class inside the 
					// method only , creating object of method local inner class outside the
					// context of the method is not possible

	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();
		obj.m1();
	}
}

