// static vaiable inside static nested / static inner class
// static block inside static inner class
class Outer {
	static class Inner {
		static int x = 10;

		static {
			System.out.println("Inside static block of the Inner class");
			System.out.println(x);
		}
	}
}

class Client {
	public static void main(String args[]) {
		Outer obj = new Outer();
		Outer.Inner obj1 = new Outer.Inner();
		//System.out.println(Outer.Inner.x);
	}
}

