// Inner class
// main method present in the outer class , so reference of Outer is not necessary
// but creating object (new) needs Outers object as class present inside the scope of 
// outer class can be considered as instance variable and to access instance variable
// in main which is static object creation is necessory

class Outer {
	class Inner {
		void m1() {
			System.out.println("in m1");
		}
	}

	void m2() {
		System.out.println("in m2");
	}

	public static void main(String args[]) {
		Inner obj = new Outer().new Inner();
		//^
		//no reference needed as inner is in the scope of main

		obj.m1();
	}
}
