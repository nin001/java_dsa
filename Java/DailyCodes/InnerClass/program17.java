// constructor in anonymous inner class
// we cannot write constructor in anonymous inner class


class Demo {
	void fun() {
		System.out.println("in fun");
	}
}

class Client {
	public static void main(String args[]) {
		Demo obj = new Demo {
			Client$1() {
				System.out.println("constructor");
			}
		};

		obj.fun();
	}
}
