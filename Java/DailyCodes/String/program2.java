// Identity Hash Code
// when Strings are initialized like this "String str = "<string>";" then they are stored in the 
// String constant pool (SCP) where there are String objects and String initialised in same way with
// string content same 
// Single object is created and references are given to the String object

// String objects created with new , theres object is created in heap and 
// every time new is called new object is created


class StringDemo{
	public static void main(String args[]) {
		String str1 = "Niraj";
		String str2 = "Niraj";

		String str3 = new String("Niraj");
		String str4 = new String("Niraj");
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
		System.out.println(System.identityHashCode(str4));
	}
}
