//More on object creation of Strings

class StringDemo{
	public static void main(String args[]) {
		String str1 = "Niraj";
		String str2 = "Randhir";

		String str3 = str1 + str2;   //this creates new object of string which contains concatinated
					     //strings and its reference is stored in str3
		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	}
}

