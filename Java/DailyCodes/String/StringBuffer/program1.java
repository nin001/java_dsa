// String Buffere 
// - Mutable string and no need of creating new object when changing single character 
// in string object

class StringBufferDemo {
	public static void main(String args[]) {
		StringBuffer sb = new StringBuffer();

		System.out.println(sb.capacity());
		System.out.println(sb);

		sb.append("Niraj");
		
		System.out.println(sb.capacity());
		System.out.println(sb);

		sb.append("Randhir");
		
		System.out.println(sb.capacity());
		System.out.println(sb);

		sb.append("Coder");
		
		System.out.println(sb.capacity());
		System.out.println(sb);
	}
}
