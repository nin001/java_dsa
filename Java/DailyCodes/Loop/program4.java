// Nested while
class WhileDemo{
	public static void main(String a[]){
		int x = 10;
		while(x>=0){
			int y = 1;
			while(y<=5){
				System.out.println("Inner Loop Itr " + y++);
			}
			System.out.println("Outer Loop Itr : " + x--);
		}
	}
}
