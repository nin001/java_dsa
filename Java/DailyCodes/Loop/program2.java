// Nested for

class ForDemo{
	public static void main(String a[]){
		for(int i = 1 ; i<= 5 ; i++){
			for(int j = 1 ; j<=5 ; j++){
				System.out.println("Inner loop iteration " + j);
			}
			System.out.println("Outer Loop Iteration " + i);
		}
	}
}
