// active count and active group count
//
// These two methods are used in getting the count
// of number of threads that are active in a thread group
// which also involves sub groups , and active group count
// is the count of the groups under the invoked groups heirarchy

class MyThread implements Runnable {
	public void run() {
		System.out.println(Thread.currentThread());
		try {
			Thread.sleep(2000);
		}catch(InterruptedException ie) {

		}
	}
}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup grp = new ThreadGroup("Parent");
		ThreadGroup child1 = new ThreadGroup(grp,"Child1");
		ThreadGroup child2 = new ThreadGroup(grp,"Child2");

		MyThread obj1 = new MyThread();
		Thread t1 = new Thread(grp,obj1);

		
		MyThread obj2 = new MyThread();
		Thread t2 = new Thread(child1,obj2);
		
		MyThread obj3 = new MyThread();
		Thread t3 = new Thread(child2,obj3);

		t1.start();
		t2.start();
		t3.start();

		System.out.println(grp.activeCount());
		System.out.println(child1.activeCount());
		System.out.println(child2.activeCount());

		System.out.println("\n"+grp.activeGroupCount());

	}
}
