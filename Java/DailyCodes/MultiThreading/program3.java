/* Start method is responsible to make thread crated execute and
 * invoke the run method in thread java stack
 * Start method is very important method present in the Thread class
 * and is reposible in thread execution.
 * When we override the method in our child class extending Thread class
 * Then when we try to invoke the start method it will execute the 
 * overriding method present in our class and that method will be executed
 * by main thread , So created thread will be of no value.
*/

class MyThread extends Thread {
	public void run() {
		System.out.println("Run method :"+Thread.currentThread().getName());
	}

	public void start() {
		System.out.println("Overriden method start in child class");
		run();
	}
}

class ThreadDemo {
	public static void main(String args[]) {
		MyThread obj = new MyThread();

		obj.start();

		System.out.println("main method : "+Thread.currentThread().getName());
	}
}
