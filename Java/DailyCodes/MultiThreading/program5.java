// Creating Thread using Runnable interface
//
// steps : 
// - Our class implements Runnable interface (which consists only run method)
// - Implement the abstrace method in class
// - While creating object of the class we need the thread class object
// to invoke the start method which is responsible for thread creation and
// also the constructor of thread class which will create thread by calling 
// vm.crate()
// - passing our classes object which is of runnable type(parent-child)
// There is a constructor present inthread class with runnable as a parameter
// - invoking start method

class MyThread implements Runnable {
	public void run() {
		System.out.println("Run : "+Thread.currentThread().getName());
	}
}

class ThreadDemo {
	public static void main(String args[]) {
		
		System.out.println("Main : "+Thread.currentThread().getName());
		MyThread obj = new MyThread();
		Thread t = new Thread(obj);		//invokes Thread(Runnable)

		t.start();			// This will call our start as t
						// also contains our mythread object
	}
}
