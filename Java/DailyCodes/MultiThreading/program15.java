// Yield method in Thread in concerency control
//
// Yield method is very much similar to join , yield() gives
// up the priority to the thread(other thread on which we have 
// invoked yield method) for a single time quantum
//
// It requests to the schedular to schedule other thread for
// a time quantum


class MyThread extends Thread {
	public void run() {
		System.out.println(getName());
	}
}

class ThreadDemo {
	public static void main(String args[]) {
		MyThread obj = new MyThread();

		obj.start();

		obj.yield();

		System.out.println(Thread.currentThread().getName());

	}
}
