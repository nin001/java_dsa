// Same name for thread

class MyThread extends Thread {
	
	MyThread(String name) {
		super(name);
	}

	public void run() {
		System.out.println(Thread.currentThread());

	}
}

class ThreadDemo {
	public static void main(String args[]) {
		MyThread obj = new MyThread("name");
		obj.start();

		MyThread obj2 = new MyThread("name");
		obj2.start();
	}
}
