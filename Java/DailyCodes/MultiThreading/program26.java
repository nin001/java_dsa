// Thread Pool 
//
// Thread Pool is a Pool of Threads where threads are already created
// and stored in a program and as tasks come each thread is assigned to
// a task , Thread creation is a heavy operation and creating thread
// for every task hitting our program will lower our performance
// for this a pool of threads is created and then for tasks threads are
// assigned
//
// Thread Pool in java
//
// ****Thread Pool creators : executors class
// 			present in java.util.concurrent
// 		contains various methods to create Thread pool 
// 		of our requirement
// 	eg - newFixedThreadPool(int)-- creates pool of thread with given
// 	number of threads 
//
// 	- newCachedThreadPool() -- creates threads as tasks hit our program
//
// 	- newSingleThreadExecutor() -- has single thread in the pool and is assigned
// 	to other tasks when first task is executed
//
// **** ExecutionService : These are the manager classes and interfaces which
// 	binds the tasks with the requests and also assigns tasks to the requests
//
// 	List of classses and interfaces
// 	- Executor  -interface(base interface in threadPool)\
// 	- ExecutorService -- interface
// 	- AbstractExecutorService -- class(abstract)
// 	- ThreadPoolExecutor -- class (youngest) --mostly used

// First program

// newFixedThreadPool(int)

import java.util.concurrent.*;

class MyThread implements Runnable {

	int num = 0;

	MyThread(int num) {
		this.num = num;
	}

	public void run() {
		System.out.println("Start "+num);
		System.out.println(Thread.currentThread());
		// in thread pool currentThread prints the object in different format
		// like this [pool-1-Thread-0 prio grp]
	}
}

class ThreadPoolDemo {
	public static void main(String args[]) {
		ExecutorService serv = Executors.newFixedThreadPool(5);
		
		for(int i = 0 ; i<10 ; i++) {
			MyThread obj = new MyThread(i+1);
			serv.execute(obj);		// method present in root interface
		}				// invoked to execute the run in the object
							// parameter of execute() method is
							//Runnable type
		serv.shutdown();	// shutdown method invokation is must, if we do not cal
					// then serv will be waiting for tasks 
	}
}				

