// Multithreading in java
//
// There are three methods that are present in the thread class
// which is used to deal with concerrency problems
// and get more control over execution of the threads
//
// 1.sleep()-- current thread goes to sleep state and other threads
// get priority
//
// 2.join() --- join method on certain thread waits for the
// execution of the thread and gives proprity to the thread and
// waits for its execution
//
// 3.yield()

class MyThread extends Thread {
	public void run() {
		for(int i = 0 ; i<10 ; i++) {
			System.out.println("In Thread- 0");
			try {
				Thread.sleep();
			}catch(InterruptedException ie) {
			}
		}
	}
}

class ThreadDemo {
	public static void main(String args[]) throws InterruptedException {
		MyThread obj = new MyThread();
		obj.start();


		for(int i = 0 ; i<10 ; i++) {
			System.out.println("In Main");
			Thread.sleep();
		}
	}
}
