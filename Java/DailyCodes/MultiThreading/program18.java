// Real Time example 
//
// ThreadGroup

// example Chandrayaan - 3 
// Teams which are involved in mission chandrayaan - 3 
// 1- Management Team
// 2- Engineering Team
// 3- Flight Operations Team

class ManagementTeam extends Thread {

	ManagementTeam(ThreadGroup gp , String nm) { super(gp,nm); }

	public void run() {
		System.out.println("||||Management team||||");
		System.out.println("Responsible for Planning and management of the missions");
		
	}

	private void manage() {
		//Method for management which 
		//consists management functionality
	}
}

class EngineeringTeam extends Thread {

	EngineeringTeam(ThreadGroup gp , String nm) { super(gp,nm); }
	public void run() {
		System.out.println("||| Engineering Team|||");
		System.out.println("Responsible for Design and Engineering SpaceCraft");
	}

	private void spaceCraft() {
		// information and calculations
		// about space craft and satellite
	}
}

class FlightOperationsTeam extends Thread {

	FlightOperationsTeam(ThreadGroup gp , String nm) { super(gp,nm); }
	public void run() {
		System.out.println("||| Flight operations team |||");
		System.out.println("Responsible for Orbiting path calculations");
	}
}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup chandrayaan = new ThreadGroup("Chandrayaan-3");

		ThreadGroup Management = new ThreadGroup(chandrayaan,"Management");
		ManagementTeam emp = new ManagementTeam(Management,"Employee1");

		ThreadGroup Engineering = new ThreadGroup(chandrayaan,"Engineering");
		EngineeringTeam emp2 = new EngineeringTeam(Engineering,"Employee2");

		ThreadGroup FlightOperation = new ThreadGroup(chandrayaan,"Flight Opeations");
		FlightOperationsTeam emp3 = new FlightOperationsTeam(FlightOperation,"Employee3");
	
		emp.start();
		emp2.start();
		emp3.start();
	}
}
