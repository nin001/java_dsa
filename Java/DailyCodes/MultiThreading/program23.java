// destroy() method
//
// to clean and destroy the thread group ,
// conditions - > ThreadGroup must be empty
// 		> execution of the threads in the grp must be completed

class MyThread extends Thread {
	MyThread(ThreadGroup gp , String nm) { super(gp,nm); }

	public void run() {
		System.out.println(getName());
	}
}

class ThreadGroupDemo {
	public static void main(String args[])throws InterruptedException {
		ThreadGroup gp1 = new ThreadGroup("niraj");

		MyThread t1 = new MyThread(gp1,"t-1");
		MyThread t2 = new MyThread(gp1,"t-2");

		t1.start();
		t2.start();

		
		gp1.destroy();	 // depricated method which cannot be called
	}
}
