// Real time example 
//
// Join method

class Aptitude extends Thread {
	public void run() {
		System.out.println("Aptitude Test");
	}
}

class Interviews extends Thread {
	public void run() {
		Aptitude obj = new Aptitude();
		obj.start();
		try{
			obj.join();
		}catch(InterruptedException ie) {}
		System.out.println("Interview process");
	}
}

class JobOffer extends Thread{
	public void run() {
		Interviews obj = new Interviews();
		obj.start();
		try{
			obj.join();
		}catch(InterruptedException ie){}
		System.out.println("Offered Job");
	}
}

class ThreadDemo {
	public static void main(String args[])throws InterruptedException {
		
		JobOffer obj = new JobOffer();
		obj.start();

		obj.join();
		System.out.println("Party");
	}
}

