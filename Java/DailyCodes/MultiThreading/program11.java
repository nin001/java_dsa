// join() --- join method on certain thread waits for the
// execution of the thread and gives proprity to the thread and
// waits for its execution
//

class MyThread extends Thread {
	public void run() {
		for(int i = 0 ; i<10 ; i++) {
			System.out.println("In Thread- 0");
		}
	}
}

class ThreadDemo {
	public static void main(String args[]) throws InterruptedException {
		MyThread obj = new MyThread();
		obj.start();

		obj.join();	  // wait untill exec of obj thread that is thread-0

		for(int i = 0 ; i<10 ; i++) {
			System.out.println("In Main");
		}
	}
}
