// What name is given when we create a thread assigning constructor
// and then using no-arg constructor

class MyThread extends Thread {
	
	MyThread() {
		super();
	}

	MyThread(String name) {
		super(name);
	}

	public void run() {
		System.out.println(getName());
	}
}


class ThreadDemo {
	public static void main(String args[]) {
		MyThread obj = new MyThread("Name 1");
		MyThread obj2 = new MyThread("Name 2");
		MyThread obj3 = new MyThread();
/** Name of the thread that is created after creating name1 and name2 thread 
 * The name for thread created in obj3 is Thread-0 
 * This is done because count of the threads are maintained for those only
 * who are created using no-arg constructor of the Thread class
 */
		obj.start();
		obj2.start();
		obj3.start();
	}
}

