// isDaemon() and setDaemon(boolean); 
//
// Daemon Thread group is a group of threads which are
// supporting group for the main program execution 
// These thread groups get terminated automatically when
// the last thread is stopped

class MyThread extends Thread {

	MyThread(ThreadGroup gp , String n) { super(gp,n); }
	public void run() {
		System.out.println(Thread.currentThread().getName());

	}
}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup gp1 = new ThreadGroup("G1");
		ThreadGroup gp2 = new ThreadGroup("G2");

		System.out.println(gp1.isDaemon());
		System.out.println(gp2.isDaemon());

		MyThread obj1 = new MyThread(gp1,"1");
		MyThread obj2 = new MyThread(gp1,"2");

		MyThread obj3 = new MyThread(gp2,"3");
		MyThread obj4 = new MyThread(gp2,"4");

		gp1.setDaemon(true);
		
		System.out.println(gp1.isDaemon());
		System.out.println(gp2.isDaemon());

		obj1.start();
		obj2.start();
		obj3.start();
		obj4.start();


	}
}
