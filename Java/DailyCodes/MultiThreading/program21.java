// Thread Group Methods to try
//1 - Interrupt
//2 - isDeamon
//3 - setDeamon
//4 - destroy
//5 - resume
//6 - suspend
//7 - stop
//8 - activeCount
//9 - activeGroupCount


// Method - interrupt
// non-static method cannot be invoked using class name
// return type void

class MyThread extends Thread {
	
	MyThread(ThreadGroup grp , String name) { super(grp,name); }

	public void run() {
		for(int i = 0 ; i<10 ; i++) {
			System.out.println(Thread.currentThread().getName() + i);
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {
				System.out.println("Interrupted thread that was in sleep");
			}
		}
	}
}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup grp1 = new ThreadGroup("MyGroup");

		MyThread t1 = new MyThread(grp1,"One");
		MyThread t2 = new MyThread(grp1,"Two");

		MyThread t3 = new MyThread(Thread.currentThread().getThreadGroup(),"main one");
		MyThread t4 = new MyThread(Thread.currentThread().getThreadGroup(),"main two");

		t1.start();
		t2.start();
		t3.start();
		t4.start();
		
		System.out.println("Inturrupting grp1");
		grp1.interrupt();	//used to interrupt all the threads present in the Threadgroup
	}
}


