// resume and stop and suspend
//
// Methods used to stop the exectution of the
// threadGroup it is depricated method and gives compile time warining
// resume the execution of the group

class MyThread implements Runnable {

	public void run() {
		System.out.println("Execution of thread");
		for(int i = 0 ; i<10 ; i++) {
			System.out.println(i+1);
			try {
				Thread.sleep(10);
			}catch(InterruptedException ie) {

			}
		}
	}
}

class ThreadGroupDemo {
	public static void main(String args[])throws InterruptedException {
		ThreadGroup grp = new ThreadGroup("Niraj");

		MyThread obj = new MyThread();
		Thread t1 = new Thread(grp,obj,"Thread");
		
		t1.start();

		Thread.sleep(10); 
		//grp.stop();	//stops execution of the thread(depricated

		grp.suspend();

		System.out.println("Thread grp execution stopped and main execution started");
		for(int i = 0 ; i<10 ; i++) {
			System.out.println(i*10);
		}

		System.out.println("Main execution over and thread grp resumed");
		grp.resume();
	}
}
