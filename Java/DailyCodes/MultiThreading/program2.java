// Need of overriding the run method
//
// run method is present in the The thread class
// which contains no - code , that means if we do
// not override the run method , Thread that we have
// created will start execution of run method present in
// the Parent(thread) class . So if we are creating thread
// we also need to assign the method respective task
// with the help of overriding the run method and giving 
// our own implementation

class MyThread extends Thread {
	/*public void run() {
		System.out.println(Thread.currentThread().getName());
		System.out.println("In thread");
	}*/ //If we donot override the run method , our run method will go to dead state
	    //Without executing any task
}

class ThreadDemo {
	public static void main(String args[]) {
		MyThread obj = new MyThread();
		obj.start();

		System.out.println(Thread.currentThread().getName());
		System.out.println("In main");
	}
}
