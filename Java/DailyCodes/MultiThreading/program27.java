// giving thread to execute method as parameter

import java.util.concurrent.*;

class MyThread extends Thread {
	public void run() {
		System.out.println(Thread.currentThread());
	}
}

class ThreadPoolDemo {
	public static void main(String args[]) {
		ExecutorService serv = Executors.newCachedThreadPool();

		for(int i = 0 ; i<10 ; i++) {
			MyThread obj = new MyThread();
			serv.execute(obj);
		}
		serv.shutdown();
	}
}
