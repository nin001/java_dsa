// Deadlock scenario in java

class MyThread extends Thread {
	static Thread mainObj = null;

	public void run() {
		try{
			mainObj.join();
		}catch(InterruptedException ie) {
		}
		System.out.println("In thread-0");
	}
}

class ThreadDemo {
	public static void main(String args[])throws InterruptedException {

		MyThread.mainObj = Thread.currentThread();

		MyThread obj = new MyThread();

		obj.start();

		obj.join();
	}
}

