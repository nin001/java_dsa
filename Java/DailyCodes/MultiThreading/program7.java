// Priority of threads
//
// There are default priotities assigned to the threads , 
// there are three priotity variables present in the thread
// class -
// normal priority - which is 5
// min priority - which is 1 
// max priority - which is 10
// That is priority ranges between 1-10

class MyThread extends Thread {
	public void run() {
		System.out.println("Run -"+Thread.currentThread().getName()+" "+Thread.currentThread().getPriority());
	}
}

class ThreadDemo {
	public static void main(String args[]) {
		System.out.println("main Thread : " + Thread.currentThread().getPriority());
		MyThread obj = new MyThread();
		obj.start();

		System.out.println("Setting main threads priority");
		Thread.currentThread().setPriority(7);	// priority must be between 1-10
							// or exception is thrown
							// IllegallArgumentException

		MyThread obj2 = new MyThread();
		obj2.start();

	}
}
