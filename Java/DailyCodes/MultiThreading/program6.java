// Advantage of using Runnable interface as parent is 
// that we can implement Runnable and as well as , we can
// extend a class with rich functionality and use their methods

class Rich {
	//consists 200 methods and powerful methods
}

class MyThread implements Runnable extends Rich {
	public void run() {
		// invokation of methods present in Rich class
	}
}
