// To avoid deadlocks in java threads
// join with paramters is used ,which would wait for the
// thread execution , for the given parameter

class MyThread extends Thread {
	static Thread mainObj = null;
	public void run() {
		try {
			mainObj.join(1000);
		}catch(InterruptedException ie){}
		System.out.println("In thread-0");
	}
}

class ThreadDemo {
	public static void main(String args[])throws InterruptedException {
		MyThread.mainObj = Thread.currentThread();

		MyThread obj = new MyThread();
		obj.start();

		obj.join(1000);
		System.out.println("In main");
	}
}		
