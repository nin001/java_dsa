
class MyThread extends Thread {
	public void run() {
		System.out.println(Thread.currentThread().getName() + Thread.currentThread().getPriority());
	}
}

class ThreadDemo {
	public static void main(String args[]) {
		Thread t = Thread.currentThread();

		System.out.println(Thread.currentThread().getName() + Thread.currentThread().getPriority());
		MyThread obj = new MyThread();
		obj.start();

		t.setPriority(7);

		System.out.println(Thread.currentThread().getName() + Thread.currentThread().getPriority());
		MyThread obj1 = new MyThread();
		obj1.start();
	}
}
