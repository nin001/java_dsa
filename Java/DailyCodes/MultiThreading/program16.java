// ThreadGroup
//
// Thread group is a class in the java lang package ,
// It is used to create a thread group and under that 
// group there could present multiple threads performing
// similar functionality .
// Thread group is used to simplify operations like , notifying 
// threads or terminating the thread process , If ThreadGroups were
// not present then , we would have required to operate on each and
// every thread individually , But using thread group , we can operate
// on whole group using the ThreadGroup and notify  using groupname only

// Creating ThreadGroup in java

class MyThread extends Thread {
	
	MyThread(ThreadGroup grp , String name) {
		super(grp,name);	// Paramters should reach Thread constructor
					// so that threads should be created under a 
					// ThreadGroup with name

	}

	public void run() {
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup grp1 = new ThreadGroup("Core2Web");

		// To create a thread under a user defined group 
		// We need to keep in mind few things
		// 1- Our class should contain constructor with
		// ThreadGroup and string parameter
		// 2 - String paramter should be given the name 
		// of the thread creating
		// ** Name of the thread is given so that There 
		// should not be ambiguity of thread with same 
		// names and if we are creating Thread groups we 
		// should also be handling the thread names

		MyThread obj1 = new MyThread(grp1,"Java");
		MyThread obj2 = new MyThread(grp1,"c");
		MyThread obj3 = new MyThread(grp1,"c++");

		obj1.start();
		obj2.start();
		obj3.start();
	}
}
