// MulltiThreading in java
//
// To create thread in java there are two ways
// 1) Thread class - By extending Thread class in our class and
// overriding rum method which the thread will execute
//
// 2) Runnable interface = By implementing Runnable interface in 
// our class

//First way
class  MyThread extends Thread {
	public void run() {	// we cannot write throws exception because overriden method doesnt throw any exception
		System.out.println("Thread name : "+Thread.currentThread().getName());

		for(int i = 0 ; i<10 ; i++) {
			System.out.println("thread");
			try {
				Thread.sleep(1000);
			}catch(InterruptedException obj) {

			}
		}
	}
}

class ThreadDemo {
	public static void main(String args[]) throws InterruptedException {

		MyThread obj = new MyThread();

		obj.start();

		System.out.println("Thread name : "+Thread.currentThread().getName());

		for(int i = 0 ; i<10 ; i++) {
			System.out.println("Main");
			Thread.sleep(1000);
		}

	}
}
		
