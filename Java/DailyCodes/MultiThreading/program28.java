// newCachedThreadPool

import java.util.concurrent.*;

class MyThread implements Runnable {
	public void run() {
		System.out.println(Thread.currentThread());
		try {
			Thread.sleep(1000);
		}catch(InterruptedException ie) {
		}
	}
}

class ThreadPoolDemo {
	public static void main(String args[]) {
		ExecutorService serv = Executors.newCachedThreadPool();

		for(int i = 0 ; i<10 ; i++) {
			MyThread obj = new MyThread();
			serv.execute(obj);
		}
		serv.shutdown();
	}
}
