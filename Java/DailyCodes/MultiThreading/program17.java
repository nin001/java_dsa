// Child ThreadGroup in java

class MyThread extends Thread {
	
	MyThread(ThreadGroup grp , String name) {
		super(grp,name);
	}

	public void run() {
		System.out.println(Thread.currentThread());
	}

}

class ThreadGroupDemo {
	public static void main(String args[]) {
		ThreadGroup grp1 = new ThreadGroup("Core2Web");

		MyThread obj1 = new MyThread(grp1,"java");
		obj1.start();

		ThreadGroup child = new ThreadGroup(grp1,"Incubators");

		MyThread obj2 = new MyThread(child,"Flutter");
		obj2.start();
	}
}
