// Real time example of Inheritance

class Universe {
	int noOfGalaxies_inBillion = 200;

	Universe() {
		System.out.println("Number of Galaxies : "+noOfGalaxies_inBillion+"Billion");
	}
}

class MilkyWay extends Universe {
	int noOfStars = 100;

	MilkyWay() {
		System.out.println("Number of Stars in MilkyWay : "+noOfStars+"Billion");
	}
}

class SolarSystem extends MilkyWay {
	int noOfPlanets = 8;

	SolarSystem() {
		System.out.println("Number of Planets in Solar System : "+noOfPlanets);
	}
}

class Earth{
	public static void main(String[] args) {
		SolarSystem obj = new SolarSystem();
	}
}
