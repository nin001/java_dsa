// Access specifier inheritance 
// private , protected , public and default

class Parent {

	private int x = 10;
	protected String str = "Niraj";
	Parent() {
		System.out.println("Parent constructor");
	}

	private void access1() {
		System.out.println("Access1");
		System.out.println(x);
		System.out.println(str);
	}

	protected void access2() {
		System.out.println("Access2");
		System.out.println(x);
		System.out.println(str);
	}
}

class Child extends Parent {
	Child() {
		System.out.println("Child constructor");
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();

		System.out.println(obj.x);
		System.out.println(obj.str);
		obj.access1();
		obj.access2();
	}
}

//OUTPUT
//program3.java:35: error: x has private access in Parent
//                System.out.println(obj.x);
//                                     ^
// program3.java:37: error: cannot find symbol
//                 obj.access1();
//                    ^
//  symbol:   method access1()
//  location: variable obj of type Child
//2 errors
