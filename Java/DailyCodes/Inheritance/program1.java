// Inheritance is a concept where methods and variables that can be common in multiple classes
// is written in one class and other classes are declared its child
// Inheritance is relation between 2 classes (parent-child) relation and due to this relation
// methods and variables (instance) are inherited to the extended (child) class
//
// ***Points to remember
// > Access specifier of class and constructor are same i.e. the specifier of class we give applies to 
// the constructor 
// > Access specifier of class cannot be private & protected
// > Valid Specifiers to class that we can give is - public and default
// > Modifier static is not valid to the class as well as constructor

class ICC {
	ICC() {
		System.out.println("In Icc constructor");
	}
}

class BCCI extends ICC {
	BCCI() {
		System.out.println("In Bcci constructor");
	}
}

class Client {
	
	public static void main(String[] args) {
		BCCI obj = new BCCI();
	}
}

/*OUTPUT
 * In Icc constructor
 * In Bcci construtor
We create object of BCCI class which is child class of ICC 
Both the class have no-arg constructor and we create object of BCCI class
which have invokespeacial in the constructor which calls the ICC constructor
which further calls object constructor

This is done to initialize instance variables of parent classes
that are inherited (access is given) and that is the reason to call 
every parent classes constructor
*/
