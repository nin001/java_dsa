//Inheritance 
// Combination of instance and static variables and methods

class Parent {
	int x = 10;
	static int y = 20;

	static {
		System.out.println("Parent Static block");
	}

	Parent() {
		System.out.println("Parent constructor");
	}
	
	void methodOne() {
		System.out.println(x);
		System.out.println(y);
	}

	static void methodTwo() {
		System.out.println(y);
	}
}

class Child extends Parent {
	static {
		System.out.println("Child static block");
	}

	Child() {
		System.out.println("Child constructor");
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
		obj.methodOne();
		obj.methodTwo();
	}
}
