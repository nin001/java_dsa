// Instance variable and functions inheritance in the classes

class Parent {

	int x = 10;

	Parent() {
		System.out.println("In parent constructor");
	}
}

class Child extends Parent {
	int y = 20;
	Child() {
		System.out.println("In child constructor");
	}

	void access() {
		System.out.println(x);
		System.out.println(y);
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
		obj.access();
	}
}

