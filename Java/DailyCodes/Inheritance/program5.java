// Inheritance
// Static variables

class Parent {
	static int x = 10;

	static {
		System.out.println("Parent static block");
	}
	static void access() {
		System.out.println(x);
	}
}

class Child extends Parent {
	static {
		System.out.println("Child static block");
		System.out.println(x);
		access();
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
	}
}
