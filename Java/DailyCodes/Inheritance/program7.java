// Inheritance 
// Using super to access parents variables and methods

class Parent {
	int x = 10;
	static int y = 20;

	Parent() {
		System.out.println("In parent constructor");
	}
}

class Child extends Parent {
	int x = 100;
	static int y = 200;

	Child() {
		System.out.println("In child constructor");
	}

	void access() {
		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
		obj.access();
	}
}

/*
 Super Keyword :
 Using super keyword , we can access parent classes variables and methods
 Super keyword is a typecast of this to its parent 
 compiler puts Parents name in the place of super to access parents members
 */
