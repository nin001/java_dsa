//Static in Inheritance

//static block in inheritance

class Parent {
	Parent() {
		System.out.println("Parent constructor");
	}
	static {
		System.out.println("In parent static block");
	}
}

class Child extends Parent {
	Child() {
		System.out.println("Child constructor");
	}
	static {
		System.out.println("In child static block");
	}
}

class Client {
	public static void main (String[] args) {
		Child obj = new Child();
	}
}
