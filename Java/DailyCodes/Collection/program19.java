// Predefined classes implements Comparable interface 
// which helps compare the contents within
//
// So even we create new objects and pass to the set 
// Duplicate values would not be allowed but , in Case of 
// User Defined we need to make our class comparable
// and also define the logic by overriding method compareTo

import java.util.*;

class HashSetDemo {
	public static void main(String args[]) {
		HashSet hs = new HashSet();

		hs.add(10);
		hs.add(20);
		hs.add(new Integer(10));
		hs.add(new Integer(20));
// Internally in add method Only objects could be stored so , in place of 10
// new Integer(10) is called and as Integer class is Comparable type , contents
// are compared and only then Objects are Inserted in the Collection(set)
		System.out.println(hs);
	}
}
