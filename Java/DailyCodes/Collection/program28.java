// LinkedHashMap
//
// - Stores key value pair 
// - Preserves insertion order

import java.util.*;

class LinkedHashMapDemo {
	public static void main(String args[]) {
		LinkedHashMap lhm = new LinkedHashMap();

		lhm.put("Kanha","info");
		lhm.put("Badhe","Barclays");
		lhm.put("Ashish","BMC");
		lhm.put("Rahul","PTC");

		System.out.println(lhm);
	}
}
