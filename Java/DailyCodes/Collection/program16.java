// User Defined Classes , Check contents (same object not allowed)

import java.util.*;

class MyClass extends TreeSet implements Comparable {
	String str = null;

	MyClass(String str) {
		this.str = str;
	}

	public boolean add(Object o) {
		Iterator itr = Demo.ts.iterator();
		while(itr.hasNext()) {
			if(itr.next().equals(((String)o).equals(str)))
				return false;
		}
		super.add(o);
		return true;
	}

	public int compareTo(Object o) {
		return 0;
	}
}

class Demo {
	static TreeSet ts = new TreeSet();
	public static void main(String args[]) {

		ts.add(new MyClass("Niraj"));
		ts.add(new MyClass("Akash"));
		ts.add(new MyClass("Niraj"));

		System.out.println(ts);
	}
}
