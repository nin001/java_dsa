// Deque (interface)
//
// - double ended queue
// - can perform operations on both ends

import java.util.*;

class DequeDemo {
	public static void main(String args[]) {
		Deque d = new ArrayDeque();

		d.offer(10);
		d.offer(40);
		d.offer(20);
		d.offer(30);

		System.out.println(d);

		//offerFirst() and offerLast()
		d.offerFirst(5);
		d.offerLast(50);

		System.out.println(d);

		//pollFirst() and pollLast()
		d.pollFirst();
		d.pollLast();

		System.out.println(d);

		// peekFirst() and peekLast()
		System.out.println(d.peekFirst());
		System.out.println(d.peekLast());

		System.out.println(d);

		//Iterator
		Iterator itr = d.iterator();
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}

		//descendingIterator
		Iterator dItr = d.descendingIterator();
		while(dItr.hasNext()) {
			System.out.println(dItr.next());
		}

	}
}
