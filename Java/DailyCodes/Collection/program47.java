//ArrayDeque
//
//Methods;
// - inc(int,int)
// - inc(int,int,int)
// - dec(int,int)
// - sub(int,int,int)
// - elementAt(Object[] , int)
// - nonNullElementAt(Object[] , int)

import java.util.*;

class ArrayDequeDemo {
	public static void main(String args[]) {
		ArrayDeque ad = new ArrayDeque();

		ad.offer(4);
		ad.offer(2);
		ad.offer(3);
		ad.offer(1);
		ad.offer(5);
/*
		System.out.println(ad);

		ad.offerFirst(-5);
		ad.offerLast(7);

		System.out.println(ad);

		ad.pollFirst();
		ad.pollLast();
		System.out.println(ad);

		System.out.println(ad.peekFirst());
		System.out.println(ad.peekLast());
*/

		System.out.println(ArrayDeque.inc(2,5,8));
		System.out.println(ArrayDeque.dec(2,5));
		System.out.println(ArrayDeque.sub(2,5,8));
		
		
	}
}
