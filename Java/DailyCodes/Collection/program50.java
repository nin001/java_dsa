// Comparator in priorityBlockingQueue

import java.util.concurrent.*;
import java.util.Comparator;

class User {
	String name;
	User(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}

class SortByName implements Comparator {
	public int compare(Object obj1 , Object obj2) {
		return ((User)obj1).name.compareTo(((User)obj2).name);
	}
}

class Client {
	public static void main(String args[]) {
		PriorityBlockingQueue pq = new PriorityBlockingQueue(5,new SortByName());

		pq.put(new User("Niraj"));
		pq.put(new User("Atharva"));
		pq.put(new User("Aakash"));
		pq.put(new User("Yash"));

		System.out.println(pq);
	}
}
