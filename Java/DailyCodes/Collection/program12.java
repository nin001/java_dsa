// Multhreading Vector code

import java.util.*;

class MyThread extends Thread {
	public synchronized void run() {
		System.out.println("Processing "+ Thread.currentThread().getName());
		for(int i = 0 ; i<5 ; i++) {
			VectorDemo.v.addElement(Thread.currentThread().getName() + i);
		}
		System.out.println(VectorDemo.v);
	}
}

class VectorDemo {

	static Vector v = new Vector();

	public static void main(String args[]) {

		MyThread t1 = new MyThread();
		MyThread t2 = new MyThread();
		MyThread t3 = new MyThread();
		MyThread t4 = new MyThread();

		t1.start();
		t2.start();
		t3.start();
		t4.start();

	}
}
