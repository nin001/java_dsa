// Using comparable interface , Using user defined 
// classes as a entry in TreeMap
//
// For User defined class to work in TreeMap 
// It either needs to pass a comparator to the constructor
// or it needs to be of comparable type

// Comparable interface is a interface which has only one method
// compareTo()

import java.util.*;

class Employee implements Comparable {
	String name;
	int id;

	Employee(String name , int id) {
		this.name = name;
		this.id = id;
	}

	public String toString() {
		return "{" + id + " : " + name + "}";
	}
// To sort Map compared on the names , This compare to is invoked
// but if we want to sort on the bassis of ids , we cannot overload
// this method , which a drawback of comparable , which makes it rigid 
// that Entries will get sorted by only one way

	public int compareTo(Object obj) {
		return (name.compareTo(((Employee)obj).name));
	}
}

class TreeMapDemo {
	public static void main(String args[]) {
		TreeMap tm = new TreeMap();
		
		tm.put(new Employee("Niraj",1001),"IT/Software");
		tm.put(new Employee("Deep",1001),"Civil");
		tm.put(new Employee("Devang",1004),"Law");
		tm.put(new Employee("Akansha",1005),"Accounts");
		tm.put(new Employee("Akash",1008),"Creative");

		System.out.println(tm);
	}
}
