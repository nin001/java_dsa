// Navigable set
//
// Child interface of SortedSet
// Direct parent of TreeSet

// Methods : 
/*  public abstract E lower(E);
  public abstract E floor(E);
  public abstract E ceiling(E);
  public abstract E higher(E);
  public abstract E pollFirst();
  public abstract E pollLast();
 */

import java.util.*;

class NavigableSetDemo {
	public static void main(String args[]) {
		NavigableSet ns = new TreeSet();	// Reference of Navigableset
							// Can only call methods that are
							// present in the NavigableSet
		
		ns.add(10);
		ns.add(20);
		ns.add(30);
		ns.add(40);
		ns.add(50);

		System.out.println(ns);

		// lower
		System.out.println(ns.lower(40));
		// Element that is lower to the Element is returned
		// if element is not present in the Collection then null is returned

		//floor
		System.out.println(ns.floor(51));
		// No need of argument Element should be present in the collection
		// Element lower than element and closest to the element is returned


		//ceiling
		System.out.println(ns.ceiling(15));
		// No need of argument Element should be present in the collection
		// Element that is heigher and closest to the element is returned

		//higher
		System.out.println(ns.higher(40));
		// Element should be present in the collection or null returned
		// Element heigher than Argument is returned
	}
}
