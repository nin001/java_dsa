// Adding elements our user Defined classes object

import java.util.*;

class CompanyInfo {

	String name = null;
	int empId = 0;

	CompanyInfo(String name , int empId) {
		this.name = name;
		this.empId = empId;
	}

	// if we are adding user defined class in the collection
	// while printing , we would get our class objects address
	// instead of getting string as other predefinded classes
	// Due to , while printing implicit call to .toString to our
	// object is done, but in our class Inherited toString method is
	// present , which wont print anything
	
	public String toString() {
		return name + " : " + empId;
	}
}

class Demo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		al.add(new CompanyInfo("Niraj",1001));
		al.add(1,new CompanyInfo("Yash",1002));
		al.add(new CompanyInfo("Sainath",1003));

		System.out.println(al);
	}
}

/** Output without toString overriding
 * [CompanyInfo@6ff3c5b5, CompanyInfo@3764951d, CompanyInfo@4b1210ee]
 ** Outout with toString
 [Niraj : 1001, Yash : 1002, Sainath : 1003]
*/
