// ListIterator
//
// ListIterator is a Bidirectional cursor which can be only
// applied to the List Collections (ArrayList , LinkedList , Vector , Stack)
//
// methods: 
// hasNext()
// next()
// hasPrevious()
// previous()
// nextIndex()
// previousIndex()
// add()
// remove()
// set()

import java.util.*;

class Element {
	String data = null;
	Element(String data) {
		this.data = data;
	}

	public String toString() {
		return this.data;
	}
}

class ListIteratorDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		al.add(new Element("One"));
		al.add(new Element("Two"));
		al.add(new Element("Three"));

		ListIterator listItr = al.listIterator();

		while(listItr.hasNext()) {
			System.out.println(listItr.next());
		}

		System.out.println("Previous method");

		while(listItr.hasPrevious()) {
			System.out.println(listItr.previous());
		}

	}
}
