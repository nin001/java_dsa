// WeakHashMap
//
// HashMap dominates garbage collector and even there exist 
// objects that have no reference , but then also , Garbage collector
// cannot free objects that are part of HashMap
//
// This is the reson weakHashMap was bought in picture to allow gc
// to free objects that are part of map

import java.util.*;

class Demo {
	String str;
	Demo(String str) {
		this.str = str;
	}

	public String toString() {
		return str;
	}

	// Garbage collector invoked finalize method to notify that
	// our classes object is freed
	public void finalize() {
		System.out.println("Notify");
	}


}

class GCDemo {
	public static void main(String args[]) {
		Demo obj1 = new Demo("Core2Web");
		Demo obj2 = new Demo("Biencaps");
		Demo obj3 = new Demo("Incubator");

		WeakHashMap wm = new WeakHashMap();
		wm.put(obj1,2016);
		wm.put(obj2,2019);
		wm.put(obj3,2023);

		obj1 = null;
		obj2 = null;

		System.gc();

		System.out.println("in main");
// if we used hashMap instead of weakhashmap then notify msg wouldnt print 
// meaning gc couldnt free our objects
// Butas we use weakhashmap 2 notify msgs would print
		System.out.println(wm);
	}
}
