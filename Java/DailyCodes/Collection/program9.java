// iterator 

import java.util.*;

class IteratorDemo {
	public static void main(String args[]) {
		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addFirst(5);

		Iterator itr = ll.iterator();	//returns the object of Iterator 

		while(itr.hasNext()) {
			if((Integer)itr.next() == 30) {
				itr.remove();
			}
			System.out.println(itr.next());
		}
	}
}
