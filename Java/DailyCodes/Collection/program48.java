// Blocking queue (interface)
//
// Blocking queue is a bounded type of queue which has some
// additional features 
//
// - Fixed size
// - Thread safe , Queue locks itself and only one thread can operate
// at a time
//
// Heirarchy
// 	Queue(interface)
// 	 |
// 	BlockingQueue(Interface)
// 	  |
//	  |- ArrayBlockingQueue
//	  |- LinkedBlockingQueue
//	  |- PriorityBlockingQueue	

import java.util.concurrent.*;
import java.util.*;

class BQDemo {
	public static void main(String args[])throws InterruptedException {
		BlockingQueue bq = new ArrayBlockingQueue(3);	// Size compulsory

		// put(E) throws InterruptedException
		// 	As it is thread safe , 
		// 	if Queue is empty , 
		// 	waits untill freed
		bq.put(10);
		bq.put(20);
		bq.put(30);

		System.out.println(bq);

//		bq.put(40);	// This will wait (DEADLOCK) as it is single thread
				// There is no thread that will free take() elements from 
				// queue 
		
		//Overloaded method in BlockingQueue 
		//Waits for given time and then discards the element
		//if space is not made
		bq.offer(40,5,TimeUnit.SECONDS);
		System.out.println(bq);


		bq.take();		// First element is removed
					// take() throws Interrupted exception
					// As ThreadSafe
					// If empty queue, then waits for other
					// thread to put()
		System.out.println(bq);

		bq.put(40);

		// drainTo()
		// Empties the queue and copies elements in given collection
		// drainTo(Collection)	-> empties queue and copies every element in the Collect
		// drainTo(Collection,int)-> copies given amt(int) of elements in the coll'n

		ArrayList al = new ArrayList();

		System.out.println("ArrayList : " + al);
		System.out.println("Queue : " + bq);
		bq.drainTo(al);
		System.out.println("ArrayList : " + al);
		System.out.println("Queue : " + bq);

/** LinkedBlockingQueue - Methods are same , But as it is Linked, It has no-arg constructor
 * and supports Dynamic queue */


	}
}
