// What is collection framework
//
// Collection framework is a framework consisting multiple 
// interfaces and classes which are used to create collection
// in java
// Collection is storing multiple objects of different types 
// in the name of single variable , a collection can contain 
// multiple types of objects
//
// Collection framework consists pre implemented interfaces classes
// and there methods which can be used directly without actual implementation
// and optimization. That methods and classes are written inside the 
// collection framework which would reduce the work of the programmer

/* Heirarchy of the class
 Collection framework consists two root interfaces 
 Collection and map 
 Collection interface is used to store a single object and node type data
 whereas map is used to store node with key,value pair known as entry

// heirarchy of collection class
// 				Iterable (interface)
// 				|
// 				Collection (interface)
// 				|
// --------------------------------------------------
// |			    |			    |
// List			  Set			Queue
// - ArrayList	 	   | 		           |
// - LinkedList	   ---------------- 	  -----------------
// - Vector	   |		   |	  | 		   |
//    |          Hashset     SortedSet	PriorityQueue	DeQue	and also Blocking Queue
//    Stack	   | 		|         		
//    		 LinkedHashSet  NavigableSet
//    		 		|
//    		 		TreeSet
//
//  Heirarchy of Map 
//  	Map --- HashMap --- LinkedHashMap
//  	|
//  	SortedMap
//  	|
//  	NaviableMap
//  	|
//  	TreeMap
*/


//Arrayset Code as collection example

import java.util.*;

class CollectionDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		al.add(10);
		al.add(20);
		al.add(10);
		al.add("Niraj");

		System.out.println(al);

	}
}

