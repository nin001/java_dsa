// Real time example for comparator

import java.util.*;

class StudentAttendance {
	String name = null;
	int attendance[] = new int[5];
	int attCountWeek = 0;

	StudentAttendance(String name , int attendance[]) {
		this.name = name;
		for(int i = 0 ; i<attendance.length ; i++) {
			this.attendance[i] = attendance[i];
			if(attendance[i] == 1) {
				attCountWeek++;
			}
		}
	}

	public String toString() {
		return "{ Name : "+name+" | Attendance in week : " + attCountWeek + "}\n";
	}
}

class SortByName implements Comparator {
	public int compare(Object obj1 , Object obj2) {
		return ((StudentAttendance)obj1).name.compareTo(((StudentAttendance)obj2).name);
	}
}

class SortByHeighestAttendance implements Comparator {
	public int compare(Object obj1 , Object obj2) {
		return -(((StudentAttendance)obj1).attCountWeek - ((StudentAttendance)obj2).attCountWeek);
	}
}

class Client {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Real time example");

		char ch = 'n';
		ArrayList al = new ArrayList();

		do{
			
			System.out.println("1.Add Student record");
			System.out.println("2.SortByName");
			System.out.println("3.SortByHeighestAttendance");
			System.out.println("4.printRecord");

			System.out.println("Enter choice : ");
			int choice = sc.nextInt();

			switch(choice) {
				case 1:{
					       System.out.println("Enter name  :");
					       String name = sc.next();

					       System.out.println("Enter week attendance (0-ab/1-p) : ");
					       int arr[] = new int[5];
					       for(int i = 0 ; i<arr.length ; i++) {
						       arr[i] = sc.nextInt();
					       }

					       al.add(new StudentAttendance(name,arr));
					       System.out.println(al);
					       
				       }
				       break;
					
				case 2:{
				       Collections.sort(al,new SortByName());
				       System.out.println(al);
				       }break; 
				case 3:{
				       Collections.sort(al,new SortByHeighestAttendance());
				       System.out.println(al);
				       }break;
				case 4:
				       System.out.println(al);
				       break;
				default:
					System.out.println("Invalid choice");
			}

			System.out.println("Do you want to continue (y/n) : ");
			ch = sc.next().charAt(0);
		}while(ch == 'Y' || ch == 'y');
	}
}
