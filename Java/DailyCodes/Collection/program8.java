// Collections of different types such as Lists Sets Queues 
// Are meant to store different types of data as per requirement
// and perform particular operation on the data
//
// To use collection framework and perform operations on the data
// Use of for / for-each loop is very rigid and is complicated
//
// Instead Collection framework have provided Cursors for traversing 
// the collection and perform operations on it
//
// There are 4 Types of Cursors in Java
// 1. Iterator : iterator is a interface which consists methods to traverse
// over collection
// - iterator traverses in unidirectional
// - Iterator works with every Collection (list , set , queue)
// - Iterator is known as Universal Iterator
//
// 2. ListIterator : Child of Iterator
// - Bidirectional traversal
// - Works with only child of List
//
// 3. Enumeration 
// 4. SplitIterator : works on streams (storing or retriving of objects in file)


import java.util.*;

class IteratorDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		for(int i = 0 ; i< 5 ; i++) {
			al.add(i+10);
		}

		Iterator itr = al.iterator();

		while(itr.hasNext()) {
			System.out.println(itr.next());
		}


	}
}
