// Real time example for Key,Value 
//
// User defined class , (Key,Value) 

import java.util.*;

class Plateform {
	String str;
	int userCnt;

	Plateform(String str , int userCnt) {
		this.str = str;
		this.userCnt = userCnt;
	}

	public String toString() {
		return "{" + str + " : " + userCnt + "}";
	}


}

class SortByUserCount implements Comparator {
	public int compare(Object obj1 , Object obj2) {

		return ((Plateform)obj1).userCnt - ((Plateform)obj2).userCnt;

	}
}

class SortByName implements Comparator {

	public int compare(Object obj1 , Object obj2) {

		return (((Plateform)obj1).str.compareTo(((Plateform)obj2).str));
	}
}
class Client {
	public static void main(String args[]) {
		TreeMap tm = new TreeMap(new SortByUserCount());

		tm.put(new Plateform("YouTube",999959),"Google");
		tm.put(new Plateform("MiniTV",334546),"Amazon");
		tm.put(new Plateform("Facebook",496863),"META");
		tm.put(new Plateform("WhatsApp",423684),"META");
		tm.put(new Plateform("ChatGPT",502024),"OpenAI");

		System.out.println(tm);

		TreeMap tm2 = new TreeMap(new SortByName());
		
		tm2.put(new Plateform("YouTube",999959),"Google");
		tm2.put(new Plateform("MiniTV",334546),"Amazon");
		tm2.put(new Plateform("Facebook",496863),"META");
		tm2.put(new Plateform("WhatsApp",423684),"META");
		tm2.put(new Plateform("ChatGPT",502024),"OpenAI");

		System.out.println(tm2);

	}
}
