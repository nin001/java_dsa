// Stack
// Direct Parent - Vector

import java.util.*;

class StackDemo {
	public static void main(String args[]) {
		Stack s = new Stack();

		// methods
		// push()
		// pop()
		// peek()

		s.push(10);
		s.push(20);
		s.push(30);

		System.out.println(s);
	}
}
