// Producer consumer problem

// Uses Blocking queue and multithreading , 
// There exist a producer which adds elements and a consumer
// which deletes elements from the Blocking queue
//
// Producer and consumer are two threads , If queue is full then 
// producer cannot add elements and if queue is empty then consumer
// cannot delete the element
//
// As we are using blocking queue , It is synchronised and thread safe
// so at a time only one thread could operate on the Queue

import java.util.concurrent.*;
import java.util.*;

class Producer implements Runnable {
	BlockingQueue bq;
	Producer(BlockingQueue bq) {
		this.bq = bq;
	}

	public void run() {
		for(int i = 1 ; i<=10 ; i++) {
			try {
				bq.put(i);
				System.out.println("Produced "+i);
			}catch(InterruptedException ie) {

			}
		}
	}
}

class Consumer implements Runnable {
	BlockingQueue bq;
	Consumer(BlockingQueue bq) {
		this.bq = bq;
	}

	public void run() {
		for(int i =  1 ; i<=10 ; i++) {
			try {
				bq.take();
				System.out.println("Consumed "+i);
			}catch(InterruptedException ie) {

			}
		}
	}
}

class Client {
	public static void main(String args[])throws InterruptedException {

		BlockingQueue queue = new ArrayBlockingQueue(3);

		Producer producer = new Producer(queue);
		Consumer consumer = new Consumer(queue);

		Thread producerThread = new Thread(producer);
		Thread consumerThread = new Thread(consumer);

		producerThread.start();
		consumerThread.start();

	 	producerThread.join();
		consumerThread.join();

		System.out.println(queue);
	}
}
