// Properties
//
// Used to store key,value pair
// Used to create a code that is linked with the .properties file
// which is stored in the server
// so that there is no need to recompile the server code again and again

import java.util.*;
import java.io.*;

class PropertiesDemo {
	public static void main(String args[]) throws IOException,FileNotFoundException {
		Properties p = new Properties();

		FileReader file = new FileReader("propDemo.properties");

		// Loads the .properties file in the Properties collection
		p.load(file);

		System.out.println(p);

		// getProperty and setProperty

		System.out.println(p.getProperty("Niraj"));
		System.out.println(p.setProperty("Niraj","Rajendra Randhir"));	//updates in the Properties not in
										//the .properties file
		System.out.println(p);



	}
}
