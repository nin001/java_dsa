// Other methods ArrayList

import java.util.ArrayList;
import java.util.Scanner;

class ArrayListDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size of ArrayList : ");
		int size = sc.nextInt();

		al.ensureCapacity(size);

		for(int i = 1 ; i<= size ; i++) {
			al.add(i);
		}

		System.out.println(al);

		System.out.println("Size of ArryList : " + al.size());
		System.out.println("Tring to insert over ensureCapacity()");
		al.add("new");
		al.add("new");

		System.out.println(al);

	}
}
