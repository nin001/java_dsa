// Sorted Map
//
// SortedMap is a interface in java 
// Which is direct child of Map interface
//
// Heirarchy
// 	SortedMap (interface)
// 	   |
// 	NavigableMap (interface)
// 	   |
// 	TreeMap (Class)


// Treemap 
// 	- Youngest class which has both sorted and navigable map methods
// 	- Used to store key value pair and sorts the entry using comparator

import java.util.*;

class TreeMapDemo {
	public static void main(String args[]) {
		TreeMap tm = new TreeMap();

		tm.put("India","ind");
		tm.put("Pakistan","pak");
		tm.put("Srilanka","sl");
		tm.put("Australia","aus");
		tm.put("Bangladesh","Ban");

		System.out.println(tm);
	}
}
