// Vector
//
// Dynamic array
// It is present from the starting version of java
// also called as Legacy Collections 
//
// Not used often because ArrayList and LinkedList does the work
//
// Most methods are synchronized and can be used in multithreading

import java.util.*;

class VectorDemo {
	public static void main(String args[]) {
		Vector v = new Vector();

		v.addElement(10);
		v.addElement(20);
		v.addElement(30);

		System.out.println(v);
	}
}
