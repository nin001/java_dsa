// Iterating over map
//
// To iterate over map , there is no iterator , so
// we need to convert our iterator to the Set

import java.util.*;

class IteratingMapDemo {
	public static void main(String args[]) {
		SortedMap sm = new TreeMap();

		sm.put("Ind","India");
		sm.put("Pak","Pakistan");
		sm.put("Ban","Bangladesh");
		sm.put("Sl","Srilanka");

		Set<Map.Entry> setMap = sm.entrySet();

		Iterator itr = setMap.iterator();

		while(itr.hasNext()) {
			Map.Entry entry = (Map.Entry)itr.next();
			System.out.println("Key : " + entry.getKey() + " | Value : " +entry.getValue());
		}
	}
}
