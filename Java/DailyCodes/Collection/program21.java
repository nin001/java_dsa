// To sort List Heirarchy classes 
// There are two ways to sort
// 1 - Passing our List collection to the TreeSet Constructor
// 	that will sort the List collection
// 	
// 	Disadvantage :
// 	- Firstly It creates new collection of objects which is a
// 	heavy operation which will require time
// 	- Creation of List collection into Treeset collection will 
// 	require space (extra)
// 	- Duplicate elements in the List will be eleminated which is
// 	data loss
// 2 - Using sort method of the Collections class which sorts the 
// List type Collections
// 	To sort the user defined collection we need to implement comparator
// 	interface whose which compare method is called , internally in list
// 	while sorting compareTo method is not invoked so implementing comparable is
// 	not an option , so we need to implement the comparator interface and 
// 	override its method which is compare() and There is also present
// 	equals() method which is already inherited from Object class

import java.util.*;

class SortDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();

		al.add("Niraj");
		al.add("Yash");
		al.add("Akash");
		al.add("Bandya");
		al.add("Niraj");
		al.add("Bandya");

		System.out.println("Total elements : " + al.size());

		// This will sort the array List But delete duplicate elements present in the data
		TreeSet ts = new TreeSet(al);
		System.out.println("Size after sorting using treeset : "+ts.size());
		System.out.println(ts);

	}
}

