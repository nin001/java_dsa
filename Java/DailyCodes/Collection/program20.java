// Comparable interface implementation

import java.util.*;

class MyClass implements Comparable {
	String str = null;

	MyClass(String str) {
		this.str = str;
	}

	public int compareTo(Object obj) {
		//return ((MyClass)obj).str.compareTo(this.str);  // descending
		return this.str.compareTo(((MyClass)obj).str);   // ascending
	}

	public String toString() {
		return this.str;
	}
}

class TreeSetDemo {
	public static void main(String args[]) {
		TreeSet ts = new TreeSet();

		ts.add(new MyClass("Niraj"));
		ts.add(new MyClass("Sainath"));
		ts.add(new MyClass("Niraj"));
		ts.add(new MyClass("Yash"));
		ts.add(new MyClass("Niraj"));
		ts.add(new MyClass("Niraj"));

		System.out.println(ts);
	}
}
