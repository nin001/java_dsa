//Enumeration
//
//Cursor particularly for just Legacy Collection Vector and stack
//
//Methods
//hasMoreElements()
//nextElement()

import java.util.*;

class EnumerationDemo {
	public static void main(String[] args) {
		Vector v = new Vector();
		
		v.addElement(1);
		v.addElement(2);
		v.addElement(3);
		v.addElement(4);
		Enumeration en = v.elements();	//returns the enumeration for the vector

		while(en.hasMoreElements()) {
			System.out.println(en.nextElement());
		}
	}
}
