// Hash table
//
// Legacy class which is child of directory class
// It has a bucket of default size 11 and according to
// the bucket size it stores the entry in the Table

// - Synchronized methods , multiple threads cannot work at
// a time in Hashtable
// - Comparitively slow of hashmap
// - legacy class

import java.util.*;

class HashtableDemo {
	public static void main(String args[]) {
		Hashtable ht = new Hashtable();

		ht.put(10,"Sachin");
		ht.put(7,"Dhoni");
		ht.put(18,"Rohit");
		ht.put(1,"Kohli");
		ht.put(45,"Rohit");

		System.out.println(ht);
	}
}
