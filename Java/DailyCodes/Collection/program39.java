// Dictionary
//
// - Legacy class which is used to store k,v pair
// - abstract class , cannot create object but can use as a reference
// - as is a legacy class so enumeration iterator is used
//
/*public java.util.Dictionary();
  public abstract int size();
  public abstract boolean isEmpty();
  public abstract java.util.Enumeration<K> keys();
  public abstract java.util.Enumeration<V> elements();
  public abstract V get(java.lang.Object);
  public abstract V put(K, V);
  public abstract V remove(java.lang.Object);*/

import java.util.*;

class DictDemo {
	public static void main(String args[]) {
		Dictionary dc = new Hashtable();

		dc.put(1,10);
		dc.put(2,20);
		dc.put(3,30);
		dc.put(4,40);
		dc.put(5,50);

		System.out.println(dc);

		//size()
		System.out.println(dc.size());

		//isEmpty()
		System.out.println(dc.isEmpty());

		// Enumeration 
		// keys() and elements()

		Enumeration keys = dc.keys();
		Enumeration vals = dc.elements();

		while(keys.hasMoreElements() && vals.hasMoreElements()) {
			System.out.println(keys.nextElement() + " " + vals.nextElement());
		
		}

		// There are also methods such as get and remove 
		// which can be used to get the value and also remove entry from 
		// dictionary
	}
}

