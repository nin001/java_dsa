// Generating properties file through store

import java.util.Properties;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;

class PropertiesDemo {
	public static void main(String args[]) {
		Properties p = new Properties();

		Scanner sc = new Scanner(System.in);

		System.out.println("Number of entries : ");
		int entries = sc.nextInt();

		while(entries!=0) {
			System.out.println("Enter key : ");
			String key = sc.next();

			System.out.println("Enter value : ");
			String val = sc.next();

			p.setProperty(key,val);
			entries--;
		}

		System.out.println("Enter name of .properties file : ");
		String name = sc.next();

		try {
			p.store(new FileWriter(name),"GENERATED PROP FILE");
		}catch(IOException ie) {
			System.out.println("IOEXCEPTION");
		}
	}
}
