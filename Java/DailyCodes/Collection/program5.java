// LinkedList 
//
// A Collection in Java which is under List (parent List)
//
// LinkedList is a data structure which has nodes and is sequential
// data structure and each node has address to its next nodes
// 
// In java , Due to LinkedList is child of List
// Which allows to access data (element) using index
// which makes LinkedList also to access data using index , But
// LinkedList concept dont allow to access elements using index
// So we should put restrictions so that one cannot access using
// index


import java.util.*;

class LinkedListDemo {
	public static void main(String args[]) {
		LinkedList ll = new LinkedList();

		// Important methods present in LinkedList
		//  - AddFirst()
		//  - AddLast();
		//  - removeFirst()
		//  - removeLast()
		
		ll.add(10);
		ll.addFirst(20);
		ll.addLast(30);

		System.out.println(ll);

		ll.add(40);
		ll.add(50);

		System.out.println(ll);

		ll.removeFirst();
		ll.removeLast();
		
		System.out.println(ll);

	}
}
