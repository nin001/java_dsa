// Using SortedSet
//
// Parent interface of navigable Set
//
// Under Set heirarchy
//
// Methods :
// subset
// headset
// tailset
// first()
// last()

// As It is a interface we can only take the Reference of the Interface
// but we need to create the object of the Treeset

import java.util.*;

class SortedSetDemo {
	public static void main(String args[]) {
		SortedSet ss = new TreeSet();		// can only call the methods that is present in the
							// SortedSet
		
		ss.add(10);
		ss.add(20);
		ss.add(30);
		ss.add(40);
		ss.add(50);

		System.out.println(ss);

		// subset
		// Return type : SortedSet
		// Includes first element till last element -1 (Last element is excluded)
		System.out.println(ss.subSet(new Integer(10),new Integer(40)));

		//headSet
		// return type : SortedSet
		// Returns the elements before the element given (excluds head)
		System.out.println(ss.headSet(new Integer(30)));

		//tailset
		//return type : SortedSet
		//Returns the elments after the element including the given 

		System.out.println(ss.tailSet(new Integer(20)));

		//first()
		//returns first element
		System.out.println(ss.first());

		//last()
		//returns the last element
		System.out.println(ss.last());

	}
}
