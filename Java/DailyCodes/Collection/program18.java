// UserDefined class in LinkedHashSet

import java.util.*;

class CricPlayer {
	int jrNo = 0;
	String name = null;

	CricPlayer(int jrNo , String name) {
		this.jrNo = jrNo;
		this.name = name;
	}

}

class LinkedHashSetDemo {
	public static void main(String args[]) {
		LinkedHashSet hs = new LinkedHashSet();

		hs.add(new CricPlayer(7,"Dhoni"));
		hs.add(new CricPlayer(18,"Virat"));
		hs.add(new CricPlayer(45,"Rohit"));
		hs.add(new CricPlayer(7,"Dhoni"));

		System.out.println(hs);
	}
}
