// User defined in PriorityBlockingQueue

import java.util.concurrent.*;
import java.util.*;

class Employee implements Comparable{
	String name;
	Employee(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}

	public int compareTo(Object obj) {
		return name.compareTo(((Employee)obj).name);
	}

}

class Client {
	public static void main(String args[]) {
		PriorityBlockingQueue pq = new PriorityBlockingQueue();

		pq.put(new Employee("Sainath"));
		pq.put(new Employee("Akash"));
		pq.put(new Employee("Niraj"));
		pq.put(new Employee("Yash"));

		System.out.println(pq);
	}
}
