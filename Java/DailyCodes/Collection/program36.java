// SortedMap
//
/*  Methods :
  public abstract java.util.Comparator<? super K> comparator();
  public abstract java.util.SortedMap<K, V> subMap(K, K);
  public abstract java.util.SortedMap<K, V> headMap(K);
  public abstract java.util.SortedMap<K, V> tailMap(K);
  public abstract K firstKey();
  public abstract K lastKey();
  public abstract java.util.Set<K> keySet();
  public abstract java.util.Collection<V> values();
  public abstract java.util.Set<java.util.Map$Entry<K, V>> entrySet();*/

import java.util.*;

class SortedMapDemo {
	public static void main(String args[]) {
		SortedMap sm = new TreeMap();		// TreeMap youngest child of SortedMap
							// SortedMap is interface so we can
							// Take its reference

		sm.put("nin","Niraj");
		sm.put("bango","Devang");
		sm.put("captain","Aman");
		sm.put("playboy","Atharva");
		sm.put("monty","Akash");

		System.out.println(sm);

		//subMap()
		System.out.println(sm.subMap("bango","monty"));

		//headMap()
		//doesnt include givenkey
		System.out.println(sm.headMap("captain"));

		//tailMap()
		// includes givenkey
		System.out.println(sm.tailMap("playboy"));

		//firstKey()
		System.out.println(sm.firstKey());

		//lastKey()
		System.out.println(sm.lastKey());

		//keySet()
		System.out.println(sm.keySet());

		Set keysSet = sm.keySet();

		//values()
		System.out.println(sm.values());

		// can store the sm.values in any type as its return type is Collection
		Collection val = sm.values();

		//entrySet()
		//
		//returns a Set of the map which is key,value pair type
		
		System.out.println(sm.entrySet());

		Set<Map.Entry> entries = sm.entrySet();
	}
}
