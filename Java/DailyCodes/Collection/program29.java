// HashMap Methods
//
// - V get(Object)
// - V put(K,V);
// - V remove(Object);
// - Set keySet();
// - Collection values();
// - Set<Map$Entry> entrySet();

import java.util.*;

class HashMapDemo {
	public static void main(String args[]) {
		HashMap hm = new HashMap();

		hm.put("Java",".java");
		hm.put("Python",".py");
		hm.put("Dart",".dart");

		System.out.println(hm);

		// get()
		System.out.println(hm.get("Java"));
		System.out.println(hm.get("c"));

		// keyset()
		System.out.println(hm.keySet());

		// values()
		System.out.println(hm.values());

		//entrySet()
		System.out.println(hm.entrySet());
	}
}
