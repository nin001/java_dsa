// Queue (interface)
//
// Queue is a type in collection which can store collection of objects
// and follows FIFO approch
// FIFO - First in First out
//
// Heirarchy :
// 			Queue(I)
// 			   |
//    ------------------------------------------------------
//    |							   |
//    unbounded(java.util)			 bounded(java.util.concurrent)
//            |							|
//  ----------------------	  ---------------------------------------------------
//  |			 |	  |			   |			     |
//  Priority	    Deque(I)	Priority	    	 Array			 Linked
//  Queue	      |	        Blocking 		Blocking		Blocking
//  		-------------    Queue			Queue			Queue
//  		|	     |
//  	   LinkedList	 ArrayDeque
//

import java.util.*;

class QueueDemo {
	public static void main(String args[]) {
		Queue que = new LinkedList();

		//offer() method used to add elements

		que.offer(10);
		que.offer(20);
		que.offer(50);
		que.offer(30);
		que.offer(40);

		System.out.println(que);

		System.out.println(que.poll());			// removes and returns the first element

		System.out.println(que);

		System.out.println(que.poll());

		System.out.println(que);

		System.out.println(que.remove());
		System.out.println(que);

		System.out.println(que.peek());
		System.out.println(que.element());
	}
}
