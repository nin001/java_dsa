// Generating .properties file

import java.util.*;
import java.io.*;

class PropertiesDemo {
	public static void main(String args[]) {
		Properties p = System.getProperties();

		try {
			p.store(new FileWriter("javaProp.properties"),"Generating Properties file using Store");
		} catch (IOException ie) {
			System.out.println("Exception occured");
		}
		System.out.println(p);
	}
}
