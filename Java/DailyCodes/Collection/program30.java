//IdentityHashMap
//
//- used to store key,value pair
//- duplicate keys were not allowed in map , But
//  identityHashMap allows Duplicate keys 

import java.util.*;

class IdentityHashMapDemo {
	public static void main(String args[]) {
		IdentityHashMap im = new IdentityHashMap();

		im.put(new String("Niraj"),"SCOE");
		im.put(new String("Yash"),"RMD");
		im.put(new String("Aadyaraj"),"SKN");
		im.put(new String("Niraj"),"COEP");

		System.out.println(im);

		IdentityHashMap im2 = new IdentityHashMap();

		im2.put(new Integer(10),1);
		im2.put(new Integer(10),2);
		im2.put(new Integer(10),3);
		im2.put(new Integer(10),4);

		System.out.println(im2);
	}
}
