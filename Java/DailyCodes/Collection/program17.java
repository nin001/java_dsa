// LinkedHashSet
//
// Direct parent of HashSet
// - Doesnt Allow Duplicate values (compared through comparable)
// - Preserves Insertion order

import java.util.*;

class LinkedHashSetDemo {
	public static void main(String args[]) {
		LinkedHashSet linkedhs = new LinkedHashSet();

		linkedhs.add("Kanha");
		linkedhs.add("Rahul");
		linkedhs.add("Badhe");
		linkedhs.add("Ashish");
		linkedhs.add("Rahul");
		linkedhs.add("Ashish");

		System.out.println(linkedhs);
	}
}
