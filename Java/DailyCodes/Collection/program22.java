// Using comparator to sort 
// user defined class


import java.util.*;

class Movies {
	String mName = null;

	Movies(String mName) {
		this.mName = mName;
	}

	public String toString() { 
		return mName;
	}
}

// Class that sorts the arraylist that implements comparator

class SortByName implements Comparator {
	public int compare(Object obj1 , Object obj2) {
		return ((Movies)obj1).mName.compareTo(((Movies)obj2).mName);
	}
}

class UserDefinedList {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();

		al.add(new Movies("RHTDM"));
		al.add(new Movies("KKKG"));
		al.add(new Movies("Bajrangi"));
		al.add(new Movies("MulshiPattern"));
		
		System.out.println(al);
		
//		Collections.sort(al);
//		Run time exception at this line , ClassCastException
//		This exception occurs due to our class cannot be converted to Comparable
//		So to achive this, as there is no call for compare method in the add 
//		method of arrayList we need to implement a class called Comparator
//		and then override the method compare which is internally called during sort method
		

		//Using the class that implements comparator 
		//sort method with parameter as List and comparator is present in the Collections

		Collections.sort(al,new SortByName());

		System.out.println("Sorted ArrayList : " + al);
	}
}
