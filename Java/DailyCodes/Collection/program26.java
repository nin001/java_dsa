

// Map 
// Map is a part of Collection framework , used to store
// a key,value pair as an entry.
//
// Heirarchy
// 		Map(I)
// 		|
// ------------------------------------Dictonary
// |	       |          |          |        |
// HashMap(c) Identity   WeakHash  Sorted    HashTable
// |          HashMap      Map      Map        |
// |                                 |         property
// LinkedHashMap                NavigableMap
// 				     |
// 				 TreeMap


// HashMap (c)
// Child class implementing Map interface used to store Key,Value pair

import java.util.*;

class HashMapDemo {
	public static void main(String args[]) {
		// HashSet internally calls map
		// This is the reason that insertion order is not preserved
		// and also HashMap and HashSet order is same

		HashSet hs = new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Badhe");
		hs.add("Rahul");
		
		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Rahul","BMC");

		System.out.println(hm);
	}
}
