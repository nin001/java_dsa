// ArrayList
//
// ArrayList is child class which is Child of List
//
// ArrayList is was created for the following reason
// - Dynamic Array (Array has one limitation that is its size
// 		   is fixed) but ArrayList is Dynamic in nature 
// - Array can only store homogenous data in it , But ArrayList
// can store multiple type of objects

import java.util.ArrayList;

class ArrayListDemo extends ArrayList{		// protected method removeRange
	public static void main(String args[]) {

		/***List of important methods in ArrayList
		 * boolean add(E);	adds element in the ArrayList
		 * int size();		returns the size of the ArrayList
		 * boolean contains(Object);	returns if given object is present
		 * 				in collection or not
		 * int indexOf(Object);	  returns the index of the given element
		 * lastIndexOf(Object);   returns the last occurence of the object
		 * E get(int);	  returns the Element present in the index given
		 * E set(int , E);	replaces the index given with the Element 
		 * 			and returns the object removed
		 * void add(int,E);   adds element in the given index followed by
		 * 			already present elements
		 * E remove(int);     removes element present in the index and returns
		 * 			the removed element
		 * boolean remove(Object);	removes first occured object present in 
		 * 			  the ArrayList and returns true of removed
		 * void clear();	clears the ArrayList (empty)
		 * boolean addAll(Collection);	adds another collection in the rear side
		 * boolean addAll(int , Collection);	adds Collection to the specified
		 * 					index followed by remaining Es
		 * protected void removeRange(int,int);	removes from first paramter to the second
		 * 				-1 index 
		 * Object[] toArray();	   returns array of Object type of the arrayList
		 */

		ArrayListDemo al = new ArrayListDemo();

		al.add(10);
		al.add(20);
		al.add("Core2Web");
		al.add(10);
		al.add("Core2Web");
		//add method to add elements in the ArrayList

		System.out.println(al);

		//size()
		System.out.println("Size of ArrayList : " + al.size());

		//contains(Object);
		System.out.println("Contains Core2Web : " + al.contains("Core2Web"));

		//indexOf(Object);
		System.out.println("Index of 10 : " + al.indexOf(10));
		System.out.println("Index of 22 : " + al.indexOf(22));

		//lastIndexOf(obj);
		System.out.println("Last index of Core2Web : " + al.lastIndexOf("Core2Web"));

		//get(int);
		System.out.println("Get 10 : " + al.get(3));

		//set(int,Object);
		al.set(3,"Niraj");
		System.out.println("Set 3 Niraj");
		System.out.println(al);

		// add(int,E);
		al.add(4,"Randhir");
		System.out.println("Add 4 Randhir");
		System.out.println(al);

		// remove(int);
		System.out.println("Removed : " + al.remove(4));

		// boolean remove(object);
		System.out.println("Remove Niraj : " + al.remove("Niraj"));

		// addAll(Collection);
		ArrayList al2 = new ArrayList();
		al2.add("AL2 ELEMENT");
		al2.add("AL2 ELEMENT");
		al2.add("AL2 ELEMENT");

		al.addAll(al2);
		System.out.println(al);

		//removeRange(int,int);
		System.out.println("Remove range 4,7 ");
		al.removeRange(4,7);			// protected method
		System.out.println(al);

		//toArray();

		Object arr[] = al.toArray();
		for(Object data : arr) {
			System.out.print(data+ " ");
		}
		System.out.println();

		//clear();

		al.clear();
		System.out.println(al);

	}
}
