
// Duplicate values in HashMap gets replaces as key
// If we insert 

import java.util.*;

class HashMapDemo {
	public static void main(String args[]) {

		HashMap hm = new HashMap();

		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Badhe","CarPro");
		hm.put("Kanha","BMC");		// This value for key gets replaced in the 
						// HashMap and there will beonly three entries
						// in the HashMap

		System.out.println(hm);
	}
}
