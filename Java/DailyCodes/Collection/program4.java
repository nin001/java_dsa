// List Iterator trial

import java.util.ArrayList;
import java.util.ListIterator;

class ArrayListDemo {
	public static void main(String args[]) {
		ArrayList al = new ArrayList();
		for(int i = 1 ; i<=5 ; i++) 
			al.add(i);
		//ListIterator
		ListIterator itr = al.listIterator();
		System.out.println(al);
		System.out.println(itr);

		// methods present in ListIterator that are used in this code
		// - boolean hasNext()
		// - E next()
		
		while(itr.hasNext()) {
			System.out.print(itr.next());
		}

		System.out.println();

	}
}
