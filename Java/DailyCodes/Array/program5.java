// Trial Multidimensional array in java

import java.io.*;

class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		System.out.println("Enter col  : ");
		int col = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][col];
		
		System.out.println("Enter values in Array : ");
		for(int i = 0 ; i<row ; i++){
			for(int j = 0 ; j<col ; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Array : ");
		for(int i = 0 ; i<row ; i++){
			for(int j = 0 ; j<col ; j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}
