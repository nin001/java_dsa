//identityHashCode
//user input array

import java.util.*;

class IdentityHashCodeDemo{
	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr1[] = new int[n];
		int arr2[] = new int[n];

		System.out.println("Enter elements ");
		for(int i = 0 ; i<arr1.length ; i++){
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter elements ");
                for(int i = 0 ; i<arr2.length ; i++){
                        arr2[i] = sc.nextInt();
                }

		for(int i = 0 ; i<arr1.length ; i++){
                        System.out.println("Array1 element "+i+" IdentityHashCode : "+System.identityHashCode(arr1[i]));
                        System.out.println("Array2 element "+i+" IdentityHashCode : "+System.identityHashCode(arr2[i]));
                }
	}
}
