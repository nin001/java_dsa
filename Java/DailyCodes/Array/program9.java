// Integer Cache
// There is created a pool which is ranged from -128 to 127 if variable of char or int type
// share same value then single object of that value is made and the variables are pointed to 
// the object
//
// Autoboxing 
// When a character or integer primitive variables are created they are objectised that means
// they are made as an object rather than a variable

class IntegerCacheDemo{
	public static void main(String args[]){
		int x = 10;
		int y = 10;
		Integer z = 10;

		System.out.println(System.identityHashCode(x));
		System.out.println(System.identityHashCode(y));
		System.out.println(System.identityHashCode(z));

		System.out.println("Integer class object is made");
		Integer obj = new Integer(10);
		System.out.println(System.identityHashCode(obj));
	}
}
