// user input jagged array

import java.io.*;

class JaggedArray{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter number of rows IN JAGGED ARRAY : ");
		int row = Integer.parseInt(br.readLine());

		int arr[][] = new int[row][];

		for(int i = 0 ; i<arr.length ; i++){
			System.out.println("Enter row "+(i+1)+"'s column size : ");
			int col = Integer.parseInt(br.readLine());
			arr[i] = new int[col];
		}

		System.out.println("Enter values : ");
		for(int i = 0 ; i<arr.length ; i++){
			System.out.println("Row "+(i+1));
			for(int j = 0 ; j<arr[i].length ; j++){
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}
		System.out.println("Jagged Array");
		for(int i = 0 ; i<arr.length ; i++){
			for(int j = 0 ; j<arr[i].length ; j++){
				System.out.print(arr[i][j]+ " ");
			}
			System.out.println();
		}
	}
}
