// 3 d array

import java.util.*;

class ThreeDArray{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter plane : ");
		int p = input.nextInt();

		System.out.println("Enter row : ");
		int r = input.nextInt();

		System.out.println("Enter col : ");
		int c = input.nextInt();

		int arr[][][] = new int[p][r][c];

		System.out.println("Enter values : ");

		for(int i = 0 ; i<arr.length ; i++){
			System.out.println("Plane "+(i+1));
			for(int j = 0 ; j<arr[i].length ; j++){
				System.out.println("row "+(j+1));
				for(int k = 0 ; k<arr[i][j].length ; k++){
					arr[i][j][k] = input.nextInt();
				}
			}
		}

		for(int[][] x : arr){
			for(int[] y : x){
				for(int z : y){
					System.out.print(z+" ");
				}
				System.out.println();
			}
			System.out.println();
		}
	}
}
