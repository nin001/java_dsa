// IdentityHashCode
// Checking identity hash code for 2 arrays with same elements
// -128 <=element<=127

class IdentityHashCodeDemo{
	public static void main(String args[]){
		int arr1[] = {10,20,30};
		int arr2[] = {10,20,30};

		for(int i = 0 ; i<arr1.length ; i++){
			System.out.println("Array1 element "+i+" IdentityHashCode : "+System.identityHashCode(arr1[i]));
			System.out.println("Array2 element "+i+" IdentityHashCode : "+System.identityHashCode(arr2[i]));
		}
	}
}
