// Networking concepts can be used to connect two processes over a port number
// There are two classes used to create Server client code and establish connection
// and interact using its predefined methods
// ServerSocket and Socket(Client)

import java.io.*;
import java.net.*;

class Server {
	public static void main(String args[])throws IOException {
		ServerSocket ss = new ServerSocket(1535);	//Server socket instance with Port number as constructor
								//parameter

		Socket s = ss.accept();		//Accept() method returns the socket object when client side 
						//establish connection to the ServerSocket
		OutputStream os = s.getOutputStream();

		PrintStream ps = new PrintStream(os);

		ps.println("Hello Client");	// Used to write in the OutputStream established
		ps.println("Bye client");

		s.close();
		ss.close();

	}
}
