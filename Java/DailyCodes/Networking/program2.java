// URL - Uniform resource locator
//
// URL PARTS
// https://www.google.com:80/index.html
// |		|	   |	 |
// protocol   servername  portno file
//
// Two classes present in Java that provied metadata of the URL and Connection

import java.net.*;
import java.util.*;
import java.io.*;

class URLDemo {
	public static void main(String args[])throws IOException {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter website");
		String site = sc.next();

		URL url = new URL(site);		//Throws MalformedURLException

		System.out.println(url.getProtocol());		//Gives name of protocol
		System.out.println(url.getPort());		//Gives portno
		System.out.println(url.getFile());		//Gives file that is opened(inurl)
	}
}
