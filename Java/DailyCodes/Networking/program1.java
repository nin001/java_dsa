// IP address in java
//
// InetAddress
// 
// Command in linux:
// IPAddr
// IPConfig

import java.net.*;
import java.util.*;
import java.io.*;

class IPAddress {
	public static void main(String args[])throws IOException {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter website");
		String site = sc.next();

		InetAddress ip = InetAddress.getByName(site);	// Throws UnknownHostException
		
		System.out.println("IP Address of Entered Website is");
		System.out.println(ip);
	}
}
