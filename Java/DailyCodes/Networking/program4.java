// URLConnection
// class which gives metadata about the url

import java.net.*;
import java.util.*;
import java.io.*;

class URLConnectionDemo {
	public static void main(String args[])throws IOException {
		URL url = new URL("https://www.google.com");

		URLConnection conn = url.openConnection();

		System.out.println("Last Modified : "+ new Date(conn.getLastModified()));
	}
}
