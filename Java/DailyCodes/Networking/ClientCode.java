// Networking concepts can be used to connect two processes over a port number
// There are two classes used to create Server client code and establish connection
// and interact using its predefined methods
// ServerSocket and Socket(Client)

import java.io.*;
import java.net.*;

class Client {
	public static void main(String args[])throws IOException {

		Socket s = new Socket("localhost",1535);	// Client side socket which establishes connection to the Port number

		InputStream is = s.getInputStream();	// InputStream (Connection throgh which communication happens
		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));	// BufferedReader which has
											// InputStream as a parameter
		String str;
		while((str = br.readLine())!=null) {
			System.out.println(str);
		}

	}
}
