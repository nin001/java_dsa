// Open website using code

import java.io.*;
import java.awt.*;
import java.net.*;


class OpenBrowser {
	public static void main(String args[]) throws URISyntaxException,IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Website : ");
		String site = br.readLine();

		URI obj = new URI(site);

		Desktop desk = Desktop.getDesktop();
		desk.browse(obj);
	}
}
