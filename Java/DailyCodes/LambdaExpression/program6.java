// Lambda functions features

interface User {
	void fun(int x);
}

class Demo {
	public static void main(String args[]) {
		User obj1 = (int x)-> {
			System.out.println("Demo1");
		};

		obj1.fun(40);

		
		//Compiler matches arguments with the data type in the inteface
		User obj2 = (x)-> {
			System.out.println("Demo 2");
		};

		obj2.fun(40);

		// Can leave braces if we have only one parameter
		User obj3 = x->{
			System.out.println("Demo 3");
		};
		obj3.fun(1);

		// If we have only one statement then block can be skipped
		User obj4 = x-> System.out.println("Demo 4");
	}
}



