// Lambda function with comparator

// Comparator is a interface which have many methods
// but there are two methods that are abstract
// - compare()
// - equals()
//

import java.util.*;

class Event {
	String eventName;
	float budget;
	Event(String eventName , float budget) {
		this.eventName = eventName;
		this.budget = budget;
	}

	public String toString() {
		return eventName + " : " + budget;
	}
}

class Client {
	public static void main(String args[]) {
		
		ArrayList al = new ArrayList();
		
		al.add(new Event("BDay party",12000));
		al.add(new Event("Marriage",80000));
		al.add(new Event("Freshers",50000));

		System.out.println(al);

		Collections.sort(al,
			(obj1,obj2)-> {
				return ((Event)obj1).eventName.compareTo(((Event)obj2).eventName);
			}
		);

		System.out.println(al);
	}
}
