// There are Functional interfaces in Java 
// that are predefined
//
// Runnable , Comparable

// Runnable
// There are three ways we can use Runnable 
// - UserDefined class that implements Runnable , and then overriding run()
// and Passing UserDefined classes object to Thread constructor
// - Anonymous inner class
// - Lambda function

// Using anonymous inner class

/***Normal Method
 * class ThreadDemo implements Runnable {
 * 	public void run() {
 * 		// overriden code
 * 	}
 * }
 */

class ThreadDemo {
	public static void main(String args[]) {
		
		Runnable obj1 = new Runnable() {
			public void run() {
				System.out.println(Thread.currentThread().getName());
			}
		};

		Thread t1 = new Thread(obj1);
		t1.start();
	}
}
