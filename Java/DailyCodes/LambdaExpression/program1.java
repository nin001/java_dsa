// Lambda expression is a expression which is 
// applied on Functional Interface
//
// There are three types of interfaces
// - Normal interfaces : Interface which can have n no of abstract methods
// - Marker interface : Interface with 0 method , used to notify JVM to provide
// certain functionality
// - Functional interface : Interface which have only one method

// Lambda expression is used to define the one method that is abstract in the
// interface with interaface reference , using lambda function/expression no 
// .class file is created
//
// Features:
// 	- Efficient , as no class file is created
// 	- Code length is decreased

// Example code

interface Core2Web {
	void lang();
}

class Year2022 {
	public static void main(String[] args) {
		Core2Web c2w = ()-> {
			System.out.println("Java/Bootcamp/OS");
		};

		c2w.lang();
	}
}
