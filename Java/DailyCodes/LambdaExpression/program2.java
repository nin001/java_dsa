// Parameter in lambda expression

interface MCD {
	String order(String ord , int price);
}

class OrderDemo {
	public static void main(String args[]) {
		// Anonymous inner class
		MCD ord = new MCD() {
			public String order(String ord , int price) {
				return ord + " : " + price;
			}
		};

		System.out.println(ord.order("McSpicy,Fries,Coke",500));

		MCD ord2 = (o,price)-> {
			return o + " : " + price;
		};

		System.out.println(ord2.order("MaharajaMc,Peri peri fries,Coke",600));

	}
}
