// Using lambda function

class ThreadDemo {
	public static void main(String args[]) {
		
		Runnable obj = ()-> {
			System.out.println(Thread.currentThread().getName());
		};
		
		Thread t1 = new Thread(obj);
		
		Thread t2 = new Thread(
			()->{
				System.out.println(Thread.currentThread().getName());
			}
		);

		t1.start();
		t2.start();
	}
}
