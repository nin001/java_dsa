//instance variable
class InstanceVariable{
	static int x = 10; // instance variable (can also be called global variables these variables are accessible within the context 
			   // of class if they are not declared static then we cannot access them directly if static then accessible to 
			   // all methods that are present in the class

	public static void main(String args[]){
		int y = 20;
		System.out.println(y);
		System.out.println(x);
	}
}

/*
 * when instance variable are declared without static
 * "static int x = 10"
program3.java:8: error: non-static variable x cannot be referenced from a static context
		System.out.println(x);
		                   ^
1 error
*/
