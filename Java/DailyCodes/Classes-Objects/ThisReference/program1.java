// Hidden This reference 
// This is a hidden parameter of all instance methods
// constructor:
// constructor is also a non-static / instance method and hidden this is 
// first parameter of constructor using this instance variable are initialized
// in constructor
//
// other non-static methods:
// As methods stack frame is pushed into java stack , there would be no access 
// to instance variables and methods , using this which is address/reference of 
// object in heap , methods get access to the instance elements
//
// this: (parameter prototype)
// <class> this 	(similar to int x)


class ThisDemo {
	int x = 10;

	ThisDemo() {		// Hidden parameter present "ThisDemo this"
				// using that instance variable is initialized like this
				// this.x = 10;
		System.out.println("In constructor");
	}

	void disp() {		// disp() non static method also have this as hidden parameter
				// like this
				// disp(ThisDemo this)
		System.out.println("Value of x = "+x);
	}
}

class Client {
	public static void main(String args[]) {
		ThisDemo obj = new ThisDemo();		//internal call -->ThisDemo(obj)  passes address of object
		obj.disp();		// internal call disp(obj)   passes address of object
	}
}


