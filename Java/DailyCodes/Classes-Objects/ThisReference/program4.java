//Using this reference 
//We can call multiple constructor by using only one object

class ThisDemo {
	ThisDemo() {
		this(10);
		System.out.println("In no-arg constructor");
	}
	ThisDemo(int x) {
		System.out.println("In para constructor");
	}
	
}

class Client {
	public static void main(String args[]) {
		ThisDemo obj = new ThisDemo();
	}
}

/*
output:
In no-arg contructor

output: with this()
In para constructor
In no-arg constructor

*/

/****Points to remember
 * This() constructor call should be first line in the constructor
 * This() call cannot be written in recursive manner
 * if Constructor has this() then invokespeacial is removed
 */
