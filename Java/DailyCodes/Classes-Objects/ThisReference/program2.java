// Address of object
//
// printing this and object address to confirm that 
// this is the address/reference to the object that 
// is present in the heap section

class ThisDemo {
	int x = 10;

	ThisDemo(int x) {
		this.x = x;

		System.out.println("In constructor");
		System.out.println("Value of x initialized :"+this.x);
		System.out.println("Address of object (this) : "+this);
	}

	void disp() {
		System.out.println("Address of object in disp() : "+this);
	}
}

class Client {
	public static void main(String args[]) {
		ThisDemo obj = new ThisDemo(500);		//internally ThisDemo(obj);

		obj.disp();

		System.out.println("Main :"+obj);
	}
}
