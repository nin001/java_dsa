// Constructor as Setter method using this
// *Company protocols are to give name of the variables in 
// constructor the same as the name of instance variables
// and initialising using this reference

class ThisDemo {
	String name = null;
	int age = 0;

	ThisDemo() {
	}
	ThisDemo(String name , int age) {
		this.name = name;
		this.age = age;

		System.out.println("Values initalized");
	}

	void disp() {
		System.out.println("Name : "+name+"\nAge : "+age);
	}
}

class Client {
	public static void main(String[] args) {
		ThisDemo obj1 = new ThisDemo();
		ThisDemo obj2 = new ThisDemo("Niraj",20);
		System.out.println("Without Setter ");
		obj1.disp();

		System.out.println("With constructor as setter");
		obj2.disp();
	}
}
