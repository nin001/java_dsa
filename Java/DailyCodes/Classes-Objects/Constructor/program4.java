// Constructor Types
// IF YOU DONT WRITE ONE IN CODE
// then compiler adds a constructor in out code which calls parent classes constructor
// this constructor is called Default Constructor
// 
//
// IF WE WRITE OUR OWN CONSTRUCTOR
// if it has 0 parameters 
// then it is called - No-Arguments constructor
//
// if it has 1 or more parameters
// then it is called - Parameterized constructor

class ConstructorDemo {
	ConstructorDemo() {
		System.out.println("In constructor");
	}

	void fun() {
		ConstructorDemo obj = new ConstructorDemo();

		System.out.println(obj);
	}

	public static void main(String args[]) {
		ConstructorDemo obj1 = new ConstructorDemo();
		System.out.println(obj1);
		ConstructorDemo obj2 = new ConstructorDemo();
		System.out.println(obj2);
		obj1.fun();
	}
}

/*
In constructor
ConstructorDemo@d716361
In constructor
ConstructorDemo@6ff3c5b5
In constructor
ConstructorDemo@3764951d
*/
