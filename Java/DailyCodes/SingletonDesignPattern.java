// Singleton design pattern
//
// Pattern which is used when , want to restrict from creating object
// everytime user wants to create object returning same object

class Singleton {
	private Singleton(){

	}
	static Singleton obj = new Singleton();

	static Singleton getObj() {
		return obj;
	}
}

class Client {
	public static void main(String args[]) {
		Singleton obj = Singleton.getObj();

		System.out.println(obj);
		Singleton obj1 = Singleton.getObj();

		System.out.println(obj1);
	}
}
