// File reader
// FileReaderDemo.txt file for reading
// Constructors
// (String)
// (File)

import java.io.*;

class FileReaderDemo {
	public static void main(String args[])throws IOException {
		File file = new File("FileReaderDemo.txt");

		FileReader fr = new FileReader(file);

		int data = fr.read();

		while(data != -1) {

			System.out.print((char)data);
			data = fr.read();
		}

		fr.close();
	}
}
