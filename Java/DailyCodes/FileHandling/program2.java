// Creating directory using File classes method mkdir()

import java.io.*;

class FileDemo {
	public static void main(String args[]) {
		File directory = new File("FileHandlingDemoDirectory");		// objects that represents a file
		directory.mkdir();
		// As java supports unix system. There is a method in File class that creates
		// a Directory in the present working directory using method , mkdir()
		// This methods will Represent to the FileHandlingDemoDirectory folder if already 
		// present or create a folder.
	}
}
