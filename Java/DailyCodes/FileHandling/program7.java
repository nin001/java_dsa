// File handling in java
//
// Classes That are involved in File Handling in java
//
// To perform Write opeation in java
// - Writer(class) 	Parent (root)
// - OutputStreamWriter(class)	
// - FileWriter		(class with only constructors, used to wwrite in files)
//
// To perform Read operation in java
// - Reader(class)	Parent(root)
// - InputStreamReader
// - FileReader
// 
// Other classes (IMPORTANT)
// - FileOutputStream
// - FileInputStream
// - FileDescriptor
// - ObjectOutputStream
// - ObjectInputStream

// Code to write in File 

import java.io.*;

class FileWriterDemo {
	public static void main(String args[])throws IOException {
		FileWriter fw = new FileWriter("FileWriterDemo.txt");	// If file not present it will create
									// a new file and also Constructor
									// throws IOException
		fw.write("Niraj");
		fw.write("Randhir");
		// write() function throws IOException

		fw.close();	// throws IOException
	}
}
