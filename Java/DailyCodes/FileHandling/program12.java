// Deserialization

import java.io.*;

class Deserialization {
	public static void main(String args[])throws IOException , ClassNotFoundException {
		// Deserialization can be achived in hava using
		// FileInputStream and ObjectInputStream

		FileInputStream file = new FileInputStream("SerializationDemo.txt");

		ObjectInputStream ois = new ObjectInputStream(file);

		Player p1 = (Player)ois.readObject();
		Player p2 = (Player)ois.readObject();

		System.out.println("Name : " + p1.name);
		System.out.println("JrNo : " + p1.jrNo);

		System.out.println("Name : " + p2.name);
		System.out.println("JrNo : " + p2.jrNo);

		ois.close();
		file.close();
	}
}
