// Methods in File Class
// 
// getname()
// getParent()
// getPath()
// getAbsolutePath()
// canRead()
// canWrite()
// isDirectory()
// isFile()
// lastModified()
// delete()

import java.io.*;

class FileDemo {
	public static void main(String args[]) throws IOException{
		File obj = new File("Core2Web.txt");

		//createNewFile() and mkdir() methods are used to actually create file/folder from object
		obj.createNewFile();

		//getName()
		System.out.println(obj.getName());

		//getParent()
		System.out.println(obj.getParent());		//If we have not provided any parent , that
								//object becomes root , so the output will be
								//null
		//getPath()
		System.out.println(obj.getPath());
		//getAbsolutePath()
		System.out.println(obj.getAbsolutePath());

		//canRead()
		System.out.println(obj.canRead());
		//canWrite()
		System.out.println(obj.canWrite());

		//isDirectory()
		System.out.println(obj.isDirectory());
		//isFile()
		System.out.println(obj.isFile());

		//lastModified()
		System.out.println(obj.lastModified());



	}
}

