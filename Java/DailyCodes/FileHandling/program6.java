// list() method in File
//
// makes list of files and returns String[] with names

import java.io.*;

class FileDemo {
	public static void main(String args[]) {
		File obj = new File("/mnt/d/CODES/java_dsa/Java/DailyCodes/FileHandling");
			//Path given , now obj represents FileHandling dir

		int FileCount = 0;
		int FolderCount = 0;
		String[] files = obj.list();

		for(String file : files) {
			File temp = new File(file);
			if(temp.isDirectory()) {
				FolderCount++;
			}else {
				FileCount++;
			}
			System.out.println(file);
		}

		System.out.println("Number of Files : "+FileCount);
		System.out.println("Number of Folders : "+FolderCount);
	}
}
