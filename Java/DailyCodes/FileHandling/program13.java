// Checking for ./ path

import java.io.*;

class Demo {
	public static void main(String args[]) throws IOException {
		String fname = "test.txt";
		String path = new File(".").getCanonicalPath()+"/FileHandlingDemoDirectory/"+fname;
		System.out.println(path);
		File f = new File(path);

		if(f.exists()) {
			System.out.println("File found");
			
			FileReader fr = new FileReader(f);

			int data = fr.read();
			while(data != -1) {
				System.out.print((char)data);
				data = fr.read();
			}
		}else {
			System.out.println("malfunction");
		}
			
	}
}
