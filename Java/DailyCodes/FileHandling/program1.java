// File handling in java
// can be achieved using Several classes which allows to load
// read ,write ,in files of different extension 
//
// File class is used in File Handling in java

import java.io.*;

class FileHandlingDemo {
	public static void main(String args[]) {
		File fobj = new File("Demo.txt");
		
		// This code doesnt create file in the current working directory
		// It creates a object that represents a file that represents the Demo.txt
		// file , That can be handled and manipulated using File classes methods

	}
}
