// FileDescriptor
//
// FileDesciptor is a number given to each file created Like(stdIn,out,err)
// but in java as everything is a object, FileDescriptor is a class which acts
// as a via through which interactions between File and java program occurs
//
// FileInputSream and FileOutputStream contain getFD through which we can getFileDesc 
import java.io.*;

class FileDescriptorDemo {
	public static void main(String args[])throws IOException {
		FileInputStream fis = new FileInputStream("FileReaderDemo.txt");
		FileDescriptor fd = fis.getFD();

		FileReader fr = new FileReader(fd);

		int data = fr.read();
		while(data != -1) {
			System.out.print((char)data);
			data = fr.read();
		}
		fis.close();
		fr.close();
	}
}
		
