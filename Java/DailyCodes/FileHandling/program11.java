// IMPORTANT
//
// Serialization and deserialization
//
// Storing a object in a file is Serialization
// Reading a object from a file is Deserialization

import java.io.*;

class Player implements Serializable {
	int jrNo;
	String name;
	Player(int jrNo , String name) {
		this.jrNo = jrNo;
		this.name = name;
	}
}

class SerializationDemo {
	public static void main(String args[])throws IOException {

		Player p1 = new Player(18,"Virat");
		Player p2 = new Player(7,"Dhoni");

		// Classes help in serialization
		// FileOutputStream , ObjectOutputStream
	
		FileOutputStream file = new FileOutputStream("SerializationDemo.txt");

		ObjectOutputStream oos = new ObjectOutputStream(file);

		// ObjectOutputStream class constains writeObject method which writes
		// object in a FileOutputStream type representing a file

		oos.writeObject(p1);
		oos.writeObject(p2);

		oos.close();
		file.close();
	}
}
