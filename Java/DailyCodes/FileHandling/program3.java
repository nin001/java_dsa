// Creating new file in the current working directory using 
// createNewFile() methods in File class

import java.io.*;

class FileDemo {
	public static void main(String args[]) {
		File fobj = new File("Demo");
		File whatIsDefExtens = new File("Demo.file");

		System.out.println("Creating new file");
		try {
			whatIsDefExtens.createNewFile();
			fobj.createNewFile();	// Throws IOException 
		}catch(IOException ie) {
			System.out.println("Exception occured");
		}
	}
}

