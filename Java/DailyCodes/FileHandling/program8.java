// There is a flag present in the FileWriter, which is by default false
// If this flag is true then when a code is executed again with write() function
// then , it appends meaning it writes after the data present

import java.io.*;

class FileWriterDemo {
	public static void main(String args[]) throws IOException {
		File file = new File("FileWriterDemo.txt");
		FileWriter fw = new FileWriter(file,true);	// flag is true , write () will append
		
		fw.write("\nIs the best\n");

		fw.close();
	}
}

