// Overloading
//
// same class
// length of paramter / datatype of paramters should be different

class Demo {
	
	void fun(int x) {
		System.out.println(x);
	}

	void fun(int x) {
		System.out.println(x);
	}
}
/*
program1.java:12: error: method fun(int) is already defined in class Demo
        void fun(int x) {
             ^
1 error*/
