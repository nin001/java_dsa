// Real time example of overriding

class Match {
	void matchInfo() {
		System.out.println("Odi , T20 , or test");
	}
}

class IPLMatch extends Match {
	void matchInfo() {
		System.out.println("T20 match");
	}
}

class TestMatch extends Match {
	void matchInfo() {
		System.out.println("Test match");
	}
}

class Client {
	public static void main(String args[]) {
		Match ipl = new IPLMatch();

		ipl.matchInfo();

		Match test = new TestMatch();
		
		test.matchInfo();
	}
}

