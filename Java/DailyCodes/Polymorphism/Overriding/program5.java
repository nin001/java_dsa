// Overriding

class Parent {
	String fun() {
		System.out.println("in parent fun");
		return "";
	}
}

class Child extends Parent {
	Object fun() {
		System.out.println("in child fun");
		return new Object();
	}
}
/*
cannot override fun() 

Object is not compatible with String
*/
