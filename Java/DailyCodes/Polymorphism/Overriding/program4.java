// Overriding

class Parent {
	void fun() {
		System.out.println("in parent fun");
	}
}

class Child extends Parent {
	int fun() {
		System.out.println("in child fun");
	}
}
/*
cannot override fun() 

int is not compatible with void
*/
