// While overriding return type is compulsory checked 
// in case of primitive data type
// It should be exact match , or else compiler will give error
// but in case of classes as return type , 
// return type could be covarient , but in parent class should contain
// parent and child class should contain child

class Parent {
	char fun() {
		System.out.println("In parent fun");
		return 'A';
	}
	

	Object gun() {
		System.out.println("In parent gun");
		return new Object();
	}
}

class Child extends Parent {
	char/*int*/ fun() {		//cannot override fun  int cannot be converted to char
		System.out.println("In child fun");
		return 0;
	}

	String gun() {
		System.out.println("In child gun");
		return new String();
	}
}
