//Overriding 

class Parent {
	Parent() {
		System.out.println("Parent constructor");
	}

	void fun() {
		System.out.println("in fun");
	}
}

class Child extends Parent {
	Child() {
		System.out.println("Child constructor");
	}

	void gun() {
		System.out.println("in gun");
	}
}

class Client {
	public static void main(String args[]) {
		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();
	}
}

/*
 * METHOD TABLE (compiler checks method tables in compile time and binds invokation to matched method signature
 *
 * Parent table
 * Parent()
 * fun()		// Here gun() is not present so compiler gives error for obj2.gun() as obj2 is of Parent type
 *
 * Child table
 * Child()
 * gun()
 * fun()
 */
