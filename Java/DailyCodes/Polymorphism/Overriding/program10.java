// overriding
// final modifier in overriding
// Final methods that are declared in the parent class cannot be overriden by child
// but are inherited

class Parent {
	final void fun() {
		System.out.println("Fun-parent");
	}
}

class Child extends Parent {
/*	void fun() {					//Cannot override 
		System.out.println("fun-child");	//Error : overriden method is final
	}*/
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();

		obj.fun();
	}
}
