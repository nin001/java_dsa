// Parents reference and childs object

// If object is created this way , methods can be called that are 
// present in both the classes (can be inherited) , Childs method 
// cannot be called due to compiler checks the method to the reference
// class 

class Parent {
	void fun() {
		System.out.println("in fun");
	}
}

class Child extends Parent {
	void gun() {
		System.out.println("in gun");
	}

	void fun() {
		System.out.println("overriden fun");
	}
}

class Client {
	public static void main(String args[]) {
		Parent obj = new Child();

//		Child obj2 = new Parent();     Parent cannot be converted to child

		obj.fun();
//		obj.gun();      cannot find symbol
		
	}
}
