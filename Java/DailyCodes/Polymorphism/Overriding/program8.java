// Overriding 
// Access specifier while overriding 
// We cannot override methods inherited from parent class and 
// give them weaker access specifier , but can give higher access

class Parent {
	public void fun() {		//access specifier public
		System.out.println("in parent fun");
	}
}

class Child extends Parent {
	void fun() {			//access specifier given default while overriding
		System.out.println("in child fun");
	}
}

// error , Attempting to assign weaker access priviledges was public 
