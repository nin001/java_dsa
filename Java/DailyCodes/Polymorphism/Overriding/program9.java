// access specifier private
// doesnt inherit 

class Parent {
	private void fun() {
		System.out.println("Parent");
	}
}

class Child extends Parent {
	void fun() {
		System.out.println("Child");
	}
}

class Client {
	public static void main(String args[]) {
//		Parent obj = new Child();

//		obj.fun();
		
		Child obj = new Child();
		obj.fun();
	}
}
