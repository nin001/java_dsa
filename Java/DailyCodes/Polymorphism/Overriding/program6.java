// Overriding

class Parent {
	Object fun() {
		System.out.println("in parent fun");
		return "";
	}
}

class Child extends Parent {
	String fun() {
		System.out.println("in child fun");
		return new String();
	}
}

/**Covarient class return type is supported in overrding  , 
 * in case of primitive data type , they need to be exact same*/
