// Polymorphism
// Overriding 
// Parent have some method which is going to be inherited by child
// But child doesnt agree with the implementation of the method but wants that
// method , Then child can implement its own method with the same name and the 
// parents method gets overriden 

class Parent {
	Parent() {
		System.out.println("In parent constructor");
	}

	void property() {
		System.out.println("Flat , Gold , Car");
	}

	void marry() {
		System.out.println("Deepika Padukone");
	}
}

class Child extends Parent {
	Child() {
		System.out.println("In child constructor");
	}

	void marry() {
		System.out.println("AM <3");
	}
	/*
	 * marry() method gets inherited from parent , But here child has its own 
	 * marry() method to which child has its owm implementation 
	 * SO marry() method gets overriden by the child
	 */
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
		obj.property();
		obj.marry();
	}
}
