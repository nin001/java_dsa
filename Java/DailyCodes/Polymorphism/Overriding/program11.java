// Overring
// Static modifier in overriding


class Parent {

	static void fun() {
		System.out.println("In parent static fun");
	}

}

class Child extends Parent {
	
/*	void fun() {					// cannot override static , static and final doesnt take part in 
		System.out.println("In child fun");	// overriding , 
							// Error : overriden method static
	}*/						// 
							// if parent had non static and child had static
							// Error : overriding method static
	
	static void fun() {
		System.out.println("In Child static fun");
	}
	/**This are two different static methods , 
	 * parent has its own static method fun and child has its own static method fun
	 * This concept is known as METHOD HIDING 
	 */
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();

		obj.fun();

		Parent obj1 = new Child();

		obj1.fun();
	}
	/***Static methods are bind in compile time without object(new) 
	 * and no runtime binding is done*/
}
