// Scenario 2
// having object class and string as parameter

class Scenario2 {

	void fun(Object obj) {
		System.out.println("Object");
	}

	void fun(String str) {
		System.out.println("String");
	}
}

class Client {
	public static void main(String args[]) {
		Scenario2 obj = new Scenario2();

		obj.fun("Niraj");

		obj.fun(new StringBuffer("Niraj"));
//If There doesnt exist parent child relation then this line gives ambigious error , but in this case 
//Priority goes to the Child class
		obj.fun(null); //can match with both but matches with string
	}
}
