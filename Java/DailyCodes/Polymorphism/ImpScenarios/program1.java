// Frequently asked in the interview , 
// Overloading scenarios , where there exist two methods
// with parameters String and String buffer

class Scenario1 {
	void fun(String str) {
		System.out.println("String");
	}

	void fun(StringBuffer str) {
		System.out.println("String buffer");
	}
}

class Client {
	public static void main(String args[]) {
		Scenario1 obj = new Scenario1();	// No errors when there is no method call 

		obj.fun("Niraj"); //no error -- binds with string parameter and executes 

		obj.fun(new StringBuffer("Niraj"));	// No error -- Binds and executes String Buffer parameter	

		obj.fun(null);	//Error here
				//Ambigious, 
				//As null can go into any class 'string str = null' 
				//and stringbuffer as well
				//so there exist ambigious choice so compiler gives error

	}
}
