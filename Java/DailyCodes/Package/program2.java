// Can we import user defined package using
// .* 
//
// Conditions :
// - Source file should not be present in our client folder
// - Can create all the files in a different folder say 'Source files"

import java.util.*;
import arithfun.*;

class Client {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int x = sc.nextInt();
		System.out.println("Enter number : ");
		int y = sc.nextInt();

		Addition addObj = new Addition(x,y);
		Subtraction subObj = new Subtraction(x,y);

		System.out.println("Addition : " + addObj.add());
		System.out.println("Subtraction : " + subObj.sub());
	}

}
