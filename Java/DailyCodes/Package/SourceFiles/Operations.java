// Package that would contain Multiple classes 

package arithfun;
import java.util.Scanner;

class Add {
	int x;
	int y;

	Add(int x, int y) {
		this.x = x;
		this.y = y;
	}

	int add() {
		return x+y;
	}
}

class Sub {
	int x;
	int y;

	Sub(int x, int y) {
		this.x = x;
		this.y = y;
	}

	int sub() {
		return x-y;
	}
}


class Mult {
	int x;
	int y;

	Mult(int x, int y) {
		this.x = x;
		this.y = y;
	}

	int mult() {
		return x*y;
	}
}

class Div {

	int x;
	int y;

	Div(int x, int y) {
		this.x = x;
		this.y = y;
	}

	int div() {
		return x/y;
	}
}


public class Operations {
	public void operations() {
		System.out.println("Operations : ");
		System.out.println("1. Add ");
		System.out.println("2. Sub ");
		System.out.println("3. Mult ");
		System.out.println("4. Div ");

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter choice : ");
		int ch = sc.nextInt();

		if(ch == 1) {
			System.out.println("Enter number : ");
			int x = sc.nextInt();
			System.out.println("Enter number : ");
			int y = sc.nextInt();

			Add obj = new Add(x,y);

			System.out.println(obj.add());
		}else if(ch == 2) {
			System.out.println("Enter number : ");
			int x = sc.nextInt();
			System.out.println("Enter number : ");
			int y = sc.nextInt();

			Sub obj = new Sub(x,y);

			System.out.println(obj.sub());
		}else if(ch == 3) {
			System.out.println("Enter number : ");
			int x = sc.nextInt();
			System.out.println("Enter number : ");
			int y = sc.nextInt();

			Mult obj = new Mult(x,y);

			System.out.println(obj.mult());
		}else if(ch == 4) {
			System.out.println("Enter number : ");
			int x = sc.nextInt();
			System.out.println("Enter number : ");
			int y = sc.nextInt();

			Div obj = new Div(x,y);

			System.out.println(obj.div());
		}else {
			System.out.println("Invalid choice");
		}
	}
}
