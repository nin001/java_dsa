// Addition source file to create a package
// package keyword is used to create a file as package
//
// declaring file name same as the class name

package arithfun;

public class Addition {
	int x;
	int y;
	public Addition(int x , int y) {
	       this.x = x;
	       this.y = y;
	}

	public int add() {
		return x+y;
	}
}

