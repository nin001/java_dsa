// sub package creation in java

package arithfun.powerfun;

public class SquareRoot {
	
	int num;

	public SquareRoot(int num) {
		this.num = num;

	}

	public int sqrRoot() {
		int sq = 1;
		for( ; sq*sq <= num ; sq++) {
			if(sq*sq == num) {
				return sq;
			} 
		}
		return sq-1;
	}
}
