// importing package which consist multiple classes

import arithfun.*;
import java.util.Scanner;

class PackageDemo {
	public static void main(String args[]) {
		Operations obj = new Operations();

		int flag = 1;
		
		Scanner sc = new Scanner(System.in);
		while(flag != 0) {
			
			System.out.println("Do you want to access operations ");
			String ch = sc.nextLine();

			if( ch.equals("Y") || ch.equals("y")) 
				obj.operations();
			else
				flag = 0;
		}
		System.out.println("Program end");
	}
}
