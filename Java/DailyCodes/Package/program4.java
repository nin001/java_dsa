// sub package class

import java.util.Scanner;
import arithfun.powerfun.*;

class PackageDemo {
	
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number to find Square root : ");
		int num = sc.nextInt();

		SquareRoot sqrt = new SquareRoot(num);

		System.out.println("Square Root : "+ sqrt.sqrRoot());
	}
}
		
