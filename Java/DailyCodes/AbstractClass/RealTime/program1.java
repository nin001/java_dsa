// Real time example of Abstract class in java

abstract class Parent {
	void career() {
		System.out.println("Engineering");
	}

	abstract void marry();
}

class Child extends Parent {
	void marry() {
		System.out.println("AM <3");
	}
}

class Client {
	public static void main(String args[]) {
		Parent obj = new Child();

		obj.career();
		obj.marry();
	}
}
