// if class child class doesnt give body to abstract class.
// then child class needs to be declared abstract

class Parent {
	
	void fun() {
		System.out.println("In fun");
	}

	abstract void gun();
}

/*abstract*/ class Child extends Parent {

}
/*
program4.java:4: error: Parent is not abstract and does not override abstract method gun() in Parent
class Parent {
*/

