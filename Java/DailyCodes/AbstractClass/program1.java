// Abstract class
// 
// Till now we have written classes with methods who all have body
// methods with body are called concrete methods , and class with all 
// concrete methods is called complete/concrete class
//
// Abstract methods - Methods without body that are present with a keyword 
// abstract , which means abstracted method will be given body in child class
//
// Abstract class is a class which have abstract methods present i.e. methods 
// without body
// abstracted method. 
//
// Abstract methods can have methods abstracted 0-100% 
//
// Abstract classes object cannot be instantiated as it is a incomplete class
//
// Abstract class have constructor present , as classes are going to extend it
// so its instance variables would get inherited and that variables needs to be 
// initialized for use , And as every constructor has invokespeacial as first line
// so for constructor chaining constructor is present

abstract class Demo {

	void fun() {
	
	}

	abstract void gun();
}
