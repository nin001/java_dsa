// we can use abstract class with 0 % abstraction so that object of that class 
// could not be created

abstract class Parent {
	private void important_method() {
		System.out.println("Confidential");
	}

	private void working() {
		System.out.println("Functiality");
	}

	void fun() {
		System.out.println("Fun");
	}

	void gun() {
		System.out.println("Gun");
	}
}

class Child extends Parent {
	
}

class Client {
	public static void main(String args[]) {
		//Parent obj = new Parent();

		//We here can use Parents reference but 
		//cannot access private variables 

		Child obj = new Child();

		obj.fun();
		obj.gun();
		/*
		obj.working();                           --cannot find symbol
		obj.important_method();*/
	}
}

/*
program3.java:28: error: Parent is abstract; cannot be instantiated
                Parent obj = new Parent();*/
