// if child of abstract class doesnt give body to abstract method
// then child also have to be declared as abstract

abstract class Parent {
	void career() {
		System.out.println("Engineering");
	}

	abstract void marry();
}

class Child extends Parent {
	void marry() {
		System.out.println("AM <3");
	}
}

class Client {
	public static void main(String args[]) {
		// Absract classes object cannot be created but can be used as a reference

		Parent obj = new Child();

		obj.marry();
	}
}
