// variables in interface

interface D1 {
	int x = 10;
}
interface D2 {
	int x = 100;
}

class Child implements D1,D2 {
	void fun() {
		System.out.println("in fun");
		System.out.println(x);		//reference to x is ambigious
	}
}
