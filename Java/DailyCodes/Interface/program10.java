// Calling interface static methods using interface name


interface Demo {
	static void fun() {
		System.out.println("in fun");
	}
}

class DemoChild implements Demo {

	void accessFun() {
		Demo.fun();
	}
}

class Client {
	public static void main(String args[]) {
		Demo.fun();
		DemoChild obj = new DemoChild();

		obj.accessFun();

	}
}
