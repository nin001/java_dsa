// interface
//
// It is a type of class where there is 100 % abstraction present
// i.e. Every method in the interface is abstracted 
// - If we want a class with 100% abstraction then we use interface
// - Interface is used when we want to prepare a module 
// - When there is need of 100 % abstraction then interface is used
// - Multiple inheritace is not supported in java , but using interface
// we can implement multiple interface in a class

interface InterfaceDemo {
	void fun();
	void gun();  // There is no need to write abstract infront of methods because 
}		     // interface methods are by default public abstract **compiler adds public abstract
		     // in front of the methods after compilation

class Demo implements InterfaceDemo {	// implements keyword is used here \
	public void gun() {					// While implementing methods from interface , We need to give public access specifier
		System.out.println("In Gun Demo");	// because compiler adds public and abstract before our method
	}						// So we cannot assign weaker access specifier while implementing the method

	public void fun() {
		System.out.println("In Fun demo");
	}
}

