// Interface in java
//
// Default advance code (default in interface) 
//
// If 2 interfaces have same methods with same methods signature
// with default and class is implementing both the interfaces , 
// then there is compulsion on the class to compulsory override the method in 
// the class while implementing or else there is ambigious choice between the 
// methods , Hence compiler gives error

interface Demo1 {
	default void fun() {
		System.out.println("in fun - interface demo1");
	}
}

interface Demo2 {

	default void fun() {
		System.out.println("In fun - interface demo2");
	}
}

// If implements and inherits two defaults in class then , 
// compulsory overriding must done or else error will come
// error : types Demo1 and Demo2 are incompatible;

class DemoChild implements Demo1,Demo2 {

	public void fun() {
		System.out.println("In fun - demochild");
	}
}

class Client {
	public static void main(String args[]) {
		Demo1 obj1 = new DemoChild();
		obj1.fun();		//compile time binding with interface demo1 method and runtime bind with child(override)

		Demo2 obj2 = new DemoChild();
		obj2.fun(); 		//compile time binding with interface (demo2) and runtime with demochild
		DemoChild obj3 = new DemoChild();
		obj3.fun();  		// has overriden method in it and has no reference of interface , so 
					//both binding with demochild
	}
}


