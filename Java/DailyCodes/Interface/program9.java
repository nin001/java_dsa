// In classes static methods can be called using object

class Demo {
	static void fun() {
		System.out.println("in fun");
	}
}

class DemoChild extends Demo {
}

class Client {

	public static void main(String args[]) {
		DemoChild obj = new DemoChild();

		obj.fun();
	}
}
