// Interface in java
//
// Multiple inheritance in java
// 
// Java doesnt support multiple inheritance as a concept in classes
// and object due to following reason
// - If multiple inheritance would be allowed and if 2 parent classes 
// consisted methods with same method signature then , it would be ambigious
// that to which method child class would inherit as in a class there should 
// only exist methods with unique method signature

interface Demo1 {
	void fun();
}

interface Demo2 {
	void fun();
}

class DemoChild implements Demo1 , Demo2 {
	public void fun() {
		System.out.println("in fun");
	}
}

class Client {
	public static void main(String args[]) {
		Demo1 obj1 = new DemoChild();
		obj1.fun();

		Demo2 obj2 = new DemoChild();
		obj2.fun();
	}
}

/*
 * Reason that interface supports multiple inheritance
 As in interface 100% abstraction is present and methods in interface doesnt 
 have bodies (abstract method) , If multiple Interfaces are implemented in a 
 Class then it doesnt matter and there is no ambiguity as there doesnt exist
 concrete methods and only one method is implemented 
 */
