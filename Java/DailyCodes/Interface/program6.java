// Interface demo
//
//  Default 1
//
//  Using default keyword we can give abstract method body inside the interface(can
//  write concrete method inside the interface 
//  
//  This is allowed in the interface because there could exist methods which have common 
//  implementation in the implemented classes , which will increase the code redundancy
//  To minimize the redundance common methods could be given body in interface using default and 
//  static  , default individually is used , so that if child class dont want to implement its own
//  it gets inherited in child class , but child class has the chance to override the method in 
//  its class 


interface Demo {
	default void gun() {
		System.out.println("In gun - Interface ");
	}

	default void fun() {
		System.out.println("In fun - Interface");
	}
}

class DemoChild implements Demo {
	public void gun() {		//Overriding of gun method in the demochild class
		System.out.println("In gun - DemoChild");
	}
}

class Client {
	public static void main(String args[]) {
		Demo obj = new DemoChild();

		obj.gun();
		obj.fun();
	}
}
