// Interface in java
//
// Interface also could inherit another interface in single interface
//
// Interface inheriting another interface would use extends keyword
//
// As Interface is 100% abstract class so multiple inheritance with keyword 
// extends is supported in java

interface Demo1 {
	void gun();
}

interface Demo2 {
	void fun();
}

interface DemoChild extends Demo1,Demo2 {

}

class Client implements DemoChild {
	
	public void fun() {
		System.out.println("In fun");
	}
	public void gun() {
		System.out.println("In gun");

	}
	public static void main (String args[]) {
		Client obj = new Client();

		obj.fun();
		obj.gun();
	}
}
