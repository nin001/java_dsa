// Interface in java
//
// Interface was brough incase we need to use 100% abstraction 
// But there could be some scenarios 
// that there is a comman methods that is implemented the same 
// in each class that implements interface , which will increase 
// the code redundancy , So we can give interface methods body using
// 2 keywords Default and Static , here default is not access specifier
// it is a keyword for compiler to allow to give body inside the interface
// 
// Default keword is used when comman method is written in interface
// but implementing class can override the method in its own implementation
//
// Static keyword is used when common method written in the interface is same
// for all class that "inherits" the method in the class ( overriding is not allowed)


interface Demo {
	static void fun() {
		System.out.println("In interface fun");
	}

	default void gun() {
		System.out.println("In interface gun");
	}
}

class DemoChild implements Demo {
	
	public static void main(String args[]) {
		DemoChild obj = new DemoChild();

	//	obj.fun();
	//	Static keyword in interface means that 
	//	it cannot be accessed using the object
	//
	//	It doesnt take part in the inheritance
	//
	//	that is the reason that cannot find symbol
	//	error comes
		obj.gun();

		Demo.fun();
	}
}
