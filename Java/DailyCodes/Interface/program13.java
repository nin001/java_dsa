// variables in interface

interface D1 {
	int x = 10;
}
interface D2 {
	int x = 100;
}

class Child implements D1,D2 {
	int x = 1000;
	void fun() {
		System.out.println("in fun");
		System.out.println(x);		
	}
}

class Client {
	public static void main(String args[]) {
		Child obj = new Child();
		obj.fun();
	}
}
