// Interface in java
//
// Static in interface
//
// Static keyword is also used in giving methods body(concrete methods) in 
// interface which is allowed to decrease code redundancy 
//
// Using static method in interface we can give methods body and that method cannot 
// be overriden in the implementing classes
//
// Static method in interface is interfaces own method it doesnt take part in 
// inheritance , It can be only called by Interface name only 
//
// This is done so that the static method (interface) cannot be changed (overriden) by 
// any class which is implementing interface

interface Demo {

	static void fun() {
		System.out.println("fun - demo - interface");
	}
}

class DemoChild implements Demo {

}

class Client {
	public static void main(String args[]) {
		DemoChild obj = new DemoChild();
		//obj.fun();  //cannot find symbol

		Demo obj1 = new DemoChild();
		// obj1.fun();	// illegal static method call
	}
}
