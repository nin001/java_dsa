// Interface in java
//
// Variables in java
//
// if variables are declared in java , then they are by defualt 
// public static final <dt (int , float etc)> <identifier>
//
// Why compiler adds keywords before variables in interface
//
// public - Accessible everywhere
// static - It is Interfaces variable (class)
// final - addes because implementing class should not change value , which will also refelect in
// other classes implementing it

interface Demo {
	int x = 10;  	// After compilation , public static final int x;
			// bipush value done wherever called 
}

class DemoChild implements Demo {
	void fun() {
		System.out.println(x);
	}
}

class Client {
	public static void main(String args[]) {
		DemoChild obj = new DemoChild();

		obj.fun();
	}
}
