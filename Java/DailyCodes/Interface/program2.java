// Interface 
//
// If interface is implemented in a class and abstract method of that method
// is not implemented in the child class(give body to abstrace method) then 
// the class that is implementing the interfave also needs to be called as 
// abstrace and we cannot create object of the abstract class

interface Demo {
	void fun();
	void gun();
}

abstract class DemoChild implements Demo {
	public void gun() {
		System.out.println("in DemoChild gun");
	}
}

class DemoChild1 extends DemoChild {

	public void fun() {
		System.out.println("in demochild1 fun");
	}
}

class Client {
	public static void main(String args[]) {
		Demo obj = new DemoChild1();
		obj.fun();
		obj.gun();
	}
}
