// hasMoreTokens
import java.util.*;
class InputDemoStringTokenizer{
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);

		System.out.println("Enter information : ");
		String info = input.nextLine();

		StringTokenizer st = new StringTokenizer(info," ");
		
		System.out.println("Number of Tokens in String : "+st.countTokens());		
		int i = 1;
		while(st.hasMoreTokens()){
			System.out.println("Token " + i + " : " + st.nextToken());
			i++;
		}

	}
}
