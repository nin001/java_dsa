// Real life example using string tokenizer and Wrapper class

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.StringTokenizer;

class InputDemoStringTokenizer{
	public static void main(String args[])throws IOException{
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Your Name,Roll no,Number of Live or deadBack,Aggr SGPA,Gender(M/F/O) ");
		String studentInfo = input.readLine();

		StringTokenizer st = new StringTokenizer(studentInfo," ");

		String name = st.nextToken();
		int rollNo = Integer.parseInt(st.nextToken());
		int kt = Integer.parseInt(st.nextToken());
		float agg = Float.parseFloat(st.nextToken());
		char gender = st.nextToken().charAt(0);

		System.out.println("Students Information");
		System.out.println("Name of student : "+name);
		System.out.println("Roll No : "+rollNo);
		System.out.println("Backlogs (live/dead) : "+kt);
		System.out.println("Aggr SGPA  : "+agg);
		System.out.println("Gender : "+gender);
	}
}
