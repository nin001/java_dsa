// Exceptions can be handeled in 2 ways 
//
// 1) by deligating the exception to the caller method
// who will then handel the exception in its code or 
// deligate it further
// Continuous deligation will then reach to JVM , whose
// functionality which is Default Exception Handler will
// then handel the exception but our code flow in terminated
//
// 2) Deligating required each method to use throws keyword 
// lets say if 100 methods have called in sequence , then a 
// single method who is causing the exception will throw the 
// exception (Deligate) and due to that method , each method 
// will now reqire to throw that exception till jvms Default
// exception handler
// To get rid of this , Try-catch-finally is used
// in this , Exception is handeled in the method itself , and
// rest caller methods no need to require the flow of the code

/*1 throws*/
import java.io.*;

class ThrowDemo {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String : ");
		String str = br.readLine();
		// readLine method throws IOException which is called by main method , 
		// and needs to be handeled in main method,
		// If we not deligate the exception to the caller (in this case which is JVM) then
		// there would occur compile time exception
		// UnreportedException IOException must be caught or declared to be thrown
		// at line br.readLine()

	}
}
