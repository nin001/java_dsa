// throws used in any method , then the caller method needs to handel 
// the deligated exception , or itself throw the exception

import java.io.*;

class ThrowDemo {
	void getData()throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();

		System.out.println(str);
	}

	public static void main(String args[])throws IOException {
		ThrowDemo obj = new ThrowDemo();

		obj.getData(); //caller method so , main needs to deligate or handle the exception

	}
}
