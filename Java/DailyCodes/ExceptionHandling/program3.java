// Default exception Handler
//
// Default exception handler is a functon of jvm which gets invoked 
// when we have not handled exception in our program , whenever there
// is a exception , By default exception handler (DEH) handles the exception
// and reports the exception in output
//
// Default exception handler handles just runtime exceptions 

class Demo {
	public static void main(String args[]) {
		int arr[] = new int[]{1,2,3};

		for(int i = 0 ; i<=arr.length ; i++) 
			System.out.println(arr[i]);
	}
}

// Default exception handler gives output or shows exception in this format
// 1) Name of thread which gave exception
// 2) Name of exception class
// 3) (Optional) Description
// 4) Stacktrace
// 
// Exception in "main" thread , java.lang.ArithmaticException : index 3 bounds arr
// 	at Demo.main

