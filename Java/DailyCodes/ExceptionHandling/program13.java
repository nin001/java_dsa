// User defined exceptions
//
// User defined exception can be written by making one of the checked
// or unchecked exception as parent
//
// Super line taking our object in hidden this would reach to the base 
// class and throwable
//
// To show description in exception ,parameterized constructor could be written in our 
// user defined class , whose parameter would be string msg which we would then send to the 
// parent constructor using super(msg);

import java.util.*;

class IndexOverFlowException extends IndexOutOfBoundsException {
	IndexOverFlowException(String msg) {
		super(msg);
	}
}

class IndexUnderFlowException extends IndexOutOfBoundsException {
	IndexUnderFlowException(String msg) {
		super(msg);
	}
}

class Demo {
	public static void main(String args[]) {
		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter elements : ");
		System.out.println("Note : 0 < element < 100");

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.println("Enter element : ");
			int x = sc.nextInt();
			if(x<0) {
				throw new IndexUnderFlowException("Lavdu sangitla hota na 0-100 0 peksha lahan jhala");
			}
			if(x>100) {
				throw new IndexOverFlowException("Lavdu sangitla hota na 0-100 100 peksha motha jhala");
			}
			arr[i] = x;
		}

		for(int x : arr) {
			System.out.println(x);
		}
	}
}

