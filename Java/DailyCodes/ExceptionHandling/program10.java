// to handle exception in java
// we can either delgate the exception to the caller function
// or we can handel the exception using try-catch

import java.util.*;

class ExceptionDemo {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		int x = 0;
		try {
			x = sc.nextInt();
		}catch(NumberFormatException nf) {
			System.out.println("Input number format is impropoer");
		}

		System.out.println("End main");
	}
}
