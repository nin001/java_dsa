// Exception handling in java
//
// Exception - Exception is an event that occurs during the execution or the compilation 
// of the code , an event which breaks the flow of the execution of the code and results in 
// abnormal termination
//
// Exception handling - Method invokation or parts of code which cause exception , because of 
// them whole code is terminated abnormally and , Exception handling gives opportunity to
// handle the exceptions in certain way such that normal code flow doesnt get affected
//
// Classes Heirarhy in exception handling
// In java each exception is a class and which is direct or indirect child of base class which is
// Throwable class , whose parent is Object class. 
// Heirarchy	
// 			Object
// 			 |
// 			Throwable 
// 			 |
// 	------------------------------------
// 	|				   |
// 	Error			        Exception
// 	- VMError			  |
// 	- HeapMemoryFull	---------------------
// 				|		    |
// 			     Checked  	       Unchecked
// 			     (compileTime)	(RUnTime)
// 			      exception		exception
//
// Checked exception
// - exception that is checked by the compiler and which occures in compile time
// example of Checked exception
// - IOException
// - FileNotFoundException
// - InterruptedException
//
// Unchecked Exception
// - Exception that comes in runtime due to an event which caused to terminate the 
//  normal flow of the code 
//  - ArithmeticException
//  - NullPointerException
//  - IndexOutOfBoundException
//  		-ArrayIndexOutOfBound
//  		-StringIndexOutOfBound

import java.io.*;

class ExceptionDemo {
	public static void main(String args[]) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String str = br.readLine();	// readline throws IOException
	}
	// Compile time exception
	// Unreported exception IOException must be caught or declared to be thrown
	
}
