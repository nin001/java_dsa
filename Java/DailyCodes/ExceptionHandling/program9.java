// NullPointerException

class Demo {
	void m1() {
		System.out.println("in m1");
	}

	void m2() {
		System.out.println("in m2");
	}

	public static void main(String args[]) {
		Demo obj = new Demo();

		obj.m1();
	
		obj = null;
		try {
			obj.m2();
		} catch(NullPointerException obj1) {
			System.out.println("object is null");
		}

	}
}

