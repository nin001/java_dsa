// Try - Catch - Finally
//
// Try is a label to check the risky code and throw exceptions occured 
// in try block
// Catch is a label to catch the exceptions thrown by the try statement
//
// Finally is a statement , which is executed wheather exception is handled or not,
// that is weather it goes in one of the catch or to Default exception handler
// Finally is used to close some important connnections that are created in the 
// program execution , as exception can cause in terminating the code abnormally
// so to avoid the connections to remain open even after program termination
// Finally block is written Before terminating the program and going to the default
// exception handler , finally block is once executed

class FinallyDemo {
	void m1() {
		System.out.println("in m1");
	}
	void m2() {
		System.out.println("In m2");
	}

	public static void main(String args[]) {
		Demo obj = new Demo();

		obj.m1();

		obj = null;
		try {
			obj.m2();
		} catch(NullPointerException np) {
			System.out.println("Exception occured");
		}
		finally {
			System.out.println("Connections closed");
		}
	}
	// Points to remember
	// 1- Finally cannot be written without try
	// 2- Finally needs to be in the sequence comming after every catch
 
}
