// Runtime exception 
//
// ArrayIndexOutOfBound is a runtime exception which gives 
// exception when we try to access index of array which is
// out of bound

class ExceptionDemo {
	public static void main(String args[]) {
		int arr[] = new int[]{10,20,30,40,50};

		for(int i = 0 ; i <= arr.length ; i++) {
			System.out.println(arr[i]);
		}
	}
}
