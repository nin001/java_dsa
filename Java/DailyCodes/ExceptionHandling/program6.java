// try-catch 
//
// try catch statements are used to handle exceptions , in this
// try block contains the risky code , which would throw exception 
// and catch block contains the handler code whose parameter is the
// respective exception's object

import java.io.*;

class ExceptionDemo {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter int : ");
		
		int x = 0;
		try{
			x = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj) {
			System.out.println("Sapadla exception");
		}

		System.out.println(x);
	}
}
