// Real time exception handling example

import java.util.*;

class IllegalDocumentsException extends IllegalArgumentException {
	IllegalDocumentsException(String msg) {
		super(msg);
	}
}

class TNP_Document_Verification {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Submit Documents...");
		System.out.println("Validating Documents..");

		System.out.println("Assign if verified (1-If Verified)");
		int verification = sc.nextInt();

		if(verification != 1)
			throw new IllegalDocumentsException("Illegal documents submitted DEBARRED!!");
		else
			System.out.println("Verified Successfully");
	}
}
