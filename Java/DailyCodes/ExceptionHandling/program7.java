// Runtime exception
// Arithmatic exception

class Demo {
	public static void main(String args[]) {
		System.out.println("Start Main");		
		try {
			System.out.println(10/0);
		} catch(ArithmeticException obj) {
			obj.printStackTrace();
			System.out.println("Sapadla exception");
		}
		System.out.println("End main");
	}
}
