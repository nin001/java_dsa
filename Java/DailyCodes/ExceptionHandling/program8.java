
//ArrayIndexOutOfBoundsException
class Demo {
	public static void main(String args[]) {
		int arr[] = new int[]{10,20,30};

		for(int i = 0 ; i<=arr.length ; i++) {
			try{
				System.out.println(arr[i]);
			}catch(ArrayIndexOutOfBoundsException obj) {
				System.out.println("Index "+i+" out of bouds the array");
			}	
		}
	}
}
