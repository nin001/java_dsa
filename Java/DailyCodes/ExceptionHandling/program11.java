// Default exception handler 
// It is a functionality of java , if we have not handeled 
// exceptions in java , then we deligate the exception that is
// being thrown by a method in the caller function,
// So if we have not handled the exceptions and just deligated the
// exception , then that exception is delgated to jvm and then 
// default exception handler handles the exception
//
// default exception handler gives exception msg in the following format
// 1) name of thread
// 2) name of exception class
// 3) (optional) description
// 4) stack trace


// Generating same msg as default exception handler

import java.io.*;

class DEHDemo {
	public static void main(String args[])throws IOException {
		BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Start main");
		int num = Integer.parseInt(obj.readLine());

		obj  = null;

		try {
			int x = Integer.parseInt(obj.readLine());
		} catch(NullPointerException np) {
			System.out.print("Exception in thread "+Thread.currentThread().getName() + " ");
			np.printStackTrace();
		}

		System.out.println("End main");
	}
}
