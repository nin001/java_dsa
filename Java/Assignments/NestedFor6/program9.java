// Palindorme numbers in range

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter start : ");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter end : ");
		int end = Integer.parseInt(br.readLine());
		System.out.println("Strong in range : ");
		for(int i = start ; i<=end ; i++){
			int num = i;
			int strong = 0;
			int temp = num;
			while(temp!=0){
				int rem = temp%10;
				int prod = 1;
				for(int j = 1 ; j<=rem ; j++)
					prod *= j;
				strong += prod;
				temp /= 10;
			}

			if(strong == num)
				System.out.print(strong + " ");
		}
		System.out.println();
	}
}
