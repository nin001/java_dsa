// Take 5 numbers from user and print the number of digits

import java.io.*;

class ForDemo{
	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("How many numbers you want to insert : ");
		int n = Integer.parseInt(br.readLine());
		for(int i = 1 ; i<=n ; i++){
			System.out.println("Enter Number : ");
			int num = Integer.parseInt(br.readLine());
			int count = 0;
			for(int temp = num ; temp!=0 ; temp/=10){
				count++;
			}
			System.out.println(num+" has "+count+" digits");
		}
	}
}
