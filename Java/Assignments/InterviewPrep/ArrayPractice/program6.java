// find min

import java.io.*;
import arrPrac.*;

class InterviewPrep {
	public static void main(String args[]) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		ArrayPractice obj = new ArrayPractice(size);

		int arr[] = obj.create();

		int min = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(min>arr[i])
				min = arr[i];
		}

		System.out.println("Min : "+min);
	}
}
