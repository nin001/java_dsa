// Create an array of 5 dig elements

import arrPrac.*; 
class InterviewPrep {
	public static void main(String args[]) {
	
		ArrayPractice obj = new ArrayPractice(5);

		int arr[] = obj.create();

		for(int x : arr) {
			System.out.println(x);
		}
	}
}
