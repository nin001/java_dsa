// take user input array and print even elements

import arrPrac.*;
import java.util.*;

class InterviewPrep {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		ArrayPractice obj = new ArrayPractice(size);

		int arr[] = obj.create();

		for(int x : arr) {
			if(x%2 == 0) 
				System.out.println(x);
		}
	}
}
