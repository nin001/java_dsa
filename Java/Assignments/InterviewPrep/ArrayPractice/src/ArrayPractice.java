package arrPrac;

import java.util.*;

public class ArrayPractice {
	int size;
	public ArrayPractice(int size) {
		this.size = size;
	}

	public int[] create() {
		Scanner sc = new Scanner(System.in);

		int arr[] = new int[size];
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.println("Enter "+i+"th element : ");
			arr[i] = sc.nextInt();
		}

		return arr;
	}

}

