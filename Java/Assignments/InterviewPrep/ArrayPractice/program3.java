// n values from user

import arrPrac.*;
import java.util.*;

class InterviewPrep {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size : ");
		int size = sc.nextInt();

		ArrayPractice obj = new ArrayPractice(size);

		int arr[] = obj.create();

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.println(arr[i]);
		}
	}
}
