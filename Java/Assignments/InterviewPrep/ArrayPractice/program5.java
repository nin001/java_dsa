// find max

import java.util.*;
import arrPrac.*;

class InterviewPrep {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int size = sc.nextInt();

		ArrayPractice obj = new ArrayPractice(size);

		int arr[] = obj.create();
		
		int max = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(max < arr[i]) {
				max = arr[i];
			}
		}
		System.out.println("Max : "+max);
	}
}
