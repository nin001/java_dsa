//anagram string

import java.io.*;

class Demo {

	static boolean isAnagram(String str1 , String str2) {
		char arr1[] = str1.toCharArray();
		char arr2[] = str2.toCharArray();

		if(arr1.length != arr2.length)
			return false;
		
		for(int i = 0 ; i<arr1.length ; i++){
			for(int j = 0 ; j<arr1.length ; j++){
				if(arr1[i]>arr1[j]){
					char temp = arr1[i];
					arr1[i] = arr1[j];
					arr1[j] = temp;
				}
				if(arr2[i]>arr2[j]){
					char temp = arr2[i];
					arr2[i] = arr2[j];
					arr2[j] = temp;
				}
			}
		}

		for(int i = 0 ; i<arr1.length ; i++) {
			if(arr1[i] != arr2[i])
				return false;
		}

		return true;
				
	}

	public static void main(String args[]) throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter string : ");
		String str1 = input.readLine();

		System.out.println("Enter another string : ");
		String str2 = input.readLine();

		if(isAnagram(str1,str2)){
			System.out.println("String "+str1+" and "+str2+" are anagram string");
		}else{
			System.out.println("String "+str1+" and "+str2+" are not anagram string");
		}
	}
}
