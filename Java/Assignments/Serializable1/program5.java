// print pattern
// row = 3;
// *
// **
// ***
// **
// *

import java.io.*;

class Demo {
	public static void main(String [] args)throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(input.readLine());

		int flag = 1;
		int var = 1;
		for(int i = 1 ; i<=row*2-1 ; i++) {
			if(flag == 1) {
				for(int j = 1 ; j<=i ; j++) {
					System.out.print("* ");
				}
				System.out.println();
				if(i == row)
					flag = 0;
			}else{
				for(int j = var ; j<row ; j++) {
					System.out.print("* ");
				}
				System.out.println();
				var++;
			}
		}
	}
}

