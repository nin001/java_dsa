// program
// take user input 2D array and print only diagonal elements

import java.io.*;

class Demo {

	static void printElements(int arr[][]) {
		for(int i = 0 ; i<arr.length ; i++) {
			for(int j = 0 ; j<arr[i].length ; j++) {
				if(i!=j)
					System.out.print(arr[i][j] + " ");
			}
		}
		System.out.println();
	}
	public static void main(String args[])throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 2D Array : ");
		System.out.println("Enter Row size : ");
		int row = Integer.parseInt(input.readLine());
		System.out.println("Enter Col size : ");
		int col = Integer.parseInt(input.readLine());

		int arr[][] = new int[row][col];

		System.out.println("Enter Elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			for(int j = 0 ; j<arr[i].length ; j++){
				arr[i][j] = Integer.parseInt(input.readLine());
			}
		}

		printElements(arr);
	}
}
