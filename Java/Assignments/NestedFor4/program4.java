//1 2 3 4
//2 3 4
//3 4
//4
class ForDemo{
	public static void main(String a[]){
		int row = 4;
		for(int i = 1 ; i<=row ; i++){
			int var = i;
			for(int j = 1 ; j<=row-i+1 ; j++){
				System.out.print(var++ + " ");
			}	
			System.out.println();
		}
	}
}
