//10
//10 9
//9 8 7
class ForDemo{
	public static void main(String a[]){
		int row = 4;
		int num = ((row*(row+1))/2);
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(i==j)
					System.out.print(num + " ");
				else
					System.out.print(num-- + " ");
			}
			System.out.println();
		}
	}
}
