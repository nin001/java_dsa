//10
//I H
//7 6 5
//D C B A
class ForDemo{
	public static void main(String a[]){
		int row = 4;
		
		int num = (row*(row+1))/2;
		char ch = (char)(64+num);
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(i%2 == 0)
					System.out.print(ch + " ");
				else
					System.out.print(num + " ");
				num--;
				ch--;
			}
			System.out.println();

		}
	}
}
