//1
//23
//345
//4567
class ForDemo{
	public static void main(String a[]){
		int row = 4;
		for(int i = 1 ; i<=row ; i++){
			int num = i;
			for(int j = 1 ; j<=i ; j++){
				System.out.print( num++ + " ");
			}
			System.out.println();
		}
	}
}
