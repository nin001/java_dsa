// 14 15 16 17
// 15 16 17 18
// 16 17 18 19
// 17 18 19 20

class ForDemo{
	public static void main(String a[]){
		int row = 4,col = 4;
		for(int i = 1 ; i<=row ; i++){
			int var = i+13;
			for(int j = 1 ; j<=col ; j++){
				System.out.print(var++ + " ");
			}
			System.out.println();
		}
	}
}
