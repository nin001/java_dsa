// 1C3 4B2 9A1
// 16C3 25B2 36A1
// 49C3 64B2 81A1
class ForDemo{
	public static void main(String a[]){
		int row = 3,col = 3;
		int var1 = 1;
		for(int i = 1 ; i<=row ; i++){
			int var2 = col;
			char ch = 'C';
			for(int j = 1 ; j<=col ; j++){
				System.out.print(var1*var1 + "" + ch + "" +var2 +" ");
				var1++;
				var2--;
				ch--;
			}
			System.out.println();
		}
	}
}
