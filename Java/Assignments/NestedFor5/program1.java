// D4 C3 B2 A1
// A1 B2 C3 D4
// D4 C3 B2 A1
// A1 B2 C3 D4

import java.io.*;

class ForDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());
		
		for(int i = 1 ; i<=row ; i++){
			char ch = (char)(64+row);
			int num = row;
			char ch2 = 'A';
			for(int j = 1 ; j<=row ; j++){
				if(i%2==0)
					System.out.print(ch2++ + "" +j + " ");
				else
					System.out.print(ch-- + "" + num-- + " ");
			}
			System.out.println();
		}
	}
}
