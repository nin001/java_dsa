//54321
//8642
//963
//84
//5

import java.io.*;

class ForDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		for(int i = 1 ; i<=row ; i++){
			int num = row-i+1;
			for(int j = 1 ; j<=row-i+1 ; j++){
				System.out.print(num--*i + " ");
			}
			System.out.println();
		}
	}
}
