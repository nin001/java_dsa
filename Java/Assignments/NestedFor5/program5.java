//fibonacci series for this pattern
//*
//**
//***

import java.io.*;

class ForDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter row :");
		int row = Integer.parseInt(br.readLine());
		
		int temp1 = 0;
		int temp2 = 1;

		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				System.out.print(temp1+ " ");
				int temp3 = temp1;
				temp1 = temp2;
				temp2 = temp1+temp3;
			}
			System.out.println();
		}
	}
}
