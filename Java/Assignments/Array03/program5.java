/*Program 5
WAP to find a Perfect number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 496 564
Output: Perfect no 496 found at index: 3
*/

import java.io.*;

class ArrayDemo{
	static int PerfectNumber(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int temp = 0;
			for(int j = 1 ; j<arr[i] ; j++){
				if(arr[i]%j == 0)
					temp += j;
			}
			if(temp == arr[i])
				return i;
		}
		return -1;
	}

	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret = PerfectNumber(arr);
		if(ret != -1)
			System.out.println("Perfect number "+arr[ret]+" found at index "+ret);
		else
			System.out.println("Perfect number not found");

	
	}
}
