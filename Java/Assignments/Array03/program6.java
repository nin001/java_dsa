/*WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
*/

import java.util.*;

class ArrayDemo{

	static int Palindrome(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int temp = 0;
			int num = arr[i];
			while(num!=0){
				temp = temp*10 + num%10;
				num /=10;
			}
			if(temp == arr[i])
				return i;
		}
		return -1;
	}

	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();
		
		int arr[] = new int[n];

		System.out.println("Enter array elements ;");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = sc.nextInt();
		}

		int ret = Palindrome(arr);
		if(ret != -1){
			System.out.println("Palindrome number "+arr[ret]+" present at index "+ret);
		}else{
			System.out.println("Palindrome number not present");
		}
	}
}
