/*
Program 8
WAP to find an ArmStong number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 153 55 89
Output: Armstrong no 153 found at index: 4
*/

import java.util.*;

class ArrayDemo{

	static int ArmS(int arr[]){
		for(int i = 0 ; i<arr.length ; i++){
			int digCount = 0;
			int temp = arr[i];
			while(temp!=0){
				digCount++;
				temp /= 10;
			}

			temp = arr[i];
			int sum = 0;
			while(temp != 0){
				int mult = 1;
				for(int j = 0 ; j<digCount ; j++){
					mult *= temp%10;
				}
				sum += mult;
				temp /=10;
			}
			if(sum == arr[i])
				return i;
		}
		return -1;
	}

	public static void main(String args[]){
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = sc.nextInt();
		}

		int ret = ArmS(arr);
		if(ret != -1){
			System.out.println("Armstrong number "+arr[ret]+" found at index "+ret);
		}else{
			System.out.println("Armstrong number not found");
		}
	}
}
