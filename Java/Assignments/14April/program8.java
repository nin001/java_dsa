//$
//@@
//&&&
//####

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter row : ");
		int row = Integer.parseInt(br.readLine());

		int temp = 1;
		for(int i = 1 ; i<=row ; i++){
			for(int j = 1 ; j<=i ; j++){
				if(temp==1){
					System.out.print("$ ");
				}else if(temp==2){
					System.out.print("@ ");
				}else if(temp==3){
					System.out.print("& ");
				}else if(temp==4){
					System.out.print("# ");
				}
			}
			System.out.println();
			temp++;
			if(temp>4)
				temp = 1;
		}
	}
}
