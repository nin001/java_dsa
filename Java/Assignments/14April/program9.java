//addition of fact of each digits

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter Number : ");
		int num = Integer.parseInt(br.readLine());
		int sum = 0;
		while(num!=0){
			int rem = num%10;
			int fact = 1;
			while(rem>1)
				fact *= rem--;
			sum+=fact;
			num /=10;
		}
		System.out.println("Addition of factorial : "+sum);
	}
}
