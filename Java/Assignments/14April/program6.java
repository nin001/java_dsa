//difference between char

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter char 1 : ");
		char ch1 = br.readLine().charAt(0);

		System.out.print("Enter char 2 : ");
		char ch2 = br.readLine().charAt(0);

		if(ch1 == ch2){
			System.out.println("Same character : "+ch1);
		}else if((ch1-ch2)<0){
			System.out.println("Difference between "+ch2+" and "+ch1+" is : "+(ch2-ch1));
		}else{
			System.out.println("Difference between "+ch1+" and "+ch2+" is : "+(ch1-ch2));
		}
	}
}
