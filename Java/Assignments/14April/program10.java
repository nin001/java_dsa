// prime nos within given range

import java.io.*;

class Demo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Enter start : ");
		int st = Integer.parseInt(br.readLine());

		System.out.print("Enter end : ");
		int end = Integer.parseInt(br.readLine());

		System.out.println("Prime nos in range ");	
		for(int i = st ; i<=end ; i++){
			int count = 0;
			for(int j = 1 ; j*j<=i ; j++){
				if(i%j == 0)
					count++;
			}
			if(count < 2)
				System.out.print(i + " ");
		}
		System.out.println();
	}
}
