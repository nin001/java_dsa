// Ciel the floor
//
// Find the Cieling and floor value of a number X in the array

import java.util.*;

class BasicArray {

	static void floorAndCieling(int arr[] , int x) {

		int ciel = -1;
		int floor = -1;

		int cd = Integer.MAX_VALUE;
		int fd = Integer.MAX_VALUE;

		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[i] >= x && cd >(arr[i] - x)) {
				ciel = arr[i];
				cd = arr[i]-x;
			}

			if(arr[i] <= x && fd > (x - arr[i])) {
				floor = arr[i];
				fd = x-arr[i];
			}
		}

		if(floor != Integer.MAX_VALUE && ciel != Integer.MAX_VALUE) {
			System.out.println("Floor of "+x+" is "+floor+" and ciel is "+ciel);
		}else {
			System.out.println("INVALID");
		}

	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter Value of X :");
		int x = sc.nextInt();

		floorAndCieling(arr,x);
	}
}
