// Product of array

import java.io.*;

class BasicArray {

	static int productArray(int arr[]) {

		int prod = 1;
		for(int i = 0 ; i<arr.length ; i++) {
			prod *= arr[i];
		}
		return prod;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter No of TestCases : ");
		int T = Integer.parseInt(br.readLine());
		
		while(T != 0) {
			System.out.println("Enter Size :");
			int size = Integer.parseInt(br.readLine());
	
			int arr[] = new int[size];
			System.out.println("Enter array elements");
			for(int i = 0 ; i<arr.length ; i++) {
				arr[i] = Integer.parseInt(br.readLine());
			}
		
			System.out.println("Product of Arr : "+productArray(arr));
			T--;
		}
	}
}
