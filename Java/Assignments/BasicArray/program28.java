// Remove duplicates from an array (unsorted)

import java.util.*;

class BasicArray {

	static void shiftElements(int arr[] , int start , int end) {
		for(int i = start+1 ; i<end ; i++) {
			arr[i-1] = arr[i];
		}
	}

	static int removeDuplicate(int arr[]) {

		int count = 0;
		for(int i = 0 ; i<arr.length-count ; i++) {
			for(int j = i+1 ; j<arr.length-count ; j++) {
				if(arr[i] == arr[j]) {
					shiftElements(arr,j,arr.length-count);
					count++;
				}
			}
		}

		return arr.length-count;
	}

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int newSize = removeDuplicate(arr);

		for(int i = 0 ; i<newSize ; i++) {
			System.out.print(arr[i]+ " ");
		}
		System.out.println();
	}
}
