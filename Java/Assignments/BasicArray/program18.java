// Find subarray with given sum

import java.io.*;

interface Subarray {
	void subarrayWithGivenSum(int arr[] , int sum);
}

class BasicArray {
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter sum :");
		int sum = Integer.parseInt(br.readLine());
		
		Subarray obj = (a,s)-> {

			for(int i = 0 ; i<a.length ; i++) {
				int temp = 0;
				for(int j = i ; j<a.length ; j++) {
					temp += a[j];
					if(temp == s) {
						System.out.println("Sum found At index "+i+" and Index "+j);
						return;
					}
					if(temp>s)
						break;
				}
			}
			System.out.println("No subarray found");
		};

		obj.subarrayWithGivenSum(arr,sum);
	}
}
