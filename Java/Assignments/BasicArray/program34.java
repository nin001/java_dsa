// Print Array in Pendulam arrangement

import java.util.*;

class BasicArray {

	static void printPendulam(int arr[]) {

		int temp[] = arr;
		Arrays.sort(temp);

		int index = arr.length-1;
		int pendFlag = 0;
		while(index < arr.length) {
			System.out.print(temp[index]+ " ");
			if(pendFlag == 0) {
				index = index - 2;
				if(index == 0) {
					pendFlag = 1;
				}
			}else {
				index = index + 2;
				if(index == arr.length) {
					break;
				}
			}
		}
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		printPendulam(arr);

	}
}
