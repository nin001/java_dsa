// Last index of One in given String

import java.io.*;

class BasicArray {

	static int lastIndexOne(StringBuffer str) {

		int index = 0;
		int last = -1;
		int length = str.length();
		while(index < length) {
			if(str.charAt(index) == '1') {
				last = index;
			}
			index++;
		}
		return last;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String :");
		StringBuffer str = new StringBuffer("");

		str.append(br.readLine());

		System.out.println(lastIndexOne(str));

		
		
	}
}
