// maximise sum arr[i]*i of an array

import java.util.*;

class BasicArray {

	static int maximizeSum(int arr[]) {
		// sorting array
		Arrays.sort(arr);

		int sum = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			sum += (arr[i]*i);
		}

		return sum;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size : ");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int sum = maximizeSum(arr);
		System.out.println("Sum : " + sum);

	}
}
