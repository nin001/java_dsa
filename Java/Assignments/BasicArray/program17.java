// product of maximum in first and minimum in second array

import java.io.*;

class BasicArray {

        static int prodFunction(int arr1[] , int arr2[]) {

                int len = 0;
                if(arr1.length > arr2.length) {
                        len = arr1.length;
                }else {
                        len =  arr2.length;
                }

                int min = arr2[0];
                int max = arr1[0];
                for(int i = 1 ; i<len ; i++) {
                        //MIN
                        if(i<arr2.length && min>arr2[i]) {
                                min = arr2[i];
                        }else if(i<arr1.length && max<arr1[i]) {
                                max = arr1[i];
                        }
                }

                System.out.println(max + " " + min);

                return max*min;

        }

        public static void main(String args[])throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.println("Enter size1 : ");
                int size1 = Integer.parseInt(br.readLine());

                int arr1[] = new int[size1];

                System.out.println("Enter Array1 elements :");
                for(int i = 0 ; i<arr1.length ; i++) {
                        arr1[i] = Integer.parseInt(br.readLine());
                }

                System.out.println("Enter size2 : ");
                int size2= Integer.parseInt(br.readLine());

                int arr2[] = new int[size2];

                System.out.println("Enter Array2 elements :");
                for(int i = 0 ; i<arr2.length ; i++) {
                        arr2[i] = Integer.parseInt(br.readLine());
                }

                int prod = prodFunction(arr1,arr2);
                System.out.println("Product : "+prod);
        }
}
