// Maximum repeating number where array has numbers in given range k-1

import java.io.*;

class BasicArray {

	static int maxRepeating(int arr[] , int k) {
		int countArr[] = new int[k];
		for(int i = 0 ; i<k ; i++) {
			countArr[i] = 0;
		}

		for(int i = 0 ; i<arr.length ; i++) {
			countArr[arr[i]]++;
		}

		int maxIndex = 0;
		for(int i = 1 ; i<k ; i++) {
			if(countArr[maxIndex]<countArr[i]) {
				maxIndex = i;
			}
		}

		return maxIndex;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Give Range Highest Value(0-K) :");
		int k = Integer.parseInt(br.readLine());

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret = maxRepeating(arr,k);

		if(ret != -1) {
			System.out.println(ret);
		}else {
			System.out.println(-1);
		}
	}
}
