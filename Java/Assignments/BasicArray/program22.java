// Exceptionally Odd
// Given array and all numbers even number of times and except one
// number occuring odd times
// Return Exceptionally odd number

import java.io.*;

class BasicArray {

	static int flag = 0;
	static int exceptionallyOdd(int arr[]) {
		
		for(int i = 0 ; i<arr.length ; i++) {
			int count = 0;
			for(int j = 0 ; j<arr.length ; j++) {
				if(arr[i] == arr[j]) {
					count++;
				}
			}
			if(count%2!=0) {
				flag = 1;
				return arr[i];
			}
		}
		return -1;
	}

	public static void main(String[] args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Array elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ele = exceptionallyOdd(arr);
		
		if(flag == 1) {
			System.out.println("Exceptionally Odd elements :"+ele);
		}else {
			System.out.println("No element found");
		}	
	}
}
