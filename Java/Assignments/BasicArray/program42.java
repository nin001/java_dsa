// Count number of elements between the range given ,in user input array(unsorted)

import java.util.*;

class BasicArray {

	static int elementsBetween(int arr[] , int a , int b) {
		int left = -1;
		int right = -1;

		for(int i = 0 ; i<arr.length ; i++) {
			if(left == -1 && arr[i] == a) {
				left = i;
			}
			if(arr[i] == b) {
				right = i;
			}
		}

		return right-left-1;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter num1 :");
		int num1 = sc.nextInt();
		System.out.println("Enter num2 :");
		int num2 = sc.nextInt();
		int count = elementsBetween(arr,num1,num2);

		System.out.println("Count : " + count);
	}
}
