// Find the closest number

import java.io.*;
import java.util.*;

class BasicArray {

	static int closestNumber(int arr[] , int k) {

		int index = 0;
		int diff = Math.abs(k-arr[index]);
		for(int i = 1 ; i<arr.length ; i++) {
			
			if(diff>(Math.abs(arr[i]-k))) {
				index = i;
				diff = Math.abs(arr[i]-k);
			}
			
		}

		return arr[index];
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter K :");
		int K = Integer.parseInt(br.readLine());

		System.out.println("Closest Number : "+closestNumber(arr,K));
	}
}

