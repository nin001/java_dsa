// Immediate Smaller elements

import java.util.*;

class BasicArray {

	static void immediateSmaller(int arr[]) {

		for(int i = 0 ; i<arr.length ; i++) {
			if(i == arr.length-1) {
				System.out.println("-1");
				break;
			}

			if(arr[i]>arr[i+1]) {
				System.out.print(arr[i+1] + " ");
			}else {
				System.out.print("-1 ");
			}
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		immediateSmaller(arr);
	}
}
