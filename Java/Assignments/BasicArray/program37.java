// Move all negative numbers to the start and positive to the end

import java.util.*;

class BasicArray {

	static void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static void moveElements(int arr[]) {

		int itr = 0;
	
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i]<0) {
				if(i!=itr) {
					swap(arr,i,itr);
				}
				itr++;
			}
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}
		
		moveElements(arr);
		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
