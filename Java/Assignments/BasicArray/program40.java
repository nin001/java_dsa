// print Fibonacci number in array

import java.util.*;

class BasicArray {

	static boolean checkFibonacci(int x) {
		int temp1 = x+4;
		int sq1 = (int)Math.sqrt(temp1);
		if(temp1 == sq1*sq1) {
			return true;
		}

		temp1 = x-4;
		sq1 = (int)Math.sqrt(temp1);
		if(temp1 == sq1*sq1) {
			return true;
		}
		return false;
	}

	// Formula : 5n^2+4 & 5n^2-4
	// if the number occured is perfect square then 
	// it is present in series
	static void fiboInArray(int arr[]) {
		for(int i = 0 ; i<arr.length ; i++) {
			int formula = 5*arr[i]*arr[i];
			if(checkFibonacci(formula)) {
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		fiboInArray(arr);
	}
}
