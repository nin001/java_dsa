// Find smallest and second smallest element in array

import java.util.*;

class BasicArray {

	static void findSmallestAndSecondSmallest(int arr[]) {

		int min = Integer.MAX_VALUE;
		
		for(int i = 0 ; i<arr.length ; i++) {
			if(min>arr[i]) {
				min = arr[i];
			}
		}

		int sec = Integer.MAX_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(sec>arr[i] && arr[i]>min) {
				sec = arr[i];
			}
		}

		if(sec != Integer.MAX_VALUE) {
			System.out.println(min + " " + sec);
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		findSmallestAndSecondSmallest(arr);
	}
}
