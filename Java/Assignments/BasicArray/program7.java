// Form largest Number from the given array

import java.util.*;

class BasicArray {

	static int formMax(List arr) {
		
		Collections.sort(arr);

		int max = 0;

		int index = arr.size()-1;
		while(index != -1) {
			max = max*10 + (int)arr.remove(index--);
		}

		return max;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size : ");
		int size = sc.nextInt();

		List arr = new ArrayList();

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<size ; i++) {
			arr.add(sc.nextInt());
		}

		int MAX_NO = formMax(arr);

		System.out.println("Max number formed : " + MAX_NO);
	}
}
