// Remove a specific index element from array

import java.util.*;

class BasicArray {
	
	// Return new size
	static int removeElement(int arr[] , int i) {
		for(int itr = i ; itr<arr.length-1 ; itr++) {
			arr[itr] = arr[itr+1];
		}

		return arr.length-1;
	}
		
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter index :");
		int index = sc.nextInt();

		if(index>=arr.length && index<0) {
			System.out.println("Invalid Index");
		}else {
			int newSize = removeElement(arr,index);
			for(int i = 0 ; i<newSize ; i++) {
				System.out.print(arr[i] + " ");
			}
			System.out.println();
		}
	
	}
}
