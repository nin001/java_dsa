// Sum of Distinct elements

import java.util.*;

class BasicArray {

	static int distinctSum(int arr[]) {
		int temp[] = Arrays.copyOf(arr,arr.length);
		Arrays.sort(temp);

		int sum = 0;
		int store = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(i == 0) {
				sum += arr[i];
				store = arr[i];
			}else {
				if(arr[i] == store) {
					continue;
				}
				store = arr[i];
				sum += arr[i];
			}
		}

		return sum;
				
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println(distinctSum(arr));
	}
}
