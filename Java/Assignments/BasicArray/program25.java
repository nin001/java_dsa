// MAximum product of 2 mumbers from array
//
// Max product will be product of max and sec max

import java.io.*;

class BasicArray {

	static int maxProduct(int arr[]) {
		int max = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(max <= arr[i]) {
				max = arr[i];
			}
		}

		int secMax = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(secMax < arr[i] && arr[i] < max) {
				secMax = arr[i];
			}
		}

		return max*secMax;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

	
		System.out.println(maxProduct(arr));
	}
}
