// First and last occurance of X

import java.util.*;

class BasicArray {

	void firstAndLastOcc(int arr[] , int x) {

		int first = -1;
		int last = -1;

		int foundFirstFlag = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == x && foundFirstFlag == 0) {
				first = i;
				last = i;
				foundFirstFlag = 1;
			}
			if(arr[i] == x && foundFirstFlag == 1) {
				last = i;
			}
		}

		if(first != -1)
			System.out.println(first+ " " + last);
		else
			System.out.println("No occurance");

	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();

		}

		System.out.println("Enter X :");
		int X = sc.nextInt();

		BasicArray obj = new BasicArray();
		obj.firstAndLastOcc(arr,X);


	}
}
		
