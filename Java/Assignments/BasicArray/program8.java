// Even occuring elements

import java.io.*;
import java.util.*;

class BasicArray {

	static void evenOccuringElements(int arr[]) {

		Arrays.sort(arr);

		int temp = arr[0];
		int count = 1;
		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[i] == temp) {
				count++;
			}else {
				if(count%2 == 0) {
					System.out.print(temp + " ");
				}
				temp = arr[i];
				count = 1;
			}
		}
			
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		evenOccuringElements(arr);
	}
}


