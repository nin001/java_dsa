//Positive and negative numbers
//An array has equal number of positive and negative numbers 
//arrange elements withoud breaking relative order such that 
//there are alternate positive and negative number

import java.util.*;

class BasicArray {

	static void arrangeElements(int arr[]) {
		int temp[] = new int[arr.length];

		int tempIndex = 0;
		int flag = 0;
		int count = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(flag == 0 ) {
				if(arr[i] > 0) {
					temp[tempIndex++] = arr[i];
					arr[i] = 0;
					flag = 1;
					count++;
					i = 0;
				}
				
			}else {
				if(arr[i] < 0) {
					temp[tempIndex++] = arr[i];
					arr[i] = 0;
					count++;
					flag = 0;
					i = 0;
				}

			}
			if(count == arr.length) {
				break;
			}
		}

		// Print
		for(int i = 0 ; i<temp.length ; i++) {
			System.out.print(temp[i] + " ");
		}
		System.out.println();

	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		arrangeElements(arr);
/*
		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i]+" ");
		}
		System.out.println();*/
	}
}
