// Contries at war
/*The two countries of A and B are at war against each other. Both countries have N
number of soldiers. The power of these soldiers are given by A[i]...A[N] and
B[i]....B[N].
These soldiers have a peculiarity. They can only attack their counterpart enemies,
like A[i] can attack only B[i] and not anyone else. A soldier with higher power can
kill the enemy soldier. If both soldiers have the same power, they both die. You
need to find the winning country.*/

import java.io.*;

class BasicArray {

	static void war(int A[] , int B[]) {
		int ACount = 0;
		int BCount = 0;
		for(int i = 0 ; i<A.length ; i++) {
			//A soldier dead
			if(A[i] < B[i]) {
				ACount++;
			}else if(A[i] > B[i]) {	// B soldier dead
				BCount++;
			}
		}

		if(ACount > BCount) {
			System.out.println("B won");
		}else if(ACount < BCount) {
			System.out.println("A won");
		}else {
			System.out.println("Both lost");
		}
	}
		
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter soldierCount :");
		int solCount = Integer.parseInt(br.readLine());

		int country1[] = new int[solCount];
		System.out.println("Enter power for country1 :");
		for(int i = 0 ; i<solCount ; i++) {
			country1[i] = Integer.parseInt(br.readLine());
		}

		int country2[] = new int[solCount];
		System.out.println("Enter power for country2 :");
		for(int i = 0 ; i<solCount ; i++) {
			country2[i] = Integer.parseInt(br.readLine());
		}

		war(country1,country2);
	}
}
