// Search element in Array
// Return Index when found

import java.util.*;

class BasicArray {

	static int searchElement(int arr[] , int key) {

		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == key) {
				return i;
			}
		}
		return -1;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int size = sc.nextInt();

		int arr[] = new int[size];
		System.out.println("Enter array Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter key : ");
		int key = sc.nextInt();

		System.out.println(searchElement(arr,key));
	}
}
