// Given three sorted Array 
// Find common elements in array

import java.util.*;

class BasicArray {

	static void findCommonElements(int arr1[] , int arr2[] , int arr3[]) {

		int i1 = 0;
		int i2 = 0;
		int i3 = 0;
		while(i1 < arr1.length && i2 < arr2.length && i3 < arr3.length ) {

			if(arr1[i1] < arr2[i2] && arr1[i1] < arr3[i3]) {
				i1++;
			}else if(arr2[i2]<arr1[i1] && arr2[i2]<arr3[i3]) {
				i2++;
			}else if(arr3[i3]<arr1[i1] && arr3[i3]<arr2[i2]) {
				i3++;
			}else if(arr1[i1] == arr2[i2] && arr2[i2] == arr3[i3]) {
				System.out.print(arr1[i1]+" ");
				i1++;
				i2++;
				i3++;
			}else {
				if(arr1[i1] == arr2[i2]) {
					i1++;
					i2++;
				}else if(arr2[i2] == arr3[i3]) {
					i2++;
					i3++;
				}else {
					i1++;
					i3++;
				}
			}

		}
		System.out.println();
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int s1 = sc.nextInt();

		int arr1[] = new int[s1];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr1.length ; i++) {
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter Size :");
		int s2 = sc.nextInt();

		int arr2[] = new int[s2];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr2.length ; i++) {
			arr2[i] = sc.nextInt();
		}


		System.out.println("Enter Size :");
		int s3 = sc.nextInt();

		int arr3[] = new int[s3];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr3.length ; i++) {
			arr3[i] = sc.nextInt();
		}

		findCommonElements(arr1,arr2,arr3);

	}
}
