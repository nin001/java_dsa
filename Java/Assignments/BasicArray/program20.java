// Two sum
// Checl if given sum exists in Array

import java.io.*;

class BasicArray {

	static String twoSum(int arr[] , int x) {

		for(int i = 0 ; i<arr.length ; i++) {
			int temp = arr[i];
			for(int j = i+1 ; j<arr.length ; j++) {
				temp += arr[j];
				if(temp == x) {
					return new String("Yes");
				}

				temp -= arr[j];
			}
		}
		return new String("No");
				
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter X :");
		int x = Integer.parseInt(br.readLine());

		System.out.println(twoSum(arr,x));


	}
}
