// Given an Integer n convert all 0s to 5

import java.util.*;

class BasicArray {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter number : ");
		int num = sc.nextInt();

		int temp = num;
		num = 0;
		while(temp !=0) {
			int rem = temp%10;
			if(rem == 0) {
				num = num*10 + 5;
			}else {
				num = num*10 + rem;
			}
			temp /=10;
		}

		temp = num;
		num = 0;
		while(temp != 0) {
			num = num*10 + temp%10;
			temp /= 10;
		}
		System.out.println(num);

			
	}
}
