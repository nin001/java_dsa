// Maximum odd sum

import java.util.*;

class BasicArray {

	static int maxOddSum(int arr[]) {

		int min = Integer.MAX_VALUE;
		int sum = 0;
		int flag = 0;
		
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i]>0) {
				sum += arr[i];
			}

			if(arr[i]%2 != 0) {
				flag = 1;
				if(min>Math.abs(arr[i])) {
					min = Math.abs(arr[i]);
				}

			}
		}

		if(flag == 0)
			return -1;
		
		if(sum%2 != 0) {
			return sum;
		}
		return sum-min;

	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int size = sc.nextInt();

		int arr[] = new int[size];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		int sum = maxOddSum(arr);
		System.out.println(sum);
	}
}
