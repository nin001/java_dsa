// Sum of pairs f(a[i],[j])

import java.io.*;

class BasicArray {

	static int pairSum(int arr[]) {
		int sum = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			for(int j = 0 ; j<arr.length ; j++) {
				int diff = arr[j]-arr[i];
				if(Math.abs(diff)>1 && 0<=i && i<j && j<arr.length) {
					sum += diff;
				}
				
			}
			//System.out.println(sum);
		}
		return sum;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter array elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int sum = pairSum(arr);
		System.out.println("Sum :"+sum);
	}
}
