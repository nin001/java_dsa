// Left sum and right sum of an array where array is divided half and
// then return product of leftsum and rightsum

import java.io.*;

class BasicArray {
	
	static int leftSumRightSum(int arr[]) {
		int n = arr.length;

		int left = 0;
		int right = 0;
		
		for(int i = 0 ; i<arr.length ; i++) {
			if(i<(n/2)) {
				left += arr[i];
			}else {
				right += arr[i];
			}
		}

		return left*right;
	}

	public static void main(String args[]) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int prodSubArray = leftSumRightSum(arr);

		System.out.println("Prod : "+prodSubArray);
	
	}
}
