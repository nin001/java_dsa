// Find the peak element in An array
// A peak elements is a element which is Higher than its neighbour elements'

import java.io.*;

class BasicArray {

	static int flag = 0;

	static int peakElement(int arr[]) {
		
		for(int i = 0 ; i<arr.length ; i++) {
			if(i == 0) {
				if(arr[i] > arr[i+1]) {
					return arr[i];
				}
			}else if(i == arr.length-1) {
				if(arr[i] > arr[i-1]) {
					return arr[i];
				}
			}else {
				if(arr[i]>arr[i-1] && arr[i]>arr[i+1]) {
					return arr[i];
				}
			}
		}

		flag = 1;
		return -1;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());

		}

		int peakEle = peakElement(arr);
		if(flag == 1) {
			System.out.println("All elements arr same");
		}else {
			System.out.println("Peak Element : " + peakEle);
		}
	}
}
