// Find unique element
// Given array of size n which contains  all elements occuring in multiples
// of except 1 find that element

import java.io.*;
import java.util.*;

class BasicArray {

	static int uniqueNo(int arr[] , int k ) {
		if(k>arr.length) 
			return -1;
		
		Arrays.sort(arr);
	
		for(int i = 0 ; i<arr.length ; i++) {
			int count = 0;
			int j = 0;
			for(j = i ; j<arr.length && arr[j]==arr[i] ; j++) {
				count++;
			}
			if(count%k != 0) {
				return arr[i];
			}

			i = j-1;
		}

		return 0;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter array elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter K :");
		int k = Integer.parseInt(br.readLine());

		int ret = uniqueNo(arr,k);
		if(ret == -1)
			System.out.println("INVALID INPUT");
		else if(ret == 0)
			System.out.println("No Such element found");
		else 
			System.out.println("unique element : "+ret);
		
	}
}
