// LEADERS OF ARRAY
// leader is a number which is greater than all the elements on its right

import java.io.*;

class BasicArray {

	static void leadersOfArray(int arr[]) {

		for(int i = 0 ; i<arr.length ; i++) {
			int leaderFlag = 0;
			for(int j = i+1 ; j<arr.length ; j++) {
				if(arr[j] > arr[i]) {
					leaderFlag = 1;
					break;
				}
			}
			if(leaderFlag == 0) {
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException {
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		leadersOfArray(arr);
	}
}
