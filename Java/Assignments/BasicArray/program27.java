// Count pair sum
// Given 2 sorted array and sum , count the pairs whose sum is equal to the 
// given sum

import java.util.*;

class BasicArray {

	// can use linear search but will increase TC , using binary search
	static boolean isPresent(int val , int arr[]) {
		int start = 0;
		int end = arr.length - 1;
		int mid = (start+end)/2;
		while(start<=end) {
			if(arr[mid] == val) {
				return true;
			}else if(arr[mid] < val) {
				start = mid+1;
			}else {
				end = mid-1;
			}
			mid = (start+end)/2;
		}
		return false;
	}

	static int countPairs(int arr1[] , int arr2[] , int sum) {

		int count = 0;
		
		for(int i = 0 ; i<arr1.length ; i++) {
			if(arr1[i] >= sum) {
				continue;
			}
			int val = sum - arr1[i];

			if(isPresent(val,arr2)){
				count++;
			}
		}
		return count;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n1 = sc.nextInt();

		int arr1[] = new int[n1];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n1 ; i++) {
			arr1[i] = sc.nextInt();
		}

		
		System.out.println("Enter size :");
		int n2 = sc.nextInt();

		int arr2[] = new int[n2];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n2 ; i++) {
			arr2[i] = sc.nextInt();
		}


		System.out.println("Enter sum :");
		int sum = sc.nextInt();

		System.out.println(countPairs(arr1,arr2,sum));

	}
}
