// Elements in range

import java.io.*;

class BasicArray {
	static String checkArray(int arr[] , int A , int B) {

		int size = 0;
		if(A>B) {
			size = B-A+1;
		}else {
			size = A-B+1;
		}

		if(size>arr.length) {
			return new String("No");
		}

		int temp[] = new int[B];

		for(int i = 0 ; i<temp.length ; i++) {
			temp[i] = 0;
		}

		for(int i = A ; i<B ; i++) {
			temp[i] = 1;
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i]>=A && arr[i]<B) {
				temp[arr[i]] = 0;
			}
		}

		for(int i = 0 ; i<temp.length ; i++) {
			if(temp[i] == 1) {
				return new String("No");
			}
		}
		return new String("Yes");
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int size = Integer.parseInt(br.readLine());
		
		int arr[] = new int[size];

		System.out.println("Enter array elements :");

		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter range :");
		int A = Integer.parseInt(br.readLine());
		int B = Integer.parseInt(br.readLine());

		System.out.println(checkArray(arr,A,B));

	}
}
