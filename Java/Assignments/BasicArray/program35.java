// Minimum product of K integers

import java.util.*;

class BasicArray {

	static int flag = 0;
	static int prodOfK(int arr[] , int k) {

		if(k>arr.length && k<0) {
			flag = 1;
			return -1;
		}

		Arrays.sort(arr);
		int prod = 1;
		
		for(int i = 0 ; i<k ; i++) {
			prod *= arr[i];
		}

		return prod;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter K :");
		int k = sc.nextInt();

		System.out.println(prodOfK(arr,k));

	}
}
