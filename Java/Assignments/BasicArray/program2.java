// Find minimum and maximum element in array

import java.io.*;

class BasicArray {

	void findMinAndMax(int arr[]) {
	
		int min = arr[0];
		int max = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[i]>max){
				max = arr[i];
			}
			if(arr[i]<min) {
				min = arr[i];
			}
		}

		System.out.println("Max : "+max+"\nMin : "+min);
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size : ");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter array elements : ");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		BasicArray obj = new BasicArray();
		obj.findMinAndMax(arr);
	}
}
