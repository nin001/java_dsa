// First element to occur K times

import java.util.*;

class BasicArray {

	static int flag = 0;

	static int firstElementKTimes(int arr[] , int k) {

		int max = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			if(max<arr[i]) {
				max = arr[i];
			}
		}

		int countArr[] = new int[max];
		for(int i = 0 ; i<max ; i++) {
			countArr[i] = 0;
		}

		for(int i = 0 ; i<arr.length ; i++) {
			countArr[arr[i]]++;
			if(countArr[arr[i]] == k) {
				flag = 1;
				return arr[i];
			}
		}

		return -1;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter K :");
		int k = sc.nextInt();

		int ele = firstElementKTimes(arr,k);
		if(flag == 1) {
			System.out.println("Element First occured K times : "+ele);
		}else {
			System.out.println("NO element");
		}
	}
}
