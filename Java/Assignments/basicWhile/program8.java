// reverse number 
class While{
	public static void main(String a[]){
		int num = 145;
		int rev = 0;
		while(num!=0){
			rev = rev*10 + num%10;
			num /= 10;
		}
		System.out.println("Reversed number " + rev);
	}
}

