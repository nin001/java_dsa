// Non repeating elements in array

import java.io.*;
import java.util.*;

class ArrayEasy {

	static int nonRepeatingElement(int arr[]) {

		Map m = new HashMap();

		for(int i = 0 ; i<arr.length ; i++) {
			if(m.containsKey(arr[i])) {
				m.put(arr[i],((Integer)m.get(arr[i]))+1);
			}else {
				m.put(arr[i],1);
			}
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(((Integer)m.get(arr[i])) == 1) {
				return arr[i];
			}
		}
		return -1;
	}

	
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

	        int nonRep = nonRepeatingElement(arr);

		System.out.println(nonRep);

	}
}
