// Three great candidates
// Maximum product of three candidates in will give best candidates with high productivity

import java.io.*;

class ArrayEasy {

	static int threeGreatCandidates(int arr[]) {
		int fmax = arr[0];
		for(int i  = 1 ; i<arr.length ; i++) {
			if(arr[i]>fmax) {
				fmax = arr[i];
			}
		}

		int smax = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i]>smax && arr[i]<fmax) {
				smax = arr[i];
			}
		}

		int tmax = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i]>tmax && arr[i]<smax) {
				tmax = arr[i];
			}
		}

		return fmax*smax*tmax;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int maxAbility = threeGreatCandidates(arr);

		System.out.println("Maximum collective ability : "+maxAbility);
	}
}
