// Move all zeros to the end of array with following the order of non-zero elements

import java.io.*;

class ArrayEasy {

	static void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static void moveZerosToEnd(int arr[]) {
		int n = arr.length;
		
		int i = 0;
		int j = n-1;
		int swapFlag = 0;
		while(i!=j) {
			if(arr[i] == 0 && swapFlag == 0) {
				swap(arr,i,j);
				i++;
				j--;
				swapFlag = 1;
			}else if(arr[i] != 0 && swapFlag == 0) {
				i++;
			}else if(arr[i] != 0 && swapFlag == 1) {
				swap(arr,i-1,i);
				i++;
				swapFlag = 1;
			}else {
				i++;
			}
		}

	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		moveZerosToEnd(arr);
		
		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
