// Rotate Array

import java.util.*;

class ArrayEasy {

	static void rotate(int arr[]) {
		int temp = arr[0];
		for(int i = 1 ; i<arr.length ; i++) {
			arr[i-1] = arr[i];
		}
		arr[arr.length-1] = temp;
	}

	static void rotateArray(int arr[] , int d) {
		if(d>arr.length) {
			d = d%arr.length;
		}

		for(int i = 0 ; i<d ; i++) {
			rotate(arr);
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter D :");
		int d = sc.nextInt();

		rotateArray(arr,d);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
