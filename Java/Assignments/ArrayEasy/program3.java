// Find dupicates in array

import java.io.*;
import java.util.*;

class ArrayEasy {

	static int[] duplicates(int arr[]) {
		int ret[] = new int[arr.length];

		Arrays.sort(arr);
		
		int flag = 0;
		int index = 0;
		int store = -1;
		for(int i = 0 ; i<arr.length-1 ; i++) {
			if(arr[i] == arr[i+1] && arr[i]!=store) {
				store = arr[i];
				ret[index++] = store;
				flag = 1;
			}
		}

		if(flag == 0) {
			int ret1[] = new int[1];
			ret1[0] = -1;
			return ret1;
		}

		return ret;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int ret[] = duplicates(arr);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
