// Given array of 0s 1s find transition point(sorted array)

import java.util.*;

class ArrayEasy {

	static int transitionPoint(int arr[]) {
		int point = -1;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == 1) {
				point = i;
				break;
			}
		}
		return point;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int transitionIndex = transitionPoint(arr);
		System.out.println(transitionIndex);
	}
}
