// Even and odd arrangement in array

import java.io.*;

class ArrayEasy {

	static void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static int evenOddArrange(int arr[]) {
		
		int eIndex = 0;
		int oIndex = 1;

		while(eIndex<arr.length && oIndex<arr.length) {
			if(arr[eIndex]%2 != 0 && arr[oIndex]%2 == 0) {
				swap(arr,eIndex,oIndex);
			}else {
				if(arr[eIndex]%2 == 0) {
					eIndex += 2;
				}else {
					oIndex += 2;
				}
			}
		}

		return 1;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		System.out.println(evenOddArrange(arr));

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
