// Find the Bitonic point
// Array with first strictly increasing and then maybe strictly decreasing find the
// maximum number in the array

import java.io.*;

class ArrayEasy {

	static int bitonicPoint(int arr[]) {
		
		int maxIndex = -1;

		int start = 0;
		int end = arr.length-1;

		int mid = (start+end)/2;
		while(start<=end) {
			if(arr[mid]>arr[mid-1] && arr[mid]>arr[mid+1]) {
				return arr[mid];
			}else if(arr[mid]<arr[mid+1]) {
				start = mid+1;
			}else {
				end = mid-1;
			}

			mid = (start+end)/2;
		}

		return -1;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int bp = bitonicPoint(arr);

		System.out.println(bp);
	}
}
