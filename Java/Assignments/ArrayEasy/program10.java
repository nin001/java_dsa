// Find Repeating element (first)

import java.io.*;
import java.util.*;

class ArrayEasy {

	static int repeatingElement(int arr[]) {

		int temp[] = Arrays.copyOf(arr,arr.length);
		int tempIndex = 0;
		int store = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(i == 0) {
				temp[tempIndex++] = arr[i];
			}
			for(int j = 0 ; j<tempIndex ; j++) {
				if(arr[i] == temp[j]) {
					store = arr[i];
					break;
				}
			}
			if(store != Integer.MIN_VALUE)
				break;
		}

		if(store == Integer.MIN_VALUE)
			return -1;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == store) {
				return i+1;
			}	
		}

		return -1;
					
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();

		}

		int ret = repeatingElement(arr);
		if(ret != -1) {
			System.out.println("Repeating element index : "+ ret);
		}else {
			System.out.println("NOT FOUND");
		}
	}
}
