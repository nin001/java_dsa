// Missing elements in Array

import java.io.*;

class ArrayEasy {

	static int findMissingElement(int arr[]) {
		// Sum of 1-n = n(n+1)/2		//as size is n and present missing element 
		// 					we will use (n+1)(n+2)/2 
		int sum = ((arr.length+1)*(arr.length+2))/2;

		for(int i = 0 ; i<arr.length ; i++) {
			// Each element will get subtracted and remaining will be in sum
			sum -= arr[i];
		}
		return sum;
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n-1];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		
		}
		System.out.println("Missing element : "+findMissingElement(arr));

	}
}
