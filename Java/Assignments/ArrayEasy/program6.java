// Second largest element(distinct)

import java.util.*;

class ArrayEasy {

	static void secLargest(int arr[]) {
		int l = Integer.MIN_VALUE;
		
		for(int i = 0 ; i<arr.length ; i++) {
			if(l<arr[i]) {
				l = arr[i];
			}
		}

		int s = Integer.MIN_VALUE;
		for(int i = 0 ; i<arr.length ; i++) {
			if(s<arr[i] && arr[i] != l) {
				s = arr[i];
			}
		}

		if(s != Integer.MIN_VALUE) {
			System.out.println("Second Largest : " + s);
		}else {
			System.out.println("Not found");
		}
		
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		secLargest(arr);
	}
}
