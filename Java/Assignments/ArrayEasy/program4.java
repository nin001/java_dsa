// Count pairs with given sum

import java.util.*;

class ArrayEasy {

	static int countPairs(int arr[] , int k) {
		int count = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			for(int j = i+1 ; j<arr.length ; j++) {
				if(arr[i]+arr[j] == k) 
					count++;
			}
		}
		return count;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter K :");
		int k = sc.nextInt();

		int pairCount = countPairs(arr,k);

		System.out.println("Count :"+pairCount);
	}
}
