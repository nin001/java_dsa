// Frequeincies of limited Range in Array 

import java.io.*;

class ArrayEasy {

	static void frequencyArray(int arr[] , int p) {
		for(int i = 1 ; i<=p ; i++) {
			int count = 0;
			for(int j = 0 ; j<arr.length ; j++) {
				if(arr[j] == i) {
					count++;
				}
			}
			System.out.print(count + " ");
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size : ");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter p :");
		int p = Integer.parseInt(br.readLine());

		frequencyArray(arr,p);

	}
}
