// First and last occurance of x in given sorted array

import java.util.*;

class ArrayEasy {

	static void firstAndLastOcc(int arr[] , int x) {

		int first = -1;
		int last = -1;

		int start = 0;
		int end = arr.length-1;
		int mid = (start+end)/2;

		while(start<=end) {
			if(arr[mid] == x) {
				start = Integer.MAX_VALUE;	//Using as a flag
				break;
			}else if(arr[mid]<x) {
				start = mid+1;
			}else {
				end = mid-1;
			}
			mid = (start+end)/2;
		}

		if(start == Integer.MAX_VALUE) {
			int i = mid;
			int j = mid;
			
			while(i>=0 && arr[i] == x) {
				i--;
			}

			while(j<arr.length && arr[j] == x) {
				j++;
			}
			first = i+1;
			last = j-1;
	
			System.out.println(first + " " + last);
		}else {
			System.out.println(-1);
		}
	}


	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter x :");
		int x = sc.nextInt();

		firstAndLastOcc(arr,x);
	}
}
