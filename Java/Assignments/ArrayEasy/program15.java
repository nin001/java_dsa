//Union of two sorted Arrays

import java.util.*;

class ArrayEasy {

	static int nextDistinct(int a[] , int start) {
		for(int i = start+1 ; i<a.length ; i++) {
			if(a[start] != a[i]) {
				return i;
			}
		}
		return a.length;
	}
	static void unionArray(int arr1[] , int arr2[]) {
		
		int n = arr1.length;
		int m = arr2.length;

		int i = 0;
		int j = 0;

		while(i<n && j<m) {
			if(arr1[i] == arr2[j]) {
				System.out.print(arr1[i] + " ");
				i = nextDistinct(arr1,i);
				j = nextDistinct(arr2,j);
			}else if(arr1[i] < arr2[j]) {
				System.out.print(arr1[i] + " ");
				i = nextDistinct(arr1,i);
				
			}else if(arr1[i] > arr2[j]) {	
				System.out.print(arr2[j] + " ");
				j = nextDistinct(arr2,j);
				
			}
		}

		while(i<n) {
			System.out.print(arr1[i] + " ");
			i = nextDistinct(arr1,i);
		}

		while(j<m) {
			System.out.print(arr2[j] + " ");
			j = nextDistinct(arr2,j);
		}

		System.out.println();

	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr1[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter size :");
		int m = sc.nextInt();

		int arr2[] = new int[m];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<m ; i++) {
			arr2[i] = sc.nextInt();
		}

		unionArray(arr1,arr2);

	}
}
