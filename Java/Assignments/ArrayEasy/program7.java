// Check if array is sorted

import java.io.*;

class ArrayEasy {

	static int isSorted(int arr[]) {
		for(int i = 0 ; i<arr.length-1 ; i++) {
			if(arr[i] > arr[i+1]) {
				return 0;
			}
		}
		return 1;
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		if(1 == isSorted(arr)) {
			System.out.println("Sorted");
		}else {
			System.out.println("Not sorted");
		}
	}
}
