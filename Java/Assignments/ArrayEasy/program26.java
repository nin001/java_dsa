// Leaders in Array
// Elements is a leader which is greater than or equal to the all the right side
// elements

import java.io.*;

class ArrayEasy {

	static void leadersInArray(int arr[]) {

		int rightMax[] = new int[arr.length];
		rightMax[arr.length-1] = arr[arr.length-1];
		for(int i = arr.length-2 ; i>=0 ; i--) {
			if(arr[i]>rightMax[i+1]) {
				rightMax[i] = arr[i];
			}else {
				rightMax[i] = rightMax[i+1];
			}
		}

		for(int i = 1 ; i<rightMax.length ; i++) {
			if(rightMax[i-1]>rightMax[i]) {
				System.out.print(rightMax[i-1] + " ");
			}
			if(i == rightMax.length-1) {
				System.out.println(rightMax[i]);
			}
		}

	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		leadersInArray(arr);
	}
}
