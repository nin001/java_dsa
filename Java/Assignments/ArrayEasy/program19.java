// Elements with left side smaller and right side greater

import java.util.*;

class ArrayEasy {

	static int leftSmallerRightGreater(int arr[]) {
		
		int n = arr.length;

		// left max and right min
		int leftMax[] = new int[n];
		leftMax[0] = arr[0];
		for(int i = 1 ; i<n ; i++) {
			if(arr[i] < leftMax[i-1]) {
				leftMax[i] = arr[i];
			}else {
				leftMax[i] = leftMax[i-1];
			}
		}

		int x = Integer.MAX_VALUE;

		for(int i = n-2 ; i>=1 ; i--) {
			if(leftMax[i]<arr[i] && x>arr[i]) {
				return arr[i];
			}

			if(x>arr[i]) 
				x = arr[i];
		}
		return -1;

	}
		
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int ele = leftSmallerRightGreater(arr);

		System.out.println(ele);
	}
}		
