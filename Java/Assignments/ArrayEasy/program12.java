// Given a sorted array and X find number of occurances in the array

import java.util.*;

class ArrayEasy {


	static int noOfOccurances(int arr[] , int start , int end , int x) {

		int count = 0;
		int mid = (start+end)/2;

		while(start<end) {
			if(arr[mid] == x) {
				count++;
				int left = mid-1;
				int right = mid+1;

				while(arr[left--] == x) {
					count++;
				}
				while(arr[right++] == x) {
					count++;

				}
				break;
			}else if(arr[mid]>x) {
				end = mid-1;
			}else {
				start = mid+1;
			}
			mid = (start+end)/2;
		}

		return count;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter X :");
		int x = sc.nextInt();

		int noOfOcc = noOfOccurances(arr,0,arr.length-1,x);

		System.out.println("No of Occurances : "+noOfOcc);
	}
}
