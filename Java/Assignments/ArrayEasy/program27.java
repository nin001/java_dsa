// Remove duplicate elements from a sorted array

import java.util.*;

class ArrayEasy {

	static void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static int removeDuplicate(int arr[]) {
		int temp = 0;
		
		for(int i = 1 ; i<arr.length ; i++) {
			if(arr[temp] != arr[i]) {
				swap(arr,temp+1,i);
				temp = i;
			}
		}

		if(temp == 0) {		// sagle elements same ahet
			return 1;
		}
		return temp;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<arr.length ; i++) {
			arr[i] = sc.nextInt();
		}

		int c = removeDuplicate(arr);

		for(int i = 0 ; i<c ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();

	}
}
