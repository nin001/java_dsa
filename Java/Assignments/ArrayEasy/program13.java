// Minimum distance between two numbers

import java.io.*;

class ArrayEasy {

	static int minDist(int arr[] , int x , int y) {

		int dist = arr.length+1;	// distance cant be more than arr.length (illegal value
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == x) {
				int flag = 0;
				int tempDist = 0;
				for(int j = i+1 ; j<arr.length ; j++) {
					tempDist++;
					if(arr[j] == y) {
						flag = 1;
						break;
					}
				}
				if(flag != 0) {
					if(dist>tempDist) {
						dist = tempDist;
					}
				}
			}
		}

		if(dist == arr.length+1) {
			return -1;
		}
		return dist;
					

	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter x :");
		int x = Integer.parseInt(br.readLine());

		System.out.println("Enter y :");
		int y = Integer.parseInt(br.readLine());

		int dist = minDist(arr,x,y);

		System.out.println(dist);

	}
}
