// Key pair
// Determine wheather a pair exist in an array with 
// given sum as X

import java.io.*;
import java.util.*;

class ArrayEasy {

	static boolean pairExist(int arr[] , int x) {
		HashSet<Integer> m = new HashSet<Integer>();

		for(int i = 0 ; i<arr.length ; i++) {
	
			if(m.contains(x-arr[i])) {
				return true;
			}

			m.add(arr[i]);
		}

		return false;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter X :");
		int x = Integer.parseInt(br.readLine());

		if(pairExist(arr,x)) {
			System.out.println("Yes");
		}else {
			System.out.println("No");
		}
	}
}
