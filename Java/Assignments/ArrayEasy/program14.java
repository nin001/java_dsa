// Intersection of 2 arrays with n and m size (print count of elements intersecting)

import java.util.*;

class ArrayEasy {

	static int intersection(int arr1[] , int arr2[]) {
		int count = 0;
		Arrays.sort(arr1);
		Arrays.sort(arr2);

		int i = 0;
		int j = 0;
		int n = arr1.length;
		int m = arr2.length;
		while(i<n && j<m) {
			if(arr1[i] == arr2[j]) {
				count++;
				i++;
				j++;
			}else if(arr1[i] < arr2[j]) {
				i++;
			}else {
				j++;
			}
		}
		return count;

	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr1[] = new int[n];

		System.out.println("Enter Elements : ");
		for(int i = 0 ; i<n ; i++) {
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter size :");
		int m = sc.nextInt();

		int arr2[] = new int[m];

		System.out.println("Enter Elements : ");
		for(int i = 0 ; i<m ; i++) {
			arr2[i] = sc.nextInt();
		}

		int interCount = intersection(arr1,arr2);

		System.out.println("intersecion elements count : "+interCount);
	}
}
