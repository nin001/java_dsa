// Given a sorted array which is rotated K times , find the value of k

import java.io.*;

class ArrayEasy {

	static int rotation(int arr[]) {
		int n = arr.length;

		int start = 0;
		int end = n-1;
		int mid = (start+end)/2;
		while(start<end) {
			if(arr[start]<arr[mid] && arr[mid]>arr[end]) {
				if(arr[mid]>arr[mid+1]) {
					return mid+1;
				}else {
					start = mid;
				}
			}else if(arr[start]<arr[mid] && arr[mid]<arr[end]) {
				if(arr[mid-1] > arr[mid]) {
					return mid+1;
				}else {
					end = mid;
				}
			}else {
				if(arr[start]>arr[start+1]) {
					return start+1;
				}else {
					start++;
					end--;
				}	
			}

			mid = (start+end)/2;
		}

		return 0;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int k = rotation(arr);

		System.out.println("K : "+k);
	}
}
