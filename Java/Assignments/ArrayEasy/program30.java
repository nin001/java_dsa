// Alternative positive negative numbers

import java.util.*;

class ArrayEasy {


	static void alternatePosNeg(int arr[]) {
		int negCount = 0;

		int n = arr.length;
		for(int i = 0 ; i<n ; i++) {
			if(arr[i]<0) {
				negCount++;
			}
		}

		int posArr[] = new int[n-negCount];
		int negArr[] = new int[negCount];

		int ni = 0;
		int pi = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] < 0) {
				negArr[ni++] = arr[i];
			}else {
				posArr[pi++] = arr[i];
			}
		}

		ni = 0;
		pi = 0;
		int  i = 0;
		int flag = 0;
		while(ni<negCount && pi<(n-negCount)) {
			if(flag == 0) {
				arr[i] = posArr[pi++];
				flag = 1;
			}else {
				arr[i] = negArr[ni++];
				flag = 0;
			}
			i++;
		}

		while(ni<negCount) {
			arr[i++] = negArr[ni++];
		}
		while(pi<(n-negCount)) {
			arr[i++] = posArr[pi++];
		}
	}

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		alternatePosNeg(arr);

		for(int i = 0 ; i<n ; i++) {
			System.out.print(arr[i]+ " ");
		}
		System.out.println();
	}
}
