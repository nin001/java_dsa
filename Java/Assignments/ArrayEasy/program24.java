// Number of buildings that will see sunshine
// sun is in the starting of the array and array contains 
// height of the buildings

import java.util.*;

class ArrayEasy {

	static int sunshine(int arr[]) {

		int bCount = 0;

		int tempHeight = -1;
		for(int i = 0 ; i<arr.length ; i++) {
			if(tempHeight<arr[i]) {
				bCount++;
				tempHeight = arr[i];
			}
		}

		return bCount;
	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}


		int count = sunshine(arr);

		System.out.println("Number of building seing sunshine : "+count);
	}
}
