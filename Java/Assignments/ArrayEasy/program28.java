// Product array Puzzle

import java.util.*;

class ArrayEasy {

	static int[] prodPuzzle(int arr[]) {
		int n = arr.length;
		int zflag = 0;

		int prod = 1;
		for(int i = 0 ; i<n ; i++) {
			if(arr[i]!=0) {
				prod *= arr[i];
			}else {
				zflag = 1;
			}

		}

		int prodArr[] = new int[n];
		for(int i = 0 ; i<n ; i++ ){
			if(zflag == 0) {
				prodArr[i] = prod/arr[i];
			}else {
				if(arr[i] == 0) {
					prodArr[i] = prod;
				}else {
					prodArr[i] = 0;
				}
			}
		}

		return prodArr;
	}
		
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int prod[] = prodPuzzle(arr);

		for(int i = 0 ; i<n ; i++) {
			System.out.print(prod[i] + " ");
		}
		System.out.println();
	}
}
