// Find all the pairs with given sum in array

import java.util.*;

class ArrayEasy {

	static void findPairs(int arr1[], int arr2[]	, int x) {

		int n = arr1.length;
		int m = arr2.length;

		Arrays.sort(arr1);
		Arrays.sort(arr2);		// Sorts Array in nlogn time complexity
		
		for(int i = 0 ; i<n ; i++) {
			int rem = x - arr1[i];
			
			int start = 0;
			int end = m-1;
			int mid = (start+end)/2;
			while(start<=end) {
				if(arr2[mid] == rem) {
					System.out.println(arr1[i] + " " + arr2[mid]);
					break;
				}else if(arr2[mid]<rem) {
					start = mid+1;
				}else {
					end = mid-1;
				}
				mid = (start+end)/2;
			}
		}

	}
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr1[] = new int[n];
		

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr1[i] = sc.nextInt();
		}
		System.out.println("Enter size :");
		int m = sc.nextInt();

		int arr2[] = new int[m];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<m ; i++) {
			arr2[i] = sc.nextInt();
		}

		System.out.println("Enter Sum : ");
		int x = sc.nextInt();

		findPairs(arr1,arr2,x);
	}
}
