// Sort array of 0s 1s and 2s

import java.util.*;

class ArrayEasy {

	static void swap(int arr[] , int i , int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	static void sort012(int arr[]) {
		int count0 = 0;
		int count1 = 0;
		int count2 = 0;
		for(int i = 0 ; i<arr.length ; i++) {
			if(arr[i] == 0) {
				count0++;
			}else if(arr[i] == 1) {
				count1++;
			}
		}

		for(int i = 0 ; i<arr.length ; i++) {
			if(count0 != 0) {
				arr[i] = 0;
				count0--;
			}else if(count1 != 0) {
				arr[i] = 1;
				count1--;
			}else {
				arr[i] = 2;
			}
		}

	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Element :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		sort012(arr);

		for(int i = 0 ; i<arr.length ; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
}
