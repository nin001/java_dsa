// Given array , find the possible triangles that can be formed using
// the array elements
// Triangle inequality theoremn -> a , b , c  are the sides of a triangle
// then a+b>c that is sum of two sides is greater than the third side

import java.util.*;

class ArrayMedium {

	static int possibleTriangleForm(int arr[]) {
		if(arr.length <3) {
			return 0;
		}

		int n = arr.length;
		
		int count = 0;
		for(int i = 0 ; i<n  ; i++) {
			for(int j = i+1 ; j<n ; j++) {
				for(int k = j+1 ; k<n ; k++) {
					if(arr[i] + arr[j] > arr[k])
						count++;
				}
			}
		}

		return count;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int count = possibleTriangleForm(arr);

		System.out.println(count);
	}
}
