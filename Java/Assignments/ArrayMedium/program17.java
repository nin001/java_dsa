// Maximum ons after flipping m 0s to 1s where array is of 1s and 0s
// and m is user defined

import java.util.*;

class ArrayMedium {

	static int maxNumOfOnes(int arr[] , int m) {
		int start = 0;
		int end = 0;
		int zeroCount = 0;
		while(end<arr.length) {
			if(arr[end] == 0) 
				zeroCount++;
			end++;
			if(zeroCount>m) {
				if(arr[start] == 0) 
					zeroCount--;
				start++;
			}
		}

		return end-star;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size:");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println("Enter m:");
		int m = sc.nextInt();

		int count = maxNumOfOnes(arr,m);

		System.out.println(count);
	}
}
