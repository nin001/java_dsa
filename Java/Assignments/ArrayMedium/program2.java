// Pythagorean triplets in an array
// if present return yes else no

import java.util.*;

class ArrayMedium {

        static boolean pythagoreanTriplets(int arr[]) {
		int max = arr[0];
		for(int i = 0 ; i<arr.length ; i++) {
			if(max < arr[i]) {
				max = arr[i];
			}
		}

		int countArr[] = new int[max+1];

		for(int i = 0 ; i<arr.length ; i++) {
			countArr[arr[i]]++;
		}

		for(int i = 1 ; i<max+1 ; i++) {
			if(countArr[i] != 0) {
				 for(int j = 1 ; j<max+1 ; j++) {
					 if((i != j && countArr[i] != 1) || countArr[i] != 0) {
						 int thirdSq = i*i + j*j;
						 int sq = (int)Math.sqrt(thirdSq);
						 if((sq*sq) == (i*i+j*j) && sq<max) {
							 if(countArr[sq] == 1) {
								 return true;
							 }
						 }
						
					 }
				 }
			}
		}

		return false;
        }
        public static void main(String args[]) {
                Scanner sc = new Scanner(System.in);

                System.out.println("Enter size :");
                int n = sc.nextInt();

                int arr[] = new int[n];

                System.out.println("Enter Elements :");
                for(int i = 0 ; i<n ; i++) {
                        arr[i] = sc.nextInt();
                }

                if(pythagoreanTriplets(arr)) {
                        System.out.println("Yes");
                }else {
                        System.out.println("No");
                }
        }
}
