// Maximum of all subarrays

import java.io.*;

class ArrayMedium {

	static int addInSub(int arr[] , int sub[] , int s , int e) {
		
		int subIndex = 0;
		sub[subIndex] = arr[s];
		int max = sub[subIndex];
		for(int i = s ; i<=e ; i++) {
			sub[subIndex] = arr[i];
			if(max<arr[i]) {
				max = arr[i];
			}
		}

		return max;
	}

	static void maxOfAllSubarrays(int arr[] , int k) {

		int sub[] = new int[k];
		int n = arr.length;
		for(int i = 0 ; i<n-(k-1) ; i++) {
			System.out.print(addInSub(arr,sub,i,i+(k-1)) + " ");
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter k :");
		int k = Integer.parseInt(br.readLine());

		maxOfAllSubarrays(arr,k);
	}
}
