// Check weather triplets are present in array with
// sum as zero , if present return 1 else return 0

import java.util.*;

class ArrayMedium {

	static int tripletsWithZeroSum(int arr[]) {

		int i = 0;
		int j = i+1;
		int k = j+1;

		while(k<arr.length) {
		
			int sum = arr[i] + arr[j] + arr[k];

			if(sum == 0) {
				return 1;
			}else {
				j++;
				k++;
			}
			if(arr.length == 3 && sum != 0) {
				return 0;
			}else if(k == arr.length) {
				i++;
				j = i+1;
				k = j+1;
			}
		}

		return 0;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		System.out.println(tripletsWithZeroSum(arr));
	}
}
