// Sum of 2 elements with sum nearest to 0

import java.io.*;

class ArrayMedium {

	static int checkCloseZero(int n1 , int n2) {
		int t1 = Math.abs(n1);
		int t2 = Math.abs(n2);

		if(t1<t2) {
			return n1;
		}else if(t2<t1) {
			return n2;
		}

		if(n1>0) {
			return n1;
		}
		return n2;
	}

	static int nearestSumZero(int arr[]) {
		
		int sum = Integer.MAX_VALUE;

		for(int i = 0 ; i<arr.length ; i++) {
			int temp = arr[i];
			for(int j = i+1 ; j<arr.length ; j++) {
				temp += arr[j];
				sum = checkCloseZero(temp,sum);
				temp = arr[i];
			}
		}
		return sum;

	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int sum = nearestSumZero(arr);
		System.out.println(sum);
	}
}
