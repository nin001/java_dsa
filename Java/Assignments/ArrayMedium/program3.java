// Find total number of subarrays whose total sum is Zero

import java.util.*;

class ArrayMedium {

	static int totalSubCount(int arr[]) {
		int n = arr.length;

		int count = 0 ;
		for(int i = 0 ; i<n ; i++) {
			int sum = 0;
			for(int j = i ; j<n ; j++) {
				sum += arr[j];
				if(sum == 0) {
					count++;
				}
			}
		}

		return count;
	}

	public static void main(String args[]) {

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int count = totalSubCount(arr);

		System.out.println(count);
	}
}
