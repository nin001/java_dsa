// Container with most water problem

import java.io.*;

class ArrayMedium {

	static int containerWithMostWater(int arr[]) {

		int n = arr.length;
		int start = 0;
		int end = n-1;
		int area = 0;

		while(start<end) {

			int temp = Math.min(arr[start],arr[end]) * (end-start);
			if(temp>area) {
				area = temp;
			}
			if(arr[start]<arr[end]) {
				start++;
			}else {
				end--;
			}
		}
		return area;
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int water = containerWithMostWater(arr);
		System.out.println(water);
	}
}
