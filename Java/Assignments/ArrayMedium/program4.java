// Kth smallest elements in arr

import java.io.*;

class ArrayMedium {

	static int kthSmallest(int arr[] , int k) {

		int kSmall = Integer.MIN_VALUE;

		for(int i = 0 ; i<k ; i++) {
			int min = Integer.MAX_VALUE;
			for(int j = 0 ; j<arr.length ; j++) {
				if(arr[j]<min && arr[j]>kSmall) {
					min = arr[j];
				}
			}

			kSmall = min;
		}
		return kSmall;
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter K :");
		int k = Integer.parseInt(br.readLine());

		int ele = kthSmallest(arr,k);

		System.out.println(ele);

		br.close();
	}
}
