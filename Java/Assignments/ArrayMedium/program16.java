// Nuts and bolts problem

import java.io.*;
import java.util.*;
class ArrayMedium {

	static void nutAndBoltProblem(char n[] , char b[]) {
// Ascii values of the given order in problem statement follows the order(ascending)
// sorting and mapping or nuts and bolts would give ans

		Arrays.sort(n);
		Arrays.sort(b);

		int i = 0;
		int j = 0;
		int l = n.length;
		while(i<l && j<l) {
			if(n[i] == b[i]) {
				System.out.println(n[i] + " " + b[i]);
			}
			i++;
			j++;
		}
	}

	public static void main(String args[])throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		char nuts[] = new char[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			nuts[i] = br.readLine().charAt(0);
		}

		char bolt[] = new char[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			bolt[i] = br.readLine().charAt(0);
		}

		nutAndBoltProblem(nuts,bolt);

	}
}
