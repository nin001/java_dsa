// Largest number formed by an array

import java.io.*;

class ArrayMedium {

	static String largestNumber(int arr[]) {

		String num = "";
		int n = arr.length;
		
		for(int i = 0 ; i<n ; i++) {
			int s1 = i;
			int dig = arr[i];
			while(dig/10 != 0) {
				dig /= 10;
			}

			for(int j = i+1 ; j<n ; j++) {
				int temp = arr[j];
				while(temp/10 != 0) {
					temp /= 10;
				}

				if(temp > dig) {
					s1 = j;
					dig = temp;
				}/*else if(temp == dig) {
					temp = arr[j];
					int dig2 = arr[i];
					int div = 10;

					while(true) {
						
						if(temp/div == 0 || dig2/div == 0) {
							break;
						}

						while(temp/(10*div) != 0) {
							temp /= 10;
						}

						while(dig2/(10*div) != 0) {
							dig2 /= 10;
						}

						if(temp>dig2) {
							s1 = j;
							dig = temp;
							break;
						}else {
							temp = arr[j];
							dig2 = arr[i];
							div *= 10;
						}
					}*/

				
			}

			num = num + Integer.toString(arr[s1]);
			int temp = arr[s1];
			arr[s1] = arr[i];
			arr[i] = temp;
		}

		return num;
	}
	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		String number = largestNumber(arr);

		System.out.println(number);

		br.close();
	}
}
