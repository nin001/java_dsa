// Maximum index 

import java.io.*;

class ArrayMedium {

	static int maxOfIndex(int arr[]) {
		
/*		int maxDiff = -1;
		for(int i = 0 ; i<arr.length ; i++) {
			for(int j = 0 ; j<arr.length ; j++) {
				if(arr[i]<=arr[j] && maxDiff < (j-i)) {
					maxDiff = j-i;
				}
			}
		}
*/

		int maxDiff = -1;
		int n = arr.length;
		int rMax[] = new int[n];
		rMax[n-1] = arr[n-1];
		for(int i = n-2 ; i>=0 ;i --) {
			if(arr[i] > rMax[i+1]) {
				rMax[i] = arr[i];
			}else {
				rMax[i] = rMax[i+1];
			}
		}

		int lMin[] = new int[n];
		lMin[0] = arr[0];
		for(int i = 1 ; i<n ; i++) {
			if(arr[i]<lMin[i-1]) {
				lMin[i] = arr[i];
			}else {
				lMin[i] = lMin[i-1];
			}
		}

		int i = 0;
		int j = 0;
		while(i<n && j<n) {
			if(lMin[i]<=rMax[j]) {
				if(maxDiff < (j-i)) {
					maxDiff = j-i;
				}
				j++;
			}else {
				i++;
			}
		}

		return maxDiff;
	
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = Integer.parseInt(br.readLine());
		}

		int max = maxOfIndex(arr);

		System.out.println(max);
	}
}
