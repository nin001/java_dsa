// Given 2 arrays and k(index) task is to find the element after merging
// two arrays(sorted)

import java.util.*;

class ArrayMedium {

	static int kthElement2Arrays(int arr1[] , int arr2[] , int k) {

		int n = arr1.length;
		int m = arr2.length;

		int i = 0;
		int j = 0;
		int kIndex = 0;

		int merge[] = new int[m+n];
		int index = 0;
		while(i<n && j<m) {
			if(arr1[i]<arr2[j]) {
				merge[index++] = arr1[i++];
				kIndex++;
			}else {
				merge[index++] = arr2[j++];
				kIndex++;
			}
			if(kIndex-1 == k)
				break;
		}
		
		if(kIndex-1 == k) {
			return merge[k-1];
		}else {
			while(i<n) {
				merge[index++] = arr1[i++];
				kIndex++;
			}

			while(j<m) {
				merge[index++] = arr2[j++];
				kIndex++;
			}

		}

		return merge[k-1];
			
				
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr1[] = new int[n];
		System.out.println("Ente Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr1[i] = sc.nextInt();
		}

		System.out.println("Enter size :");
		int m = sc.nextInt();

		int arr2[] = new int[m];
		System.out.println("Ente Elements :");
		for(int i = 0 ; i<m ; i++) {
			arr2[i] = sc.nextInt();
		}

		System.out.println("Enter k");
		int k = sc.nextInt();

		int ele = kthElement2Arrays(arr1,arr2,k);

		if(ele != Integer.MIN_VALUE) {
			System.out.println(ele);
		}else {
			System.out.println("Not found");
		}
	}
}
