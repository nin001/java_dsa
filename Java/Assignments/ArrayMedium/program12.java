// Row with max 1s (multidimensional array)

import java.util.*;

class ArrayMedium {

	static int rowWithMaxOnes(int arr[][] , int row , int col) {

		int ret = -1;

		int count = 0;
		for(int i = 0 ; i<row ; i++) {
			int OneCount = 0;
			int start = 0;
			int end = col-1;
			int mid = (start+end)/2;
			while(start<=end) {
				if(arr[i][mid] == 1) {
					int j = mid;
					while(j>=0 && arr[i][j] != 0) {
						j--;
					}
					OneCount = (col-1) - (j+1);
					break;
				}else if(arr[i][mid] == 0) {
					start = mid+1;
				}
				mid = (start+end)/2;
			}

			if(count<OneCount) {
				count = OneCount;
				ret = i;
			}
		}
		return ret;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter row :");
		int n = sc.nextInt();
		System.out.println("Enter col :");
		int m = sc.nextInt();

		int arr[][] = new int[n][m];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			for(int j = 0 ; j<m ; j++) {
				arr[i][j] = sc.nextInt();
			}
		}

		System.out.println("Input Done");
		int r = rowWithMaxOnes(arr,n,m);

		System.out.println(r);
	}
}


