// Given two arrays with size m and n , 
// task is to find numbers that are present in the first array
// but not in the second array

import java.io.*;
import java.util.*;

class ArrayMedium {

	static boolean search(int arr[] , int k) {
		int start = 0;
		int end = arr.length-1;
		int mid = (start+end)/2;
		while(start<=end) {
			if(arr[mid] == k) {
				return true;
			}else if(arr[mid]<k) {
				start = mid+1;
			}else {
				end = mid-1;
			}
			mid = (start+end)/2;
		}
		return false;
	}

	static void findMissing(int arr1[] , int arr2[]) {

		int n = arr1.length;
		int m = arr2.length;

		Arrays.sort(arr2);
		for(int i = 0 ; i<n ; i++) {
			int flag = 0;
			for(int j = 0 ; j<m ; j++) {
				if(search(arr2,arr1[i])) {
					flag = 1;
				}
			}

			if(flag == 0) {
				System.out.print(arr1[i] + " ");
			}
		}
		System.out.println();
	}

	public static void main(String args[])throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size :");
		int n = Integer.parseInt(br.readLine());

		int arr1[] = new int[n];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<n ; i++) {
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter size :");
		int m = Integer.parseInt(br.readLine());

		int arr2[] = new int[m];
		System.out.println("Enter Elements :");
		for(int i = 0 ; i<m ; i++) {
			arr2[i] = Integer.parseInt(br.readLine());
		}

		findMissing(arr1,arr2);
	}
}
