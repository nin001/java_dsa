// Maximum subarray

import java.util.*;

class ArrayMedium {

	static int[] maxSubarray(int arr[]) {
		
		int max = Integer.MIN_VALUE;
		int s = -1;
		int e = -1;
		for(int i = 0 ; i<arr.length ; i++) {
			int sum = 0;
			for(int j = i ; j<arr.length ; j++) {
				sum += arr[j];
				if(max<sum) {
					max = sum;
					s = i;
					e = j;
				}
				if(max == sum) {
					if(s<i && e<j) {
						s = i;
						e = j;
					}
				}
			}
		}

		int ret[] = new int[e-s+1];

		for(int i = s ; i<=e ; i++) {
			ret[i] = arr[i];
		}

		return ret;
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter size :");
		int n = sc.nextInt();

		int arr[] = new int[n];
		System.out.println("Enter elements :");
		for(int i = 0 ; i<n ; i++) {
			arr[i] = sc.nextInt();
		}

		int ret[] = maxSubarray(arr);

		for(int i = 0 ; i<ret.length ; i++) {
			System.out.print(ret[i] + " ");
		}
		System.out.println();

	}
}
