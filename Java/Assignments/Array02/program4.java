/*Program 4
WAP to search a specific element from an array and return its index.
Input: 1 2 4 5 6
Enter element to search: 4
Output: element found at index: 2*/

import java.io.*;

class ArrayDemo{
	static int search(int arr[] , int k){
		for(int i = 0 ; i<arr.length ; i++){
			if(arr[i] == k){
				return i;
			}
		}
		return -1;
	}
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter size : ");
		int n = Integer.parseInt(br.readLine());

		int arr[] = new int[n];

		System.out.println("Enter array elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Element to Search :");
		int k = Integer.parseInt(br.readLine());

		int ret = search(arr,k);
		if(ret != -1){
			System.out.println("Element found At Index : "+ret);
		}else{
			System.out.println("Element not Found");
		}
	}
}
