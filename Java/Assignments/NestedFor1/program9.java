// C B A
// C B A
// C B A
class ForDemo{
	public static void main(String a[]){
		int row = 3,col = 3;
		char ch = 'A';
		for(int i = 1 ; i<row ; i++){
			ch++;
		}

		for(int i = 1 ; i<=row ; i++){
			char ch1 = ch;
			for(int j = 1 ; j<=col ; j++){
				System.out.print(ch1-- + " ");
			}
			System.out.println();
		}
	}
}
