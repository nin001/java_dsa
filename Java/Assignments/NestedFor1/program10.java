// 1 2 3 4
// 2 3 4 5
// 3 4 5 6
// 4 5 6 7
class ForDemo{
	public static void main(String a[]){
		int row = 4,col = 4;
		for(int i = 1 ; i<=row ; i++){
			int var = i;
			for(int j = 1 ; j<=col ; j++){
				System.out.print(var++ + " " );
			}
			System.out.println();
		}
	}
}
