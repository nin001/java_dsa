//print Odd elements

import java.io.*;

class ArrayDemo{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array Size:");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		int sum = 0;

		System.out.println("Enter Elements : ");
		for(int i = 0 ; i<arr.length ; i++){
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i]%2!=0)
				sum += arr[i];
		}
		System.out.println("Sum of Odd Elements : "+sum);
	}
}

