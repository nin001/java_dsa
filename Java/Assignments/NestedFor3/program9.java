// cube pattern
class ForDemo{
	public static void main(String a[]){
		int var = 1;
		for(int i = 1 ; i<=3 ; i++){
			for(int j = 1 ; j<=i ; j++){
				System.out.print(var*var*var++ + " ");
			}
			System.out.println();
		}
	}
}
