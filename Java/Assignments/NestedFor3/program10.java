/*
1234
456
67
7
*/

class ForDemo{
	public static void main(String a[]){
		int var = 1;
		for(int i = 1 ; i<=4 ; i++){
			for(int j = 1 ; j<= 4-i+1 ; j++){
				if(j == 4-i+1){
					System.out.print(var + " ");
				}else{
					System.out.print(var++ + " ");
				}
			}
			System.out.println();
		}
	}
}
