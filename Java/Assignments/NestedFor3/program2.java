/*
9
98
987
*/

class ForDemo{
	public static void main(String a[]){
		for(int i = 1 ; i<=4 ; i++){
			int var = 9;
			for(int j = 1 ; j<=i ; j++){
				System.out.print(var-- + " ");
			}
			System.out.println();
		}
	}
}
